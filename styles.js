import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
  img: {
    backgroundColor: '#fff',
    height: 300,
    flexDirection: 'column',
    marginBottom: 10,
    padding: 5,
  },
  card: {
    height: 250,
  },
  wrapper: {},
  slide1: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#9DD6EB',
  },
  slide2: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#97CAE5',
  },
  slide3: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#92BBD9',
  },
  text: {
    color: '#fff',
    fontSize: 30,
    fontWeight: 'bold',
  },
  main: {
    marginTop: 5,
    marginBottom: 5,
    borderTopColor: 'rgba(0,0,0,0.1)',
    borderTopWidth: 1,
  },
  profilePic: {
    width: 50,
    height: 50,
    borderRadius: 25,
    marginRight: 5,
    backgroundColor: 'rgba(100,100,100,0.3)',
  },
  txt: {textAlign: 'center', fontSize: 12},
  txticon: {textAlign: 'center'},
  imag: {
    padding: 10,
    width: 100,
    height: 100,
    //backgroundColor: 'rgba(100,100,100,0.3)',
    resizeMode: 'center',
  },
  slide: {
    borderWidth: 1,
    borderColor: 'grey',
    padding: 5,
    marginBottom: 10,
    marginTop: 10,
    borderRadius: 10,
  },
  slider: {
    width: '50%',
    borderRadius: 20,
  },
  divtxtimg: {height: 120, width: '100%'},
  slideImg: {
    width: '50%',
    height: 60,
    borderRadius: 30,
  },
  title: {
    marginTop: 10,
  },
  imggod: {
    width: 50,
    height: 50,
    borderRadius: 25,
    justifyContent: 'center',
    alignContent: 'center',
  },
  imggodchalisha: {
    resizeMode: 'stretch',
    width: '100%',
    height: 80,
  },
  txthead: {
    textAlign: 'center',
    fontSize: 18,
    padding: 5,
  },
  txtbox: {
    flex: 0.33,
    //borderColor: ' rgba(100, 100, 100, 0.3)',
    //borderWidth: 3,
    height: 150,
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  txtimg: {
    height: 95,
    width: '90%',
    backgroundColor: 'pink',
  },
  title: {
    fontSize: 14,
    padding: 5,
  },
  row: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    marginRight: 10,
    padding: 10,
    width: '90%',
  },
  profilePic: {
    width: 50,
    height: 50,
    borderRadius: 25,
    marginRight: 5,
    backgroundColor: 'rgba(100,100,100,0.3)',
  },
  fourimag: {
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },
  card: {
    height: 250,
  },
});
export default styles;
