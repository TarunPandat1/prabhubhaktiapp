import React, {Component} from 'react';
import {
  Text,
  View,
  Dimensions,
  Button,
  TouchableOpacity,
  Image,
  StyleSheet,
  StatusBar,
  ScrollView,
  SafeAreaView,
  TouchableWithoutFeedback,
} from 'react-native';
import {createAppContainer} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';
import {
  createBottomTabNavigator,
  createMaterialTopTabNavigator,
} from 'react-navigation-tabs';
import Ionicons from 'react-native-vector-icons/Ionicons';
import {HomeScreen} from './screens/HomeScreen';
import ProductScreen from './screens/ProductScreen';
import {Videos} from './screens/Videos';
import Lokpriya from './screens/Lokpriya';
import Profile from './screens/Profile';
import Login from './screens/Login';
import Video from './screens/Video';
import Feed from './screens/Feed';
import SignUp from './screens/SignUp';
import SignScreen from './screens/SignUp';
import ProductHome from './screens/ProductHome';
import {SplashScreen} from './screens/SplashScreen';
import {createDrawerNavigator} from 'react-navigation-drawer';
import {List, ListItem} from 'native-base';
import Arti from './screens/Arti';
import Bhajan from './screens/Bhajan';
import BhajanList from './screens/BhajanList';
import ArtiList from './screens/ArtiList';
import Chalisa from './screens/Chalisa';
import ChalisaList from './screens/ChalisaList';
import {CatPosts, CatVideos} from './screens/CusCategory';
// import Wishes from './screens/Wishes';
// import Wishhome1 from './screens/Wishhome1';
// import Wishhome from './screens/Wishhome';
// import Wishhome2 from './screens/Wishhome2';
// import Wishhome3 from './screens/Wishhome3';
// import Wishhome4 from './screens/Wishhome4';
import Sort from './screens/Sort';
import AddressScreen from './screens/AddressScreen';
import Thanksup from './screens/Thanksup';
import PaymentScreen from './screens/Payment';
import AsyncStorage from '@react-native-community/async-storage';
import {ShoppingCart} from './screens';
import DeviceInfo from 'react-native-device-info';
import Hanumancategory from './screens/Hanumancategory';
import Shivacategory from './screens/Shivacategory';
import Krishnacategory from './screens/Krishnacategory';
import Ganeshcategory from './screens/Ganeshcategory';
import Laxmicategory from './screens/Laxmicategory';
import Shanicategory from './screens/Shanicategory';
import Vishnucategory from './screens/Vishnucategory';
import Durgacategory from './screens/Durgacategory';
import Godrelated from './screens/Godrelated';
import GodrelatedPost from './screens/GodrelatedPost';
import Testing from './screens/Testing';
import MyOrders from './screens/MyOrders';
import ForgetPassword from './screens/ForgetPassword';
import HeaderIcon from './screens/header';

class SideMenu extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  logout = async () => {
    await AsyncStorage.removeItem('id');
    await AsyncStorage.removeItem('username');
    await AsyncStorage.removeItem('mobile');
    await AsyncStorage.removeItem('email');
    await AsyncStorage.removeItem('address1');
    await AsyncStorage.removeItem('address2');
    await AsyncStorage.removeItem('city');
    await AsyncStorage.removeItem('state');
    await AsyncStorage.removeItem('pincode');
    await AsyncStorage.removeItem('country');
    await AsyncStorage.removeItem('coupon_used');
    await AsyncStorage.removeItem('image');
    global.getData();
    this.props.navigation.navigate('Login');
  };

  componentDidMount() {
    global.getData();
    global.getMacAddress();
  }

  render() {
    return (
      <SafeAreaView
        style={{
          flex: 1,
          Padding: 0,
          flexDirection: 'column',
        }}>
        <StatusBar backgroundColor="#ff7604" barStyle="light-content" />
        <ScrollView style={{width: '100%', flex: 0.999}}>
          <List>
            <ListItem style={{flexDirection: 'row-reverse'}}>
              <TouchableOpacity
                onPress={() => this.props.navigation.closeDrawer()}>
                <Ionicons name="ios-close" size={30} color="#ff7604" />
              </TouchableOpacity>
            </ListItem>
            <ListItem style={styles.part}>
              {global.id ? (
                <Image source={{uri: global.image}} style={styles.ppic} />
              ) : (
                <Ionicons name="ios-person" size={30} color="#ff7604" />
              )}
            </ListItem>
            <ListItem style={styles.part}>
              <TouchableOpacity
                onPress={() => this.props.navigation.navigate('Home')}>
                <Ionicons name="ios-home" size={20} style={styles.bbtn} />
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() => this.props.navigation.navigate('Home')}>
                <Text>Home</Text>
              </TouchableOpacity>
            </ListItem>
            <ListItem style={styles.part}>
              <TouchableOpacity
                onPress={() => this.props.navigation.navigate('Feed')}>
                <Ionicons name="ios-paper" size={20} style={styles.bbtn} />
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() => this.props.navigation.navigate('Feed')}>
                <Text>Feed</Text>
              </TouchableOpacity>
            </ListItem>
            <ListItem style={styles.part}>
              <TouchableOpacity
                onPress={() => this.props.navigation.navigate('Videos')}>
                <Ionicons name="ios-videocam" size={20} style={styles.bbtn} />
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() => this.props.navigation.navigate('Videos')}>
                <Text>Video</Text>
              </TouchableOpacity>
            </ListItem>
            <ListItem style={styles.part}>
              <TouchableOpacity
                onPress={() => this.props.navigation.navigate('Profile')}>
                <Ionicons name="ios-person" size={20} style={styles.bbtn} />
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() => this.props.navigation.navigate('Profile')}>
                <Text>Profile</Text>
              </TouchableOpacity>
            </ListItem>
          </List>
        </ScrollView>
        <List style={styles.part}>
          <ListItem>
            {global.id ? (
              <TouchableOpacity onPress={() => this.logout()}>
                <View style={{flexDirection: 'row'}}>
                  <Ionicons name="ios-log-out" size={20} style={styles.bbtn} />
                  <Text>Logout</Text>
                </View>
              </TouchableOpacity>
            ) : (
              <TouchableOpacity
                onPress={() => this.props.navigation.navigate('Login')}>
                <View
                  style={{
                    flexDirection: 'row',
                  }}>
                  <Ionicons name="ios-log-in" size={20} style={styles.bbtn} />
                  <Text>LogIn</Text>
                </View>
              </TouchableOpacity>
            )}
          </ListItem>
        </List>
      </SafeAreaView>
    );
  }
}
const styles = StyleSheet.create({
  ppic: {
    width: 80,
    height: 80,
    borderRadius: 40,
    margin: 1,
    backgroundColor: 'rgba(100,100,100,0.3)',
  },
  part: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  bbtn: {padding: 2, marginHorizontal: 20},
});

const CatStack = createMaterialTopTabNavigator(
  {
    CatPost: CatPosts,
    CatVideos: CatVideos,
  },
  {
    defaultNavigationOptions: ({navigation}) => ({
      tabBarIcon: ({focused, tintColor}) => {
        const {routeName} = navigation.state;
        let IconComponent = Ionicons;
        let iconName;
        if (routeName === 'Home') {
          iconName = `ios-home${focused ? '' : ''}`;
          // Sometimes we want to add badges to some icons.
          // You can check the implementation below.
          //IconComponent = HomeIconWithBadge;
        } else if (routeName === 'MyOrders') {
          iconName = `ios-cart`;
        } else if (routeName === 'Product') {
          iconName = `ios-cart`;
        } else if (routeName === 'CatVideos') {
          iconName = `ios-videocam`;
        } else if (routeName === 'MyAccount') {
          iconName = `ios-person`;
        } else if (routeName === 'CatPost') {
          iconName = 'ios-book';
        } else if (routeName === 'SignUp') {
          iconName = 'ios-cart';
        }

        // You can return any component that you like here!
        return <IconComponent name={iconName} size={20} color={tintColor} />;
        // return <HeaderIcon />;
      },
    }),
    tabBarOptions: {
      activeTintColor: '#ff7604',
      inactiveTintColor: 'gray',
      indicatorStyle: {
        backgroundColor: '#ff7604',
      },
      showIcon: true,
      labelStyle: {fontSize: 8},
      tabStyle: {height: 50},
      style: {backgroundColor: 'white'},
    },
  },
);

const CustCateStack = createMaterialTopTabNavigator(
  {
    Videos: Godrelated,
    Posts: GodrelatedPost,
  },
  {
    defaultNavigationOptions: ({navigation}) => ({
      tabBarIcon: ({focused, tintColor}) => {
        const {routeName} = navigation.state;
        let IconComponent = Ionicons;
        let iconName;
        if (routeName === 'Home') {
          iconName = `ios-home${focused ? '' : ''}`;
          // Sometimes we want to add badges to some icons.
          // You can check the implementation below.
          //IconComponent = HomeIconWithBadge;
        } else if (routeName === 'MyOrders') {
          iconName = `ios-cart`;
        } else if (routeName === 'Product') {
          iconName = `ios-cart`;
        } else if (routeName === 'Videos') {
          iconName = `ios-videocam`;
        } else if (routeName === 'MyAccount') {
          iconName = `ios-person`;
        } else if (routeName === 'Posts') {
          iconName = 'ios-book';
        } else if (routeName === 'SignUp') {
          iconName = 'ios-cart';
        }

        // You can return any component that you like here!
        return <IconComponent name={iconName} size={20} color={tintColor} />;
        // return <HeaderIcon />;
      },
    }),
    tabBarOptions: {
      activeTintColor: '#ff7604',
      inactiveTintColor: 'gray',
      indicatorStyle: {
        backgroundColor: '#ff7604',
      },
      showIcon: true,
      labelStyle: {fontSize: 8},
      tabStyle: {height: 50},
      style: {backgroundColor: 'white'},
    },
  },
);

const HomeStack = createStackNavigator(
  {
    Home: HomeScreen,
    Cat: {
      screen: CatStack,
      navigationOptions: {
        headerShown: false,
        title: 'POST',
      },
    },

    Arti: {
      screen: Arti,
      navigationOptions: {
        headerShown: true,
      },
    },
    Bhajan: {
      screen: Bhajan,
      navigationOptions: {
        headerShown: true,
      },
    },
    Chalisa: {
      screen: Chalisa,
      navigationOptions: {
        headerShown: true,
      },
    },
    BhajanList: {
      screen: BhajanList,
      navigationOptions: {
        headerShown: true,
      },
    },
    ArtiList: {
      screen: ArtiList,
      navigationOptions: {
        headerShown: true,
      },
    },
    ChalisaList: {
      screen: ChalisaList,
      navigationOptions: {
        headerShown: true,
      },
    },
    Sort: {
      screen: Sort,
      navigationOptions: {
        headerShown: false,
      },
    },
    Lokpriya: Lokpriya,
    Video: Video,
    Hanumancategory: Hanumancategory,
    Shivacategory: Shivacategory,
    Durgacategory: Durgacategory,
    Shanicategory: Shanicategory,
    Krishnacategory: Krishnacategory,
    Ganeshcategory: Ganeshcategory,
    Laxmicategory: Laxmicategory,
    Vishnucategory: Vishnucategory,
    CustCate: CustCateStack,
  },
  {
    defaultNavigationOptions: {
      headerShown: false,
    },
  },
);

const FeedStack = createStackNavigator({
  Feed: Feed,
  Lokpriya: {
    screen: Lokpriya,
    navigationOptions: {
      headerShown: true,
    },
  },
});

const ProductHomeStack = createStackNavigator(
  {
    ProductHome: {
      screen: ProductHome,
      navigationOptions: {
        headerShown: false,
      },
    },
  },
  {
    defaultNavigationOptions: {
      headerShown: false,
    },
  },
);

const UserStack = createMaterialTopTabNavigator(
  {
    MyAccount: {
      screen: Profile,
      navigationOptions: {
        title: 'My Account',
      },
    },
    MyOrders: {
      screen: MyOrders,
      navigationOptions: {
        title: 'My Orders',
      },
    },
  },
  {
    defaultNavigationOptions: ({navigation}) => ({
      tabBarIcon: ({focused, tintColor}) => {
        const {routeName} = navigation.state;
        let IconComponent = Ionicons;
        let iconName;
        if (routeName === 'Home') {
          iconName = `ios-home${focused ? '' : ''}`;
          // Sometimes we want to add badges to some icons.
          // You can check the implementation below.
          //IconComponent = HomeIconWithBadge;
        } else if (routeName === 'MyOrders') {
          iconName = `ios-cart`;
        } else if (routeName === 'Product') {
          iconName = `ios-cart`;
        } else if (routeName === 'Video') {
          iconName = `ios-videocam`;
        } else if (routeName === 'MyAccount') {
          iconName = `ios-person`;
        } else if (routeName === 'Feed') {
          iconName = 'ios-post';
        } else if (routeName === 'SignUp') {
          iconName = 'ios-cart';
        }

        // You can return any component that you like here!
        return <IconComponent name={iconName} size={20} color={tintColor} />;
        // return <HeaderIcon />;
      },
    }),
    tabBarOptions: {
      activeTintColor: '#ff7604',
      inactiveTintColor: 'gray',
      indicatorStyle: {
        backgroundColor: '#ff7604',
      },
      showIcon: true,
      labelStyle: {fontSize: 8},
      tabStyle: {height: 50},
      style: {
        backgroundColor: 'white',
      },
    },
  },
);

const ProfileStack = createStackNavigator(
  {
    Profile: UserStack,
    Login: {
      screen: Login,
    },
    SignUp: {
      screen: SignScreen,
      navigationOptions: {
        headerShown: false,
      },
    },
  },
  {
    defaultNavigationOptions: {
      headerShown: false,
    },
  },
);

const VideoStack = createStackNavigator(
  {
    Videos: Videos,
    Video: {
      screen: Video,
      navigationOptions: {
        headerShown: true,
      },
    },
  },
  {
    defaultNavigationOptions: {
      headerShown: false,
    },
  },
);

const Tabs = createBottomTabNavigator(
  {
    Home: HomeStack,
    Feed: FeedStack,
    Product: {
      screen: ProductHomeStack,
      navigationOptions: {
        tabBarVisible: false,
      },
    },
    Video: VideoStack,
    Profile: ProfileStack,
  },

  {
    defaultNavigationOptions: ({navigation}) => ({
      tabBarIcon: ({focused, tintColor}) => {
        const {routeName} = navigation.state;
        let IconComponent = Ionicons;
        let iconName;
        if (routeName === 'Home') {
          iconName = `ios-home${focused ? '' : ''}`;
          // Sometimes we want to add badges to some icons.
          // You can check the implementation below.
          //IconComponent = HomeIconWithBadge;
        } else if (routeName === 'Feed') {
          iconName = `ios-paper`;
        } else if (routeName === 'Product') {
          iconName = `ios-cart`;
        } else if (routeName === 'Video') {
          iconName = `ios-videocam`;
        } else if (routeName === 'Profile') {
          iconName = `ios-person`;
        } else if (routeName === 'Feed') {
          iconName = 'ios-post';
        } else if (routeName === 'SignUp') {
          iconName = 'ios-cart';
        }

        // You can return any component that you like here!
        return <IconComponent name={iconName} size={25} color={tintColor} />;
      },
    }),
    tabBarOptions: {
      activeTintColor: '#ff7604',
      inactiveTintColor: 'gray',
      showIcon: true,
    },
  },
);

const appDrawer = createDrawerNavigator(
  {
    drawer: Tabs,
    ProductScreen: ProductScreen,
    ShoppingCart: ShoppingCart,
    Login: Login,
    SignUp: SignUp,
    PaymentScreen: PaymentScreen,
    AddressScreen: AddressScreen,
    Thanksup: Thanksup,
    // Wishes: Wishes,
    // Wishhome: Wishhome,
    // Wishhome1: Wishhome1,
    // Wishhome2: Wishhome2,
    // Wishhome3: Wishhome3,
    // Wishhome4: Wishhome4,
  },
  {
    contentComponent: SideMenu,
    drawerWidth: Dimensions.get('window').width * (1 / 2),
  },
);

const SplashStack = createStackNavigator(
  {
    // Tessting: Testing,
    Splash: SplashScreen,
    Main: appDrawer,
    ForgetPassword: ForgetPassword,
  },
  {
    defaultNavigationOptions: {
      headerShown: false,
    },
  },
);

export default createAppContainer(SplashStack);
