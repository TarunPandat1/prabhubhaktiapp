import React, {Component} from 'react';
import {Dimensions} from 'react-native';
import Main from './Main';
import AsyncStorage from '@react-native-community/async-storage';
import {Provider} from 'react-redux';
import {createStore, applyMiddleware} from 'redux';
import thunk from 'redux-thunk';
import DeviceInfo from 'react-native-device-info';

global.ip = 'http://139.59.80.133/';
global.width = parseInt(Dimensions.get('window').width);
global.imgHeight = parseInt(global.width * 1.1);
global.data = '';

global.getData = async () => {
  global.id = await AsyncStorage.getItem('id');
  global.username = await AsyncStorage.getItem('username');
  global.mobile = await AsyncStorage.getItem('mobile');
  global.email = await AsyncStorage.getItem('email');
  global.address1 = await AsyncStorage.getItem('address1');
  global.address2 = await AsyncStorage.getItem('address2');
  global.city = await AsyncStorage.getItem('city');
  global.state = await AsyncStorage.getItem('state');
  global.pincode = await AsyncStorage.getItem('pincode');
  global.country = await AsyncStorage.getItem('country');
  global.coupon_code = String(await AsyncStorage.getItem('coupon_used'));
  global.image = await AsyncStorage.getItem('image');
};

global.getMacAddress = async () => {
  DeviceInfo.getMacAddress().then(mac => {
    global.macAddress = mac;
  });
};

const initialState = {
  cart: [],
  orderHistory: 0,
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case 'CartCounting':
      return Object.assign({}, state, {cart: action.data});
    case 'OrderHistory':
      return Object.assign({}, state, {orderHistory: action.orders});
  }
  return state;
};

const store = createStore(reducer, applyMiddleware(thunk));

class MyMain extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentDidMount() {
    global.getData();
    global.getMacAddress();
  }

  render() {
    return (
      <Provider store={store}>
        <Main />
      </Provider>
    );
  }
}

export default MyMain;
