import React, {Component} from 'react';
import {Text, View, TouchableWithoutFeedback, Image} from 'react-native';

import DynamicHomeScreen from '../screens/DynamicHomeScreen';
import styles from '../styles';

export default class Posts extends Component {
  openPost = item => {
    this.props.navigation.navigate('Lokpriya', {
      title: item.post_title,
      mySrc: item.featuredImage,
      post_content: item.post_content.replace(/<[^>]*>|&nbsp;/g, ' '),
      cid: item.customize_category,
      id: item.id,
      title: item.post_title,
      catName: item.category_name,
      headerShown: true,
    });
  };

  render() {
    const data = this.props.data;
    const comp = data.map(item => {
      return (
        <TouchableWithoutFeedback
          onPress={() => {
            this.openPost(item);
          }}>
          <View style={styles.main}>
            <View style={styles.row}>
              <View>
                <Image style={styles.profilePic} source={{uri: item.image}} />
              </View>

              <View>
                <Text style={{fontWeight: '600', fontSize: 10, paddingLeft: 4}}>
                  {item.category_name}
                </Text>
                <Text style={styles.title}>{item.post_title}</Text>
              </View>
            </View>
            <View>
              <DynamicHomeScreen
                featuredImage={item.featuredImage}></DynamicHomeScreen>
            </View>
            <View style={{paddingBottom: 8}}>
              <Text>
                {item.post_content
                  .replace(/<[^>]*>|&nbsp;/g, ' ')
                  .substring(0, 125)}
                <Text style={{color: '#ff7604'}}> ...आगे पढ़ें</Text>
              </Text>
            </View>
          </View>
        </TouchableWithoutFeedback>
      );
    });
    return (
      <>
        <View>{comp}</View>
      </>
    );
  }
}
