import React, {Component} from 'react';
import {
  Text,
  View,
  TouchableWithoutFeedback,
  Image,
  Dimensions,
} from 'react-native';

import DynamicHomeScreen from '../screens/DynamicHomeScreen';
import styles from '../styles';

import Swiper from 'react-native-swiper';
import Carousel from 'react-native-snap-carousel';

export default class Products extends Component {
  openProduct = item => {
    this.props.navigation.navigate('ProductScreen', {
      product_name: item.product_name,
      image1: item.productImage1,
      image2: item.productImage2,
      image3: item.productImage3,
      image4: item.productImage4,
      image5: item.productImage5,
      image6: item.productImage6,
      price: item.price,
      special_price: item.spcial_price,
      productId: item.product_id,
      description: item.description,
      brief_description: item.brief_description.replace(/<[^>]*>|&nbsp;/g, ' '),
      sku: item.product_sku,
    });
  };

  renderData = ({item, index}) => {
    return (
      <TouchableWithoutFeedback
        style={{borderWidth: 4, borderColor: 'red'}}
        onPress={() => this.openProduct(item)}>
        <View
          style={{
            width: 200,
            margin: 10,
            marginBottom: 20,
            marginTop: 10,
            padding: 2,
          }}>
          <View style={{backgroundColor: 'grey'}}>
            <Image style={styles.card} source={{uri: item.productImage1}} />
          </View>
          <Text style={{paddingLeft: 10, fontSize: 14, fontWeight: 'bold'}}>
            {' '}
            {item.product_name}
          </Text>
          <Text style={{paddingLeft: 10, fontSize: 14}}>
            <Image
              source={require('../image/rupay.png')}
              style={{height: 14, width: 14}}
            />
            <Text style={{color: '#ff7604', paddingRight: 10}}>
              {item.spcial_price != '0' ? (
                <>
                  {item.spcial_price}{' '}
                  <Text
                    style={{
                      color: 'rgba(0,0,0,0.5)',
                      textDecorationLine: 'line-through',
                    }}>
                    {item.price}
                  </Text>
                </>
              ) : (
                item.price
              )}
            </Text>
            <Text style={{fontWeight: 'bold'}}>
              {item.spcial_price != '0' ? (
                <>
                  {' '}
                  {100 - Math.round((item.spcial_price / item.price) * 100)}%
                  OFF{' '}
                </>
              ) : null}
            </Text>
          </Text>
        </View>
      </TouchableWithoutFeedback>
    );
  };

  render() {
    return (
      <Carousel
        style={styles.slider}
        ref={c => {
          this._carousel = c;
        }}
        data={this.props.data}
        renderItem={this.renderData}
        sliderWidth={global.width}
        itemWidth={200}
      />
    );
  }
}
