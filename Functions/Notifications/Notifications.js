//1
checkPermission = async () => {
  const enabled = await firebase.messaging().hasPermission();
  if (enabled) {
    this.getToken();
  } else {
    this.requestPermission();
  }
};

//3
getToken = async () => {
  let fcmToken = await AsyncStorage.getItem('fcmToken');
  if (!fcmToken) {
    fcmToken = await firebase.messaging().getToken();
    if (fcmToken) {
      // user has a device token
      console.log('fcmToken:', fcmToken);
      await AsyncStorage.setItem('fcmToken', fcmToken);
    }
  }
  console.log('fcmToken:', fcmToken);
};

//2
requestPermission = async () => {
  try {
    await firebase.messaging().requestPermission();
    // User has authorised
    getToken();
  } catch (error) {
    // User has rejected permissions
    console.log('permission rejected');
  }
};

createNotificationListeners = async () => {
  /*
   * Triggered when a particular notification has been received in foreground
   * */
  this.notificationListener = firebase
    .notifications()
    .onNotification(notification => {
      console.log('onNotification:');

      const localNotification = new firebase.notifications.Notification({
        sound: 'sampleaudio',
        show_in_foreground: true,
      })
        .setSound('sampleaudio')
        .setNotificationId(notification.notificationId)
        .setTitle(notification.title)
        .setBody(notification.body)
        .setData(notification.data)
        .android.setChannelId('fcm_FirebaseNotifiction_default_channel') // e.g. the id you chose above
        .android.setSmallIcon('@drawable/ic_launcher') // create this icon in Android Studio
        .android.setColor('#000000') // you can set a color here
        .android.setPriority(firebase.notifications.Android.Priority.High);

      firebase
        .notifications()
        .displayNotification(localNotification)
        .catch(err => console.error(err));
    });

  const channel = new firebase.notifications.Android.Channel(
    'fcm_FirebaseNotifiction_default_channel',
    'Demo app name',
    firebase.notifications.Android.Importance.High,
  )
    .setDescription('Demo app description')
    .setSound('sampleaudio');
  firebase.notifications().android.createChannel(channel);

  /*
   * If your app is in background, you can listen for when a notification is clicked / tapped / opened as follows:
   * */
  this.notificationOpenedListener = firebase
    .notifications()
    .onNotificationOpened(notificationOpen => {
      const {data} = notificationOpen.notification;
      console.log('onNotificationOpened:');
      console.log(notificationOpen.notification);

      this.getDataByLink(data.link).catch(err => {
        this.props.navigation.navigate('Feed');
        console.log(err);
      });
    });

  /*
   * If your app is closed, you can check if it was opened by a notification being clicked / tapped / opened as follows:
   * */
  const notificationOpen = await firebase
    .notifications()
    .getInitialNotification();
  if (notificationOpen) {
    const {data} = notificationOpen.notification;
    console.log('getInitialNotification:');
    console.log(notificationOpen.notification);
    this.getDataByLink(data.link).catch(err => {
      this.props.navigation.navigate('Feed');
      console.log(err);
    });
  }
  /*
   * Triggered for data only payload in foreground
   * */
  this.messageListener = firebase.messaging().onMessage(message => {
    //process data message
    console.log('JSON.stringify:', JSON.stringify(message));
  });
};

export {
  checkPermission,
  getToken,
  requestPermission,
  createNotificationListeners,
};
