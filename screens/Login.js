import React, {Component} from 'react';
import {
  Text,
  StyleSheet,
  View,
  TextInput,
  TouchableOpacity,
  Button,
  Image,
  TouchableWithoutFeedback,
  KeyboardAvoidingView,
  Keyboard,
} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import {connect} from 'react-redux';

class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      username: '',
      password: '',
      uerror: '',
      perror: '',
      lerror: '',
      data: '',
    };
  }

  login = async () => {
    this.setState({
      uerror: '',
      perror: '',
      lerror: '',
    });

    var user = this.state.username;
    var pass = this.state.password;

    if (user && pass) {
      const response = await fetch(
        global.ip +
          'login?username=' +
          this.state.username +
          '&password=' +
          this.state.password +
          '',
      );
      const data = await response.json();
      console.log(data);
      if (data.length > 0) {
        await AsyncStorage.setItem('id', String(data[0].id));
        await AsyncStorage.setItem('username', data[0].name);
        await AsyncStorage.setItem('mobile', String(data[0].mobile));
        await AsyncStorage.setItem('email', data[0].email);
        let address = JSON.parse(data[0].address);
        await AsyncStorage.setItem('address1', address.address1);
        await AsyncStorage.setItem('address2', address.address2);
        await AsyncStorage.setItem('state', data[0].state);
        await AsyncStorage.setItem('city', data[0].city);
        await AsyncStorage.setItem('pincode', String(data[0].pincode));
        await AsyncStorage.setItem('country', data[0].country);
        if (data[0].coupon_used !== null) {
          await AsyncStorage.setItem('coupon_used', data[0].coupon_used);
        }

        await AsyncStorage.setItem('image', data[0].image);
        this.setState({
          data: await AsyncStorage.getItem('username'),
        });
        this.props.navigation.replace('Profile');
        global.getData();
        this.props.CartCounting();
        this.props.OrderHistory();
      } else {
        this.setState({
          lerror: 'Username and Password is Incorrect...!',
        });
      }
    } else {
      if (!user) {
        this.setState({
          uerror: 'Please fill your Username',
        });
      }

      if (!pass) {
        this.setState({
          perror: 'Please fill your Password',
        });
      }
    }
  };

  data = async () => {
    this.setState({
      data: await AsyncStorage.getItem('user'),
    });
  };

  signUp = () => {
    this.props.navigation.navigate('SignUp');
  };

  componentDidMount() {
    // this.login();
  }

  render() {
    return (
      <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
        <View style={styles.main}>
          <KeyboardAvoidingView style={styles.inputCon} behavior="padding">
            <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
              <View>
                <View
                  style={{
                    justifyContent: 'center',
                    alignItems: 'center',
                    marginBottom: 50,
                  }}>
                  <Image
                    source={require('../image/logo.png')}
                    style={{height: 80, width: 320}}
                  />
                </View>
                <View>
                  <TextInput
                    style={styles.input}
                    placeholder="Username / Email / Mobile no:"
                    onChangeText={username => this.setState({username})}
                    onSubmitEditing={() => this.refs.password.focus()}
                  />
                  <Text style={styles.error}>{this.state.uerror}</Text>
                  <TextInput
                    style={styles.input}
                    placeholder="Password"
                    onChangeText={password => this.setState({password})}
                    secureTextEntry={true}
                    returnKeyType="next"
                    ref={'password'}
                  />
                  <Text style={styles.error}>{this.state.perror}</Text>

                  <Text style={styles.error}>{this.state.lerror}</Text>

                  <TouchableOpacity
                    onPress={this.login}
                    style={styles.loginBtn}>
                    <Text style={{color: '#ffffff'}}>Login</Text>
                  </TouchableOpacity>
                </View>
                <TouchableWithoutFeedback
                  onPress={() => {
                    this.signUp();
                  }}>
                  <Text style={{marginTop: 10}}>
                    New to Prabhubhakti?
                    <Text style={{color: '#ff7604'}}>SignUp</Text>
                  </Text>
                </TouchableWithoutFeedback>
                <TouchableWithoutFeedback
                  onPress={() =>
                    this.props.navigation.navigate('ForgetPassword')
                  }>
                  <Text
                    style={{
                      color: '#ff7604',
                      marginTop: 20,
                      textAlign: 'center',
                    }}>
                    Forget Password...!
                  </Text>
                </TouchableWithoutFeedback>
              </View>
            </TouchableWithoutFeedback>
          </KeyboardAvoidingView>
        </View>
      </TouchableWithoutFeedback>
    );
  }
}

const styles = StyleSheet.create({
  heading: {
    alignItems: 'center',
    justifyContent: 'center',
    fontSize: 20,
    color: '#ff7604',
    fontWeight: 'bold',
    textAlign: 'center',
  },
  main: {
    margin: 20,
    justifyContent: 'center',
    alignContent: 'center',
  },
  input: {
    borderBottomWidth: 1,
    borderBottomColor: '#ff7604',
  },
  loginBtn: {
    justifyContent: 'center',
    alignItems: 'center',
    padding: 10,
    borderWidth: 1,
    margin: 5,
    borderColor: '#ff7604',
    backgroundColor: '#ff7604',
    marginTop: 30,
    borderRadius: 15,
  },
  error: {
    color: 'red',
  },
});

function mapStateToProps(state) {
  return {
    counter: state.cart,
    orders: state.orderHistory,
  };
}

const cartAPI = () => {
  return (dispatch, getState) => {
    fetch(
      `${global.ip}cart?id=${global.macAddress}&user=${global.username}&mobile=${global.mobile}`,
    )
      .then(response => response.json())
      .then(responseJson => {
        // console.log(responseJson);
        dispatch({type: 'CartCounting', data: responseJson});
      })
      .catch(err => console.log(err));
  };
};

const orderHistoryAPI = () => {
  return (dispatch, getState) => {
    fetch(
      `${global.ip}orderHistory?user=${global.username}&mobile=${global.mobile}`,
    )
      .then(response => response.json())
      .then(responseJson => {
        // console.log(responseJson);
        dispatch({type: 'OrderHistory', orders: responseJson});
      })
      .catch(err => console.log(err));
  };
};

function mapDispatchToProps(dispatch) {
  return {
    CartCounting: () => dispatch(cartAPI()),
    OrderHistory: () => dispatch(orderHistoryAPI()),
  };
}
export default connect(mapStateToProps, mapDispatchToProps)(Login);
