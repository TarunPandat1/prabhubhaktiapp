import React, {Component} from 'react';
import {
  Text,
  View,
  StyleSheet,
  Image,
  ActivityIndicator,
  FlatList,
  Button,
  TouchableOpacity,
  TouchableWithoutFeedback,
  Share,
  Alert,
} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import Icon from 'react-native-ionicons';
import HeaderIcon from './header';
import DynamicHomeScreen from './DynamicHomeScreen';
import Ionicons from 'react-native-vector-icons/Ionicons';

export default class CatPosts extends Component {
  static navigationOptions = {
    headerShown: false,
    title: 'POST',
  };

  offset = 0;

  state = {
    post: [],
    newPost: [],
    loading: true,
    width: global.width,
    height: 100,
  };

  fetch = async o => {
    let catId = this.props.navigation.getParam('catId');
    const response = await fetch(
      global.ip + 'catPosts?catId=' + catId + '&offset=' + o,
    );
    const data = await response.json();

    if (o == 0) {
      id = this.props.navigation.getParam('catId').toString() + 'CatPosts';
      await AsyncStorage.setItem(id, JSON.stringify(data));
    }
    this.setState(
      {
        post: this.state.post.concat(data),
        loading: false,
      },
      () => {
        console.log('Post found successfully..!');
      },
    );
  };

  isCloseToBottom = ({layoutMeasurement, contentOffset, contentSize}) => {
    return (
      layoutMeasurement.height + contentOffset.y >= contentSize.height - 1500
    );
  };

  componentDidMount() {
    this.fetch(this.offset).catch(err => {
      console.log(err);
      Alert.alert(
        'Internet Connection Required',
        'Please connect to the internet then try again...!',
        [{text: 'OK', onPress: () => console.log('OK Pressed')}],
        {cancelable: false},
      );
      this.get_post();
    });
  }

  get_post = async () => {
    id = this.props.navigation.getParam('catId').toString() + 'CatPosts';
    var l = await AsyncStorage.getItem(id);
    this.setState({
      post: JSON.parse(l),
    });
  };

  openPost = item => {
    this.props.navigation.navigate('Lokpriya', {
      title: item.post_title,
      mySrc: item.featuredImage,
      section_seo_url: item.section_seo_url,
      post_content: item.post_content.replace(/<[^>]*>|&nbsp;/g, ' '),
      cid: item.customize_category,
      id: item.id,
      title: item.post_title,
      catName: item.category_name,
    });
  };

  getMorePost = () => {
    this.setState({
      loading: true,
    });
    this.offset = this.offset + 10;
    this.fetch(this.offset).catch(err => {
      console.log(err);
    });
    console.log('scroll');
  };

  getDynamicImageStyle = imageUri => {
    Image.getSize(imageUri, (width, height) => {
      dynamicHeight = height;
    });
    return dynamicHeight;
  };

  renderFlatList = item => {
    return (
      <TouchableWithoutFeedback
        style={styles.main}
        onPress={() => {
          this.openPost(item);
        }}>
        <View style={styles.main}>
          <View style={styles.row}>
            <View>
              <Image style={styles.profilePic} source={{uri: item.image}} />
            </View>
            <View>
              <Text style={{fontWeight: '600', fontSize: 10, paddingLeft: 4}}>
                {item.category_name}
              </Text>
              <Text style={styles.title}>{item.post_title}</Text>
            </View>
          </View>
          <View>
            <DynamicHomeScreen
              featuredImage={item.featuredImage}></DynamicHomeScreen>
          </View>
          <View style={{paddingBottom: 8}}>
            <Text>
              {item.post_content
                .replace(/<[^>]*>|&nbsp;/g, ' ')
                .substring(0, 125)}
              <Text style={{color: '#ff7604'}}> ...आगे पढ़ें</Text>
            </Text>
          </View>
        </View>
      </TouchableWithoutFeedback>
    );
  };

  renderFooter = () => {
    return (
      <View>
        {this.state.loading && <ActivityIndicator size="large" color="cyan" />}
      </View>
    );
  };

  render() {
    return (
      <View style={styles.container}>
        <FlatList
          onScroll={({nativeEvent}) => {
            if (this.isCloseToBottom(nativeEvent)) {
              this.getMorePost();
            }
          }}
          data={this.state.post}
          keyExtractor={(item, index) => index.toString()}
          renderItem={({item}) => this.renderFlatList(item)}
          ListFooterComponent={this.renderFooter}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  title: {
    fontSize: 18,
    padding: 5,
  },
  row: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    marginRight: 10,
    padding: 10,
    width: '90%',
  },
  profilePic: {
    width: 50,
    height: 50,
    borderRadius: 25,
    marginRight: 5,
    backgroundColor: 'rgba(100,100,100,0.3)',
  },
  image: {
    width: global.width,
    height: 400,
    backgroundColor: 'rgba(100,100,100,0.3)',
    resizeMode: 'center',
  },
  main: {
    // marginBottom: 20,
    borderTopColor: 'rgba(0,0,0,0.1)',
    borderTopWidth: 1,
  },
  container: {
    flex: 1,
  },
});
