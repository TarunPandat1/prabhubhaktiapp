import React, {Component} from 'react';
import {
  Modal,
  StyleSheet,
  Text,
  TouchableHighlight,
  TouchableWithoutFeedback,
  ImageBackground,
  ScrollView,
  Button,
  Image,
  View,
  Alert,
  TextInput,
} from 'react-native';

export default class Wishhome1 extends Component {
  Send_Data_Function = () => {
    this.props.navigation.navigate('Wishhome2', {
      NameOBJ: this.props.navigation.state.params.NameOBJ,
    });
  };
  render() {
    return (
      <View style={{flexDirection: 'column', flex: 1}}>
        <ScrollView>
          <View
            style={{
              height: 50,
              alignItems: 'center',
              justifyContent: 'center',
            }}>
            <Image
              source={require('../image/logo.png')}
              style={{height: 40, width: 150}}
              resizeMode="stretch"
            />
            <Text>shiv</Text>
          </View>
          <View>
            <Text
              style={{
                fontSize: 30,
                color: '#ff7604',
                textAlign: 'center',
                paddingBottom: 15,
              }}>
              ॐ हूं हनुमते नमः ॥
            </Text>
          </View>
          <View style={{height: 570, backgroundColor: 'pink'}}>
            <ImageBackground
              source={require('../image/han1.jpg')}
              style={{width: '100%', height: '100%'}}
              resizeMode="stretch"></ImageBackground>
          </View>
          <Text
            style={{
              fontSize: 40,
              color: 'rgba(255,0,255,0.3)',
              textAlign: 'center',
            }}>
            {this.props.navigation.state.params.NameOBJ}
          </Text>
          <View style={{height: 70}}>
            <Text
              style={{
                fontSize: 20,
                textAlign: 'center',
              }}>
              नासे रोग हरे सब पीरा।।
            </Text>
            <Text
              style={{
                fontSize: 20,
                textAlign: 'center',
              }}>
              जो सुमिरे हनुमंत बलबीरा।।
            </Text>
          </View>
        </ScrollView>
        {/* <View
          style={{alignItems: 'center', justifyContent: 'center', padding: 5}}>
          <TextInput
            style={{
              height: 40,
              borderColor: 'gray',
              borderWidth: 1,
              borderRadius: 20,
              width: 200,
            }}
          />
        </View> */}
        <View
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'center',
          }}>
          <TouchableWithoutFeedback onPress={this.Send_Data_Function}>
            <Text
              style={{
                fontSize: 25,
                fontWeight: 'bold',
                color: 'red',
                backgroundColor: '#ff7604',
                borderRadius: 20,
                width: 200,
                textAlign: 'center',
                padding: 15,
              }}>
              फूल अर्पित करें
            </Text>
          </TouchableWithoutFeedback>
        </View>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  card: {
    backgroundColor: 'red',
    width: '100%',
    marginHorizontal: 10,
    marginTop: 24,
    flex: 0.5,
  },
});
