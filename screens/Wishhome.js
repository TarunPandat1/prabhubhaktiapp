import React, {Component} from 'react';
import {
  Modal,
  StyleSheet,
  Text,
  TouchableHighlight,
  TouchableWithoutFeedback,
  ImageBackground,
  ScrollView,
  Button,
  Image,
  View,
  Alert,
  TextInput,
} from 'react-native';
import StarRating from 'react-native-star-rating';

export default class Wishhome extends Component {
  // constructor(props){
  //   super(props);
  //   this.state={
  //     prevScreenTitle=this.props.navigation.state.param.previousScreen
  //   };
  // }
  Send_Data_Function = () => {
    this.props.navigation.navigate('Wishhome1', {
      NameOBJ: this.props.navigation.state.params.NameOBJ,
    });
  };
  render() {
    return (
      <View style={{flexDirection: 'column', flex: 1}}>
        <ScrollView>
          <View
            style={{
              height: 50,
              alignItems: 'center',
              justifyContent: 'center',
            }}>
            <Image
              source={require('../image/logo.png')}
              style={{height: 40, width: 150}}
              resizeMode="stretch"
            />
            <Text>Wishhome</Text>
          </View>
          <View>
            <Text
              style={{
                fontSize: 30,
                color: '#ff7604',
                textAlign: 'center',
                paddingBottom: 15,
              }}>
              ॐ हूं हनुमते नमः ॥
            </Text>
          </View>
          <View style={{height: 570, backgroundColor: 'pink'}}>
            <ImageBackground
              source={require('../image/han4.jpg')}
              style={{width: '100%', height: '100%'}}
              resizeMode="stretch"></ImageBackground>
          </View>
          {/* <View style={{height: 80, backgroundColor: 'yellow'}}>
            {/* <Text
              style={{
                fontSize: 20,
                textAlign: 'center',
              }}>
              ने आपको हनुमान जी का प्रसाद भेजा है ॥
            </Text> 
          </View> */}
          <View style={{height: 130}}>
            <Text
              style={{
                fontSize: 40,
                color: 'rgba(255,0,255,0.3);',
                textAlign: 'center',
              }}>
              {this.props.navigation.state.params.NameOBJ}
            </Text>
            <Text
              style={{
                fontSize: 20,
                textAlign: 'center',
              }}>
              भूत-पिशाच निकट नहीं आवे।
            </Text>
            <Text
              style={{
                fontSize: 20,
                textAlign: 'center',
              }}>
              महाबीर जब नाम सुनावे।।
            </Text>
          </View>
        </ScrollView>
        {/* <View
          style={{alignItems: 'center', justifyContent: 'center', padding: 5}}>
          <TextInput
            style={{
              height: 40,
              borderColor: 'gray',
              borderWidth: 1,
              borderRadius: 20,
              width: 200,
            }}
          />
        </View> */}
        <View
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'center',
          }}>
          <TouchableWithoutFeedback onPress={this.Send_Data_Function}>
            <Text
              style={{
                fontSize: 25,
                fontWeight: 'bold',
                color: 'red',
                backgroundColor: '#ff7604',
                borderRadius: 20,
                width: 200,
                textAlign: 'center',
                padding: 15,
              }}>
              चोला चढ़ाये
            </Text>
          </TouchableWithoutFeedback>
        </View>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  card: {
    backgroundColor: 'red',
    width: '100%',
    marginHorizontal: 10,
    marginTop: 24,
    flex: 0.5,
  },
});
