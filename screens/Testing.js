import React, {Component} from 'react';
import {
  Platform,
  Text,
  View,
  PermissionsAndroid,
  RefreshControl,
  Linking,
  Button,
  Alert,
  ScrollView,
} from 'react-native';

import styles from '../styles';
import {Posts, Posters, Products} from '../Components/';

import firebase from 'react-native-firebase';

var Sound = require('react-native-sound');
Sound.setCategory('Playback');

const instructions = Platform.select({
  ios: 'Press Cmd+R to reload,\n' + 'Cmd+D or shake for dev menu',
  android:
    'Double tap R on your keyboard to reload,\n' +
    'Shake or press menu button for dev menu',
});

export default class Testing extends Component {
  state = {
    refreshing: false,
    random: 0,
    data: [{name: 'Tarun'}, {name: 'Ajay'}, {name: 'Bhanu'}],
    fcmToken: '',
    data: [
      {
        product_id: 132,
        product_name: 'Hanumant ',
        seo_url: 'https://prabhubhakti.com/hanumant-',
        description: 'Jai Shree Hanuman poster ',
        brief_description: '',
        meta_title: '',
        meta_description: '',
        meta_keyword: '',
        price: 250,
        instant_price: '',
        tax_price: 0,
        model: '',
        treatment: '',
        quantity: 0,
        special_quantity_price: '',
        weight: '',
        tax_class: '',
        minimum_quantity: 1,
        capacity: '',
        product_code: '',
        consumption: '',
        supplier_location: '',
        substract_stock: 'yes',
        require_shipping: '',
        brand: 0,
        embed: '',
        related_product: '',
        related_product_two: '',
        related_product_three: '',
        parent: '55',
        sort_order: 0,
        post_status: 1,
        product_sku: '',
        id: 129,
        productImage1:
          'https://prabhubhakti.com/resources/media/1576823061_Jai shree Hanuman 1_0.jpg',
        productImage2:
          'https://prabhubhakti.com/resources/media/1576823061_Jai shree Hanuman 2_1.jpg',
        productImage3:
          'https://prabhubhakti.com/resources/media/1576823061_Jai shree Hanuman 3_2.jpg',
        productImage4:
          'https://prabhubhakti.com/resources/media/1576823061_Jai shree Hanuman 4_3.jpg',
        productImage5: '',
        productImage6: '',
        spcial_price: 100,
        start_date: '0000-00-00',
        end_date: '0000-00-00',
        status: 0,
      },
      {
        product_id: 131,
        product_name: 'Maa Durga Trishul',
        seo_url: 'https://prabhubhakti.com/maa-durga-trishul',
        description: 'Maa Durga Trishul Poster ',
        brief_description: '',
        meta_title: '',
        meta_description: '',
        meta_keyword: '',
        price: 400,
        instant_price: '',
        tax_price: 0,
        model: '',
        treatment: '',
        quantity: 0,
        special_quantity_price: '',
        weight: '',
        tax_class: '',
        minimum_quantity: 1,
        capacity: '',
        product_code: '',
        consumption: '',
        supplier_location: '',
        substract_stock: 'yes',
        require_shipping: '',
        brand: 0,
        embed: '',
        related_product: '',
        related_product_two: '',
        related_product_three: '',
        parent: '41',
        sort_order: 0,
        post_status: 1,
        product_sku: '',
        id: 128,
        productImage1:
          'https://prabhubhakti.com/resources/media/1576822333_Devi DUrga Trishul_3.jpg',
        productImage2:
          'https://prabhubhakti.com/resources/media/1576822333_Devi DUrga Trishul 1_0.jpg',
        productImage3:
          'https://prabhubhakti.com/resources/media/1576822333_Devi DUrga Trishul 2_1.jpg',
        productImage4:
          'https://prabhubhakti.com/resources/media/1576822333_Devi DUrga Trishul 3_2.jpg',
        productImage5: '',
        productImage6: '',
        spcial_price: 0,
        start_date: '0000-00-00',
        end_date: '0000-00-00',
        status: 0,
      },
      {
        product_id: 127,
        product_name: 'Gajanan',
        seo_url: 'https://prabhubhakti.com/gajanan',
        description: 'Gajanan Poster ',
        brief_description: '',
        meta_title: '',
        meta_description: '',
        meta_keyword: '',
        price: 300,
        instant_price: '',
        tax_price: 0,
        model: '',
        treatment: '',
        quantity: 0,
        special_quantity_price: '',
        weight: '',
        tax_class: '',
        minimum_quantity: 1,
        capacity: '',
        product_code: '',
        consumption: '',
        supplier_location: '',
        substract_stock: 'yes',
        require_shipping: '',
        brand: 0,
        embed: '',
        related_product: '',
        related_product_two: '',
        related_product_three: '',
        parent: '41',
        sort_order: 0,
        post_status: 1,
        product_sku: '',
        id: 125,
        productImage1:
          'https://prabhubhakti.com/resources/media/1576820524_Gajanan 1_0.jpg',
        productImage2:
          'https://prabhubhakti.com/resources/media/1576820524_Gajanan 4_3.jpg',
        productImage3:
          'https://prabhubhakti.com/resources/media/1576820524_Gajanan 3_2.jpg',
        productImage4:
          'https://prabhubhakti.com/resources/media/1576820524_Gajanan 2_1.jpg',
        productImage5: '',
        productImage6: '',
        spcial_price: 150,
        start_date: '0000-00-00',
        end_date: '0000-00-00',
        status: 0,
      },
      {
        product_id: 125,
        product_name: 'Maa Durga Trishul',
        seo_url: 'https://prabhubhakti.com/maa-durga-trishhul',
        description: 'Maa Durga  Trishul ',
        brief_description: '',
        meta_title: '',
        meta_description: '',
        meta_keyword: '',
        price: 500,
        instant_price: '',
        tax_price: 23,
        model: '',
        treatment: '',
        quantity: 0,
        special_quantity_price: '',
        weight: '',
        tax_class: '',
        minimum_quantity: 1,
        capacity: '',
        product_code: '',
        consumption: '',
        supplier_location: '',
        substract_stock: 'yes',
        require_shipping: '',
        brand: 0,
        embed: '',
        related_product: '',
        related_product_two: '',
        related_product_three: '',
        parent: '41',
        sort_order: 0,
        post_status: 1,
        product_sku: '',
        id: 123,
        productImage1:
          'https://prabhubhakti.com/resources/media/1576669335_Devi DUrga Trishul_3.jpg',
        productImage2:
          'https://prabhubhakti.com/resources/media/1576669335_Devi DUrga Trishul 1_0.jpg',
        productImage3:
          'https://prabhubhakti.com/resources/media/1576669335_Devi DUrga Trishul 2_1.jpg',
        productImage4:
          'https://prabhubhakti.com/resources/media/1576669335_Devi DUrga Trishul 3_2.jpg',
        productImage5: '',
        productImage6: '',
        spcial_price: 0,
        start_date: '0000-00-00',
        end_date: '0000-00-00',
        status: 0,
      },
      {
        product_id: 124,
        product_name: 'Mahadev Poster',
        seo_url: 'https://prabhubhakti.com/mahadev-poster',
        description: 'Mahadev Painter',
        brief_description: '',
        meta_title: '',
        meta_description: '',
        meta_keyword: '',
        price: 400,
        instant_price: '',
        tax_price: 0,
        model: '',
        treatment: '',
        quantity: 0,
        special_quantity_price: '',
        weight: '',
        tax_class: '',
        minimum_quantity: 1,
        capacity: '',
        product_code: '',
        consumption: '',
        supplier_location: '',
        substract_stock: 'yes',
        require_shipping: '',
        brand: 0,
        embed: '',
        related_product: '',
        related_product_two: '',
        related_product_three: '',
        parent: '41',
        sort_order: 0,
        post_status: 1,
        product_sku: '',
        id: 122,
        productImage1:
          'https://prabhubhakti.com/resources/media/1576669953_Mahadev 2_1.jpg',
        productImage2:
          'https://prabhubhakti.com/resources/media/1576669953_Mahadev 3_2.jpg',
        productImage3:
          'https://prabhubhakti.com/resources/media/1576669953_mahadev_3.jpg',
        productImage4:
          'https://prabhubhakti.com/resources/media/1576669953_Mahadev 1_0.jpg',
        productImage5: '',
        productImage6: '',
        spcial_price: 0,
        start_date: '0000-00-00',
        end_date: '0000-00-00',
        status: 0,
      },
      {
        product_id: 123,
        product_name: 'Trishul',
        seo_url: 'https://prabhubhakti.com/trishul',
        description: 'Mahadev Trishul Poster',
        brief_description: '',
        meta_title: '',
        meta_description: '',
        meta_keyword: '',
        price: 300,
        instant_price: '',
        tax_price: 0,
        model: '',
        treatment: '',
        quantity: 0,
        special_quantity_price: '',
        weight: '',
        tax_class: '',
        minimum_quantity: 1,
        capacity: '',
        product_code: '',
        consumption: '',
        supplier_location: '',
        substract_stock: 'yes',
        require_shipping: '',
        brand: 0,
        embed: '',
        related_product: '',
        related_product_two: '',
        related_product_three: '',
        parent: '55',
        sort_order: 0,
        post_status: 1,
        product_sku: '',
        id: 121,
        productImage1:
          'https://prabhubhakti.com/resources/media/1576669023_Shiv Trishul 3_2.jpg',
        productImage2:
          'https://prabhubhakti.com/resources/media/1576669023_Shiv Trishul 1_0.jpg',
        productImage3:
          'https://prabhubhakti.com/resources/media/1576669023_Shiv Trishul_3.jpg',
        productImage4:
          'https://prabhubhakti.com/resources/media/1576669023_Shiv Trishul 2_1.jpg',
        productImage5: '',
        productImage6: '',
        spcial_price: 0,
        start_date: '0000-00-00',
        end_date: '0000-00-00',
        status: 0,
      },
      {
        product_id: 122,
        product_name: 'Maa Durga',
        seo_url: 'https://prabhubhakti.com/maa-durga',
        description: 'Maa Durga Navratri Special Poster',
        brief_description: '',
        meta_title: '',
        meta_description: '',
        meta_keyword: '',
        price: 300,
        instant_price: '',
        tax_price: 0,
        model: '',
        treatment: '',
        quantity: 0,
        special_quantity_price: '',
        weight: '',
        tax_class: '',
        minimum_quantity: 1,
        capacity: '',
        product_code: '',
        consumption: '',
        supplier_location: '',
        substract_stock: 'yes',
        require_shipping: '',
        brand: 0,
        embed: '',
        related_product: '',
        related_product_two: '',
        related_product_three: '',
        parent: '41',
        sort_order: 0,
        post_status: 1,
        product_sku: '',
        id: 120,
        productImage1:
          'https://prabhubhakti.com/resources/media/1576668765_Durga Devi_3.jpg',
        productImage2:
          'https://prabhubhakti.com/resources/media/1576668765_Durga Devi 2_1.jpg',
        productImage3:
          'https://prabhubhakti.com/resources/media/1576668765_Durga Devi 1_0.jpg',
        productImage4:
          'https://prabhubhakti.com/resources/media/1576668765_Durga Devi 3_2.jpg',
        productImage5: '',
        productImage6: '',
        spcial_price: 0,
        start_date: '0000-00-00',
        end_date: '0000-00-00',
        status: 0,
      },
      {
        product_id: 121,
        product_name: 'Mor Pankh',
        seo_url: 'https://prabhubhakti.com/mor-pankh',
        description: 'Bhagwan Shri Krishna Mukut Mor Pankh Poster',
        brief_description: '',
        meta_title: '',
        meta_description: '',
        meta_keyword: '',
        price: 400,
        instant_price: '',
        tax_price: 0,
        model: '',
        treatment: '',
        quantity: 0,
        special_quantity_price: '',
        weight: '',
        tax_class: '',
        minimum_quantity: 1,
        capacity: '',
        product_code: '',
        consumption: '',
        supplier_location: '',
        substract_stock: 'yes',
        require_shipping: '',
        brand: 0,
        embed: '',
        related_product: '',
        related_product_two: '',
        related_product_three: '',
        parent: '55',
        sort_order: 0,
        post_status: 1,
        product_sku: '',
        id: 119,
        productImage1:
          'https://prabhubhakti.com/resources/media/1576668567_Mor Pankh (1)_0.jpg',
        productImage2:
          'https://prabhubhakti.com/resources/media/1576668567_Mor Pankh2 (1)_3.jpg',
        productImage3:
          'https://prabhubhakti.com/resources/media/1576668567_Mor Pankh 3 (1)_1.jpg',
        productImage4:
          'https://prabhubhakti.com/resources/media/1576668567_Mor Pankh1 (1)_2.jpg',
        productImage5: '',
        productImage6: '',
        spcial_price: 0,
        start_date: '0000-00-00',
        end_date: '0000-00-00',
        status: 0,
      },
      {
        product_id: 120,
        product_name: 'Hanumana',
        seo_url: 'https://prabhubhakti.com/hanumana',
        description: 'Hanumana Poster ',
        brief_description: '',
        meta_title: '',
        meta_description: '',
        meta_keyword: '',
        price: 500,
        instant_price: '',
        tax_price: 0,
        model: '',
        treatment: '',
        quantity: 0,
        special_quantity_price: '',
        weight: '',
        tax_class: '',
        minimum_quantity: 1,
        capacity: '',
        product_code: '',
        consumption: '',
        supplier_location: '',
        substract_stock: 'yes',
        require_shipping: '',
        brand: 0,
        embed: '',
        related_product: '',
        related_product_two: '',
        related_product_three: '',
        parent: '55',
        sort_order: 0,
        post_status: 1,
        product_sku: '',
        id: 118,
        productImage1:
          'https://prabhubhakti.com/resources/media/1576668280_Hanumana_3.jpg',
        productImage2:
          'https://prabhubhakti.com/resources/media/1576668280_Hanumana 2_1.jpg',
        productImage3:
          'https://prabhubhakti.com/resources/media/1576668280_Hanumana 3_2.jpg',
        productImage4:
          'https://prabhubhakti.com/resources/media/1576668280_Hanumana 1_0.jpg',
        productImage5: '',
        productImage6: '',
        spcial_price: 0,
        start_date: '0000-00-00',
        end_date: '0000-00-00',
        status: 0,
      },
      {
        product_id: 119,
        product_name: 'Ganpati ',
        seo_url: 'https://prabhubhakti.com/ganpati-',
        description: 'Ganpati Poster ',
        brief_description: '',
        meta_title: '',
        meta_description: '',
        meta_keyword: '',
        price: 500,
        instant_price: '',
        tax_price: 0,
        model: '',
        treatment: '',
        quantity: 0,
        special_quantity_price: '',
        weight: '',
        tax_class: '',
        minimum_quantity: 1,
        capacity: '',
        product_code: '',
        consumption: '',
        supplier_location: '',
        substract_stock: 'yes',
        require_shipping: '',
        brand: 0,
        embed: '',
        related_product: '',
        related_product_two: '',
        related_product_three: '',
        parent: '55',
        sort_order: 0,
        post_status: 1,
        product_sku: '',
        id: 117,
        productImage1:
          'https://prabhubhakti.com/resources/media/1576668120_Ganpati 3_2.jpg',
        productImage2:
          'https://prabhubhakti.com/resources/media/1576668120_Ganpati 2_1.jpg',
        productImage3:
          'https://prabhubhakti.com/resources/media/1576668120_Ganpati 1_0.jpg',
        productImage4:
          'https://prabhubhakti.com/resources/media/1576668120_Ganpati_3.jpg',
        productImage5: '',
        productImage6: '',
        spcial_price: 0,
        start_date: '0000-00-00',
        end_date: '0000-00-00',
        status: 0,
      },
      {
        product_id: 106,
        product_name: ' Lakshmi Ganesh Gold Coin',
        seo_url: 'https://www.prabhubhakti.com/-lakshmi-ganesh-gold-coin',
        description: 'Lakshmi Ganesh Gold Coin',
        brief_description: '<p>Lakshmi Ganesh Gold Coin</p>\r\n',
        meta_title: 'Lakshmi Ganesh Gold Coin',
        meta_description: 'Lakshmi Ganesh Gold Coin',
        meta_keyword: 'Lakshmi Ganesh Gold Coin',
        price: 1100,
        instant_price: '',
        tax_price: 0,
        model: '',
        treatment: '',
        quantity: 0,
        special_quantity_price: '',
        weight: '',
        tax_class: '',
        minimum_quantity: 1,
        capacity: '',
        product_code: '',
        consumption: '',
        supplier_location: '',
        substract_stock: 'yes',
        require_shipping: 'yes',
        brand: 0,
        embed: '',
        related_product: '',
        related_product_two: '',
        related_product_three: '',
        parent: '0',
        sort_order: 0,
        post_status: 1,
        product_sku: '',
        id: 104,
        productImage1:
          'https://www.prabhubhakti.com/resources/media/1575444324_laxmi1_0.jpg',
        productImage2:
          'https://www.prabhubhakti.com/resources/media/1575444324_laxmi3_2.jpg',
        productImage3:
          'https://www.prabhubhakti.com/resources/media/1575444324_laxmi2_1.jpg',
        productImage4:
          'https://www.prabhubhakti.com/resources/media/1575444324_laxmi4_3.jpg',
        productImage5: '',
        productImage6: '',
        spcial_price: 0,
        start_date: '0000-00-00',
        end_date: '0000-00-00',
        status: 0,
      },
      {
        product_id: 105,
        product_name: 'Tortoise on Plate ',
        seo_url: 'https://www.prabhubhakti.com/tortoise-on-plate-',
        description: 'Tortoise on Plate ',
        brief_description: '',
        meta_title: 'Tortoise on Plate ',
        meta_description: 'Tortoise on Plate ',
        meta_keyword: 'Tortoise on Plate ',
        price: 1100,
        instant_price: '',
        tax_price: 0,
        model: '',
        treatment: '',
        quantity: 0,
        special_quantity_price: '',
        weight: '',
        tax_class: '',
        minimum_quantity: 1,
        capacity: '',
        product_code: '',
        consumption: '',
        supplier_location: '',
        substract_stock: 'yes',
        require_shipping: '',
        brand: 0,
        embed: '',
        related_product: '',
        related_product_two: '',
        related_product_three: '',
        parent: '41',
        sort_order: 0,
        post_status: 1,
        product_sku: '',
        id: 103,
        productImage1:
          'https://www.prabhubhakti.com/resources/media/1575443567_plate1_0.jpg',
        productImage2:
          'https://www.prabhubhakti.com/resources/media/1575443567_plate5_4.jpg',
        productImage3:
          'https://www.prabhubhakti.com/resources/media/1575443567_plate3_2.jpg',
        productImage4:
          'https://www.prabhubhakti.com/resources/media/1575443567_plate4_3.jpg',
        productImage5:
          'https://www.prabhubhakti.com/resources/media/1575443567_plate5_4.jpg',
        productImage6: '',
        spcial_price: 0,
        start_date: '0000-00-00',
        end_date: '0000-00-00',
        status: 0,
      },
      {
        product_id: 104,
        product_name: 'Kodi',
        seo_url: 'https://www.prabhubhakti.com/kodi',
        description: 'Kodi',
        brief_description: '<p>Kodi</p>\r\n',
        meta_title: 'Kodi',
        meta_description: 'Kodi',
        meta_keyword: 'Kodi',
        price: 1100,
        instant_price: '',
        tax_price: 0,
        model: '',
        treatment: '',
        quantity: 0,
        special_quantity_price: '',
        weight: '',
        tax_class: '',
        minimum_quantity: 1,
        capacity: '',
        product_code: '',
        consumption: '',
        supplier_location: '',
        substract_stock: 'yes',
        require_shipping: 'yes',
        brand: 0,
        embed: '',
        related_product: '',
        related_product_two: '',
        related_product_three: '',
        parent: '41',
        sort_order: 0,
        post_status: 1,
        product_sku: '',
        id: 102,
        productImage1:
          'https://www.prabhubhakti.com/resources/media/1575441530_Kodi_0.jpg',
        productImage2:
          'https://www.prabhubhakti.com/resources/media/1575441542_Kodi1_0.jpg',
        productImage3:
          'https://www.prabhubhakti.com/resources/media/1575441553_Kodi2_0.jpg',
        productImage4: '',
        productImage5: '',
        productImage6: '',
        spcial_price: 0,
        start_date: '0000-00-00',
        end_date: '0000-00-00',
        status: 0,
      },
      {
        product_id: 103,
        product_name: 'Kali Kavach',
        seo_url: 'https://www.prabhubhakti.com/kali-kavach',
        description: 'Kali Kavach',
        brief_description: '<p>Kali Kavach</p>\r\n',
        meta_title: 'Kali Kavach',
        meta_description: 'Kali Kavach',
        meta_keyword: 'Kali Kavach',
        price: 1100,
        instant_price: '',
        tax_price: 0,
        model: '',
        treatment: '',
        quantity: 0,
        special_quantity_price: '',
        weight: '',
        tax_class: '',
        minimum_quantity: 1,
        capacity: '',
        product_code: '',
        consumption: '',
        supplier_location: '',
        substract_stock: 'yes',
        require_shipping: 'yes',
        brand: 0,
        embed: '',
        related_product: '',
        related_product_two: '',
        related_product_three: '',
        parent: '41',
        sort_order: 0,
        post_status: 1,
        product_sku: '',
        id: 101,
        productImage1:
          'https://www.prabhubhakti.com/resources/media/1575439496_Kali Kavach1_0.jpg',
        productImage2:
          'https://www.prabhubhakti.com/resources/media/1575439496_Kali Kavach2_1.jpg',
        productImage3:
          'https://www.prabhubhakti.com/resources/media/1575439496_Kali Kavach3_2.jpg',
        productImage4:
          'https://www.prabhubhakti.com/resources/media/1575439496_Kali Kavach4_3.jpg',
        productImage5: '',
        productImage6: '',
        spcial_price: 0,
        start_date: '0000-00-00',
        end_date: '0000-00-00',
        status: 0,
      },
      {
        product_id: 102,
        product_name: 'Kachua ring',
        seo_url: 'https://www.prabhubhakti.com/kachua-ring',
        description: 'Kachua ring',
        brief_description: '<p>Kachua ring</p>\r\n',
        meta_title: 'Kachua ring',
        meta_description: 'Kachua ring',
        meta_keyword: 'Kachua ring',
        price: 1100,
        instant_price: '',
        tax_price: 0,
        model: '',
        treatment: '',
        quantity: 0,
        special_quantity_price: '',
        weight: '',
        tax_class: '',
        minimum_quantity: 1,
        capacity: '',
        product_code: '',
        consumption: '',
        supplier_location: '',
        substract_stock: 'yes',
        require_shipping: 'yes',
        brand: 0,
        embed: '',
        related_product: '',
        related_product_two: '',
        related_product_three: '',
        parent: '41',
        sort_order: 0,
        post_status: 1,
        product_sku: '',
        id: 100,
        productImage1:
          'https://www.prabhubhakti.com/resources/media/1575438870_Kachua ring1_0.jpg',
        productImage2:
          'https://www.prabhubhakti.com/resources/media/1575438870_Kachua ring2_1.jpg',
        productImage3: '',
        productImage4:
          'https://www.prabhubhakti.com/resources/media/1575438870_Kachua ring3_2.jpg',
        productImage5:
          'https://www.prabhubhakti.com/resources/media/1575438870_Kachua ring4_3.jpg',
        productImage6:
          'https://www.prabhubhakti.com/resources/media/1575438870_Kachua ring5_4.jpg',
        spcial_price: 0,
        start_date: '0000-00-00',
        end_date: '0000-00-00',
        status: 0,
      },
      {
        product_id: 101,
        product_name: 'Kuber Kunji',
        seo_url: 'https://www.prabhubhakti.com/kuber-kunji',
        description: 'Kuber Kunji',
        brief_description: '<p>Kuber Kunji</p>\r\n',
        meta_title: 'Kuber Kunji',
        meta_description: 'Kuber Kunji',
        meta_keyword: 'Kuber Kunji',
        price: 1100,
        instant_price: '',
        tax_price: 0,
        model: '',
        treatment: '',
        quantity: 0,
        special_quantity_price: '',
        weight: '',
        tax_class: '',
        minimum_quantity: 1,
        capacity: '',
        product_code: '',
        consumption: '',
        supplier_location: '',
        substract_stock: 'yes',
        require_shipping: '',
        brand: 0,
        embed: '',
        related_product: '',
        related_product_two: '',
        related_product_three: '',
        parent: '41',
        sort_order: 0,
        post_status: 1,
        product_sku: '',
        id: 99,
        productImage1:
          'https://www.prabhubhakti.com/resources/media/1575437880_kuber1_0.jpg',
        productImage2:
          'https://www.prabhubhakti.com/resources/media/1575437893_kuber2_0.jpg',
        productImage3:
          'https://www.prabhubhakti.com/resources/media/1575437910_kuber3_0.jpg',
        productImage4:
          'https://www.prabhubhakti.com/resources/media/1575437925_kuber4_0.jpg',
        productImage5:
          'https://www.prabhubhakti.com/resources/media/1575437939_kuber5_0.jpg',
        productImage6: '',
        spcial_price: 0,
        start_date: '0000-00-00',
        end_date: '0000-00-00',
        status: 0,
      },
      {
        product_id: 100,
        product_name: 'Laxmi Paduka',
        seo_url: 'https://www.prabhubhakti.com/laxmi-paduka',
        description: 'Laxmi Paduka',
        brief_description: '<p>Laxmi Paduka</p>\r\n',
        meta_title: 'Laxmi Paduka',
        meta_description: 'Laxmi Paduka',
        meta_keyword: '',
        price: 501,
        instant_price: '',
        tax_price: 0,
        model: '',
        treatment: '',
        quantity: 0,
        special_quantity_price: '',
        weight: '',
        tax_class: '',
        minimum_quantity: 1,
        capacity: '',
        product_code: '',
        consumption: '',
        supplier_location: '',
        substract_stock: 'yes',
        require_shipping: '',
        brand: 0,
        embed: '',
        related_product: '',
        related_product_two: '',
        related_product_three: '',
        parent: '41',
        sort_order: 0,
        post_status: 1,
        product_sku: '',
        id: 98,
        productImage1:
          'https://www.prabhubhakti.com/resources/media/1575437277_laxmi1_0.jpg',
        productImage2:
          'https://www.prabhubhakti.com/resources/media/1575437293_laxmi2_0.jpg',
        productImage3:
          'https://www.prabhubhakti.com/resources/media/1575437305_laxmi3_0.jpg',
        productImage4:
          'https://www.prabhubhakti.com/resources/media/1575437318_laxmi4_0.jpg',
        productImage5:
          'https://www.prabhubhakti.com/resources/media/1575437332_laxmi5_0.jpg',
        productImage6: '',
        spcial_price: 0,
        start_date: '0000-00-00',
        end_date: '0000-00-00',
        status: 0,
      },
      {
        product_id: 99,
        product_name: 'kamal gatta',
        seo_url: 'https://www.prabhubhakti.com/kamal-gatta',
        description: 'kamal gatta',
        brief_description:
          '<p><a href="https://www.google.com/search?rlz=1C1CHBD_enIN833IN833&amp;sxsrf=ACYBGNQYBcljstOllFyPpf5TtcsRQ1NhWw:1575360283703&amp;q=kamal+gatta&amp;tbm=isch&amp;source=univ&amp;sa=X&amp;ved=2ahUKEwjMwOmwgpnmAhXcIbcAHdftAYQQsAR6BAgKEAE">kamal gatta</a></p>\r\n',
        meta_title: '',
        meta_description: '',
        meta_keyword: '',
        price: 500,
        instant_price: '',
        tax_price: 0,
        model: '',
        treatment: '',
        quantity: 0,
        special_quantity_price: '',
        weight: '',
        tax_class: '',
        minimum_quantity: 1,
        capacity: '',
        product_code: '',
        consumption: '',
        supplier_location: '',
        substract_stock: 'yes',
        require_shipping: '',
        brand: 0,
        embed: '',
        related_product: '',
        related_product_two: '',
        related_product_three: '',
        parent: '51',
        sort_order: 0,
        post_status: 1,
        product_sku: '',
        id: 97,
        productImage1:
          'https://www.prabhubhakti.com/resources/media/1575360366_kamal_gatta_0.jpg',
        productImage2:
          'https://www.prabhubhakti.com/resources/media/1575360355_kamal_gatta_1_0.jpg',
        productImage3: '',
        productImage4: '',
        productImage5: '',
        productImage6: '',
        spcial_price: 0,
        start_date: '0000-00-00',
        end_date: '0000-00-00',
        status: 0,
      },
      {
        product_id: 98,
        product_name: 'kodi Kachua',
        seo_url: 'https://www.prabhubhakti.com/kodi-kachua',
        description: '',
        brief_description: '',
        meta_title: '',
        meta_description: '',
        meta_keyword: '',
        price: 1500,
        instant_price: '',
        tax_price: 0,
        model: '',
        treatment: '',
        quantity: 0,
        special_quantity_price: '',
        weight: '',
        tax_class: '',
        minimum_quantity: 1,
        capacity: '',
        product_code: '',
        consumption: '',
        supplier_location: '',
        substract_stock: 'yes',
        require_shipping: '',
        brand: 0,
        embed: '',
        related_product: '',
        related_product_two: '',
        related_product_three: '',
        parent: '41',
        sort_order: 0,
        post_status: 1,
        product_sku: '',
        id: 96,
        productImage1:
          'https://www.prabhubhakti.com/resources/media/1575439137_Kodi Kachua1_0.jpg',
        productImage2:
          'https://www.prabhubhakti.com/resources/media/1575439137_Kodi Kachua2_1.jpg',
        productImage3:
          'https://www.prabhubhakti.com/resources/media/1575439137_Kodi Kachua3_2.jpg',
        productImage4: '',
        productImage5: '',
        productImage6: '',
        spcial_price: 0,
        start_date: '0000-00-00',
        end_date: '0000-00-00',
        status: 0,
      },
      {
        product_id: 97,
        product_name: 'Indra kavach',
        seo_url: 'https://www.prabhubhakti.com/indra-kavach',
        description: '',
        brief_description: '',
        meta_title: '',
        meta_description: '',
        meta_keyword: '',
        price: 1100,
        instant_price: '',
        tax_price: 0,
        model: '',
        treatment: '',
        quantity: 0,
        special_quantity_price: '',
        weight: '',
        tax_class: '',
        minimum_quantity: 1,
        capacity: '',
        product_code: '',
        consumption: '',
        supplier_location: '',
        substract_stock: 'yes',
        require_shipping: '',
        brand: 0,
        embed: '',
        related_product: '',
        related_product_two: '',
        related_product_three: '',
        parent: '41',
        sort_order: 0,
        post_status: 1,
        product_sku: '',
        id: 95,
        productImage1:
          'https://www.prabhubhakti.com/resources/media/1575359326_i1_0.jpg',
        productImage2:
          'https://www.prabhubhakti.com/resources/media/1575359342_i2_0.jpg',
        productImage3: '',
        productImage4: '',
        productImage5: '',
        productImage6: '',
        spcial_price: 0,
        start_date: '0000-00-00',
        end_date: '0000-00-00',
        status: 0,
      },
    ],
  };

  componentDidMount() {
    this.checkPermission();
    this.getToken();
    this.createNotificationListeners();
  }

  async checkPermission() {
    const enabled = await firebase.messaging().hasPermission();
    if (enabled) {
      // alert('Yes');
    } else {
      try {
        await firebase.messaging().requestPermission();
        Alert('Granted');
      } catch (error) {
        Alert('Rejected');
      }
    }
  }

  async getToken() {
    const fcmToken = await firebase.messaging().getToken();
    if (fcmToken) {
      this.setState({fcmToken: fcmToken});
      console.log(`FCM: ${fcmToken}`);
    } else {
      // user doesn't have a device token yet
    }
  }

  async requestCameraPermission() {
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.CAMERA,
        {
          title: 'Cool Photo App Camera Permission',
          message:
            'Cool Photo App needs access to your camera ' +
            'so you can take awesome pictures.',
          buttonNeutral: 'Ask Me Later',
          buttonNegative: 'Cancel',
          buttonPositive: 'OK',
        },
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        console.log('You can use the camera');
      } else {
        console.log('Camera permission denied');
      }
    } catch (err) {
      console.warn(err);
    }
  }

  async createNotificationListeners() {
    /*
     * Triggered when a particular notification has been received in foreground
     * */
    this.notificationListener = firebase
      .notifications()
      .onNotification(notification => {
        console.log('onNotification:');

        const localNotification = new firebase.notifications.Notification({
          sound: 'sampleaudio',
          show_in_foreground: true,
        })
          .setSound('sampleaudio')
          .setNotificationId(notification.notificationId)
          .setTitle(notification.title)
          .setBody(notification.body)
          .setData(notification.data)
          .android.setChannelId('fcm_FirebaseNotifiction_default_channel') // e.g. the id you chose above
          .android.setSmallIcon('@drawable/ic_launcher') // create this icon in Android Studio
          .android.setColor('#000000') // you can set a color here
          .android.setPriority(firebase.notifications.Android.Priority.High);

        firebase
          .notifications()
          .displayNotification(localNotification)
          .catch(err => console.error(err));
      });

    const channel = new firebase.notifications.Android.Channel(
      'fcm_FirebaseNotifiction_default_channel',
      'Demo app name',
      firebase.notifications.Android.Importance.High,
    )
      .setDescription('Demo app description')
      .setSound('sampleaudio');
    firebase.notifications().android.createChannel(channel);

    /*
     * If your app is in background, you can listen for when a notification is clicked / tapped / opened as follows:
     * */
    this.notificationOpenedListener = firebase
      .notifications()
      .onNotificationOpened(notificationOpen => {
        const {title, body, data} = notificationOpen.notification;
        console.log('onNotificationOpened:' + data.link);
        Alert.alert(title, body + ', ' + data.link);
        // alert(JSON.stringify(notificationOpen.notification));
        console.log(notificationOpen.notification);
        // console.log(body);
        // this.getDataByLink(body).catch(err => {
        //   this.props.navigation.navigate('Feed');
        //   console.log(err);
        // });
      });

    /*
     * If your app is closed, you can check if it was opened by a notification being clicked / tapped / opened as follows:
     * */
    const notificationOpen = await firebase
      .notifications()
      .getInitialNotification();
    if (notificationOpen) {
      const {title, body} = notificationOpen.notification;
      console.log('getInitialNotification:');
      Alert.alert(title, body);
      console.log('notification body: ' + body);
      console.log(notificationOpen.notification);
      // this.getDataByLink(body).catch(err => {
      //   this.props.navigation.navigate('Feed');
      //   console.log(err);
      // });
    }
    /*
     * Triggered for data only payload in foreground
     * */
    this.messageListener = firebase.messaging().onMessage(message => {
      //process data message
      console.log('JSON.stringify:', JSON.stringify(message));
    });
  }

  call = () => {
    let phoneNumber = 7840027154;
    let callLink = `tel:${phoneNumber}`;

    Linking.canOpenURL(callLink)
      .then(supported => {
        if (!supported) {
          Alert.alert('Phone number is not available');
        } else {
          return Linking.openURL(callLink);
        }
      })
      .catch(err => console.log(err));
  };

  onRefresh = () => {
    this.setState({
      refreshing: true,
      random: Math.floor(Math.random() * 10 + 1),
    });

    setTimeout(() => {
      this.setState({refreshing: false});
    }, 200);
  };

  render() {
    return (
      <>
        <ScrollView>
          <View
            style={styles.container}
            refreshControl={
              <RefreshControl
                refreshing={this.state.refreshing}
                onRefresh={this.onRefresh.bind(this)}
              />
            }>
            <Text style={styles.welcome}>Welcome to React Native!</Text>
            <Text style={styles.instructions}>To get started, edit App.js</Text>
            <Text style={styles.instructions}>{instructions}</Text>
            <Button title="Call" onPress={() => this.call()} />
            <Text style={{justifyContent: 'center', textAlign: 'center'}}>
              {this.state.fcmToken}
            </Text>
          </View>
        </ScrollView>
      </>
    );
  }
}
