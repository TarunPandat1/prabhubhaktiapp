import React, {Component} from 'react';
import {
  View,
  Text,
  Image,
  TouchableOpacity,
  StyleSheet,
  FlatList,
  ScrollView,
  ToastAndroid,
  RefreshControl,
} from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';
import HeaderIcon from './header';
import {connect} from 'react-redux';
import {RadioButton} from 'react-native-paper';

var data = [];

class ShoppingCart extends Component {
  constructor(props) {
    super(props);
    this.state = {
      text: '',
      products: [],
      grandTotal: 0,
      product_details: '',
      checked: 'cod',
      onlineDisabled: true,
      refreshing: false,
    };
  }

  getGrandTotal = () => {
    let grandTotal = 0;
    for (var i = 0; i < this.props.counter.length; i++) {
      grandTotal += this.props.counter[i].quty_price;
    }
    this.setState({
      grandTotal: grandTotal,
    });
    data = [];
    for (var i = 0; i < this.props.counter.length; i++) {
      let ex = {
        hidden_product_name: this.props.counter[i].product_name,
        productId: this.props.counter[i].product_id.toString(),
        product_sku: this.props.counter[i].product_sku,
        quantity: this.props.counter[i].quantity.toString(),
        hidden_unit_price: this.props.counter[i].price.toString(),
        hidden_total: this.state.grandTotal.toString(),
        total_pr_price: this.props.counter[i].quty_price.toString(),
        productImage: this.props.counter[i].productImage1,
      };
      data.push(JSON.stringify(ex));

      this.setState({
        product_details: data,
      });
    }
  };
  componentDidMount() {
    console.log(this.props.counter);
  }

  timeInterval = setInterval(() => {
    this.getGrandTotal();
  }, 1000);

  productQuantity = (productId, type) => {
    const {username, mobile, macAddress, ip} = global;

    let url = `${ip}productQuantity?id=${macAddress}&productId=${productId}&user=${username}&mobile=${mobile}&type=${type}`;
    console.log(url);

    fetch(url)
      .then(response => response.json())
      .then(responseJson => {
        if (responseJson == '1') {
          this.props.CartCounting();
        } else {
          ToastAndroid.showWithGravity(
            'Something Went Wrong..!',
            ToastAndroid.LONG,
            ToastAndroid.BOTTOM,
          );
        }
      })
      .catch(err => {
        console.log(err);
        ToastAndroid.showWithGravity(
          'Something Went Wrong..!' + err,
          ToastAndroid.LONG,
          ToastAndroid.BOTTOM,
        );
      });
  };

  products = item => {
    return (
      <View style={styles.row}>
        <View style={(styles.col1, {padding: 10})}>
          <Image
            source={{uri: item.productImage1}}
            style={{height: 80, width: 80}}
          />
        </View>
        <View
          style={
            (styles.col1,
            {
              flexDirection: 'column',
              justifyContent: 'center',
              width: 150,
            })
          }>
          <Text>{item.product_name}</Text>
          <Text>Product Price: ₹{item.price}</Text>
          <Text>Total Price: ₹{item.quty_price}</Text>
        </View>
        <View style={styles.col1}>
          <View style={styles.row}>
            <View
              style={
                (styles.col1,
                {justifyContent: 'center', alignItems: 'center', margin: 10})
              }>
              <TouchableOpacity
                onPress={() => {
                  this.productQuantity(item.product_id, 'decrement');
                }}>
                <View>
                  <Ionicons
                    name="ios-remove-circle-outline"
                    size={25}
                    style={{color: 'green'}}
                  />
                </View>
              </TouchableOpacity>
            </View>
            <View
              style={
                (styles.col2,
                {justifyContent: 'center', alignItems: 'center', margin: 10})
              }>
              <Text>{item.quantity}</Text>
            </View>
            <View
              style={
                (styles.col1,
                {justifyContent: 'center', alignItems: 'center', margin: 10})
              }>
              <TouchableOpacity
                onPress={() => {
                  this.productQuantity(item.product_id, 'increment');
                }}>
                <View>
                  <Ionicons
                    name="ios-add-circle-outline"
                    size={25}
                    style={{color: 'red'}}
                  />
                </View>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </View>
    );
  };

  spearator = () => {
    return <View style={{height: 1, backgroundColor: 'rgba(0,0,0,0.1)'}} />;
  };

  footer = () => {
    return (
      <View
        style={(styles.row, {justifyContent: 'center', alignItems: 'center'})}>
        <Text style={{color: 'green'}}>
          Grand Total, You have to Pay ₹{this.state.grandTotal}
        </Text>
      </View>
    );
  };

  buy = method => {
    if (method == 'online') {
      alert(`Sorry.., Can't help you at the momment..!`);
    } else {
      let password = mobile;
      let altmobile = '';
      let discount = 0;
      let hidden_subtotal = this.state.grandTotal;
      let hidden_total = hidden_subtotal - discount;
      let shipping_price = '';
      let delivery_type = 0;

      let url = `${ip}buyProduct?user=${username}&email=${email}&password=${password}&mobile=${mobile}&altmobile=${altmobile}&city=${city}&pincode=${pincode}&address=${address1}, ${address2}&country=${country}&state=${state}&hidden_subtotal=${hidden_subtotal}&shipping_price=${shipping_price}&discount=${discount}&hidden_total=${hidden_total}&method=${method}&mode_of_payment=${method}&product_details=${[
        data,
      ]}&delivery_type=${delivery_type}&id=${macAddress}`;

      fetch(url)
        .then(response => response.json())
        .then(responseJson => {
          if (responseJson == '1') {
            this.props.CartCounting();
            this.props.OrderHistory();
            this.props.navigation.navigate('Thanksup');
          }
        });
    }
  };

  onRefresh = () => {
    this.setState({refreshing: true});
    this.props.CartCounting();
    setTimeout(() => {
      this.setState({refreshing: false});
    }, 1000);
  };

  render() {
    const {checked} = this.state;

    return (
      <View style={{backgroundColor: 'white', flex: 1}}>
        <HeaderIcon navigation={this.props.navigation} />
        <View style={{flex: 0.93, backgroundColor: 'white'}}>
          <ScrollView style={{flex: 100}}>
            {this.props.counter.length == 0 ? (
              <>
                <Text
                  style={{
                    flex: 1,
                    justifyContent: 'center',
                    textAlign: 'center',
                    marginTop: 100,
                    fontSize: 20,
                  }}>
                  No Items in your Cart, Please add some..!
                </Text>
                <View
                  style={{
                    flex: 1,
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}>
                  <TouchableOpacity
                    onPress={() => this.props.navigation.navigate('Product')}>
                    <Text
                      style={{
                        padding: 15,
                        backgroundColor: '#ff7604',
                        color: 'white',
                        marginTop: 100,
                      }}>
                      Continue Shopping
                    </Text>
                  </TouchableOpacity>
                </View>
              </>
            ) : (
              <>
                <View style={{flex: 6, backgroundColor: 'white'}}></View>
                <View style={{flex: 88}}>
                  <View
                    style={{
                      borderColor: 'rgba(255, 255, 255, 0.3)',
                      borderWidth: 2,
                    }}>
                    <FlatList
                      refreshControl={
                        <RefreshControl
                          refreshing={this.state.refreshing}
                          onRefresh={this.onRefresh.bind(this)}
                        />
                      }
                      data={this.props.counter}
                      keyExtractor={(item, index) => index.toString()}
                      renderItem={({item}) => this.products(item)}
                      ItemSeparatorComponent={this.spearator}
                      ListFooterComponent={this.footer}
                    />
                  </View>
                  <View
                    style={{
                      borderColor: 'rgba(255, 255, 255, 0.3)',
                      borderWidth: 2,
                    }}>
                    <View style={{flexDirection: 'row'}}>
                      <View
                        style={{
                          flex: 2,
                          padding: 20,
                          borderColor: '#dadada',
                          borderWidth: 4,
                        }}>
                        <Text style={{fontSize: 18}}>Address Detail </Text>
                        {username ? (
                          <View style={{marginLeft: 15}}>
                            <Text>Address: {`${address1}, ${address2}`}</Text>
                            <Text>{`${city}, ${state}`}</Text>
                            <Text>{`${pincode}`}</Text>
                            <Text>{`${username}, Ph:.${mobile}`}</Text>
                          </View>
                        ) : (
                          <>
                            <View
                              style={{
                                flex: 1,
                                justifyContent: 'center',
                                alignItems: 'center',
                              }}>
                              <Text style={{color: 'red', margin: 10}}>
                                Please Create Your Account to PrabhuBhakti..!,
                                Go to SignUp.
                              </Text>
                            </View>
                          </>
                        )}
                      </View>
                    </View>
                  </View>
                  <View
                    style={{
                      borderColor: 'rgba(255, 255, 255, 0.3)',
                      borderWidth: 2,
                    }}>
                    <View
                      style={{
                        flexDirection: 'row',
                        alignItems: 'center',
                        justifyContent: 'center',
                        marginTop: 20,
                      }}>
                      <TouchableOpacity style={{height: 100, width: 150}}>
                        <Text style={{color: '#09AA06'}}>
                          This product is only for Cash On Delivery
                        </Text>
                      </TouchableOpacity>
                    </View>
                  </View>
                </View>

                <View
                  style={{
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}>
                  <View
                    style={{
                      flexDirection: 'row',
                      flex: 1,
                      borderColor: '#dadada',
                      borderWidth: 2,
                      borderTopWidth: 4,
                    }}>
                    <View style={styles.homeicon}>
                      <RadioButton
                        value="cod"
                        status={checked === 'cod' ? 'checked' : 'unchecked'}
                        onPress={() => {
                          this.setState({checked: 'cod'});
                        }}
                      />
                    </View>

                    <View style={styles.col1}>
                      <TouchableOpacity
                        onPress={() => this.setState({checked: 'cod'})}>
                        <Text style={styles.homtxt}>Cash On Delivery</Text>
                      </TouchableOpacity>
                    </View>
                  </View>

                  <View
                    style={{
                      flexDirection: 'row',
                      flex: 1,
                      borderColor: '#dadada',
                      borderWidth: 2,
                      borderBottomWidth: 4,
                    }}>
                    <View style={styles.homeicon}>
                      <RadioButton
                        value="online"
                        status={checked === 'online' ? 'checked' : 'unchecked'}
                        onPress={() => {
                          this.setState({checked: 'online'});
                        }}
                        disabled={this.state.onlineDisabled}
                      />
                    </View>

                    <View style={styles.col1}>
                      <TouchableOpacity
                        onPress={() => {
                          this.state.onlineDisabled ||
                            this.setState({checked: 'online'});
                        }}>
                        <View styles={styles.row}>
                          <Text style={styles.homtxt}>Online Payment</Text>
                        </View>
                      </TouchableOpacity>
                    </View>
                  </View>
                </View>
              </>
            )}
          </ScrollView>
        </View>
        <View style={{flex: 0.07}}>
          <View style={{flex: 6, backgroundColor: 'white'}}>
            <TouchableOpacity
              style={styles.cart}
              onPress={() => {
                username
                  ? this.buy(checked)
                  : this.props.navigation.navigate('SignUp');
              }}
              title="Continued">
              <Text style={{color: 'white'}}>CONTINUED</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  img: {
    flex: 0.5,
    backgroundColor: '#fff',
    height: 300,
    flexDirection: 'column',
    marginBottom: 10,
    padding: 5,
  },
  cart: {
    height: 50,
    width: '100%',
    borderColor: 'rgba(100,100,100,0.3)',
    borderWidth: 3,
    backgroundColor: '#ff7604',
    alignItems: 'center',
    justifyContent: 'center',
  },
  btn: {
    height: 58,
    width: 360,
  },
  row: {
    flexDirection: 'row',
    flex: 1,
  },
  col1: {
    flex: 1,
  },
  col2: {
    flex: 2,
  },
  col3: {
    flex: 3,
  },
  col4: {
    flex: 4,
  },
  col5: {
    flex: 5,
  },
  hom: {
    flexDirection: 'row',
    flex: 1,
    borderColor: '#dadada',
    borderWidth: 2,
  },
  homtxt: {
    color: '#09AA06',
    fontSize: 20,
  },
  homeicon: {
    flex: 0.4,
    alignItems: 'center',
    justifyContent: 'center',
  },
});

function mapStateToProps(state) {
  return {
    counter: state.cart,
    orders: state.orderHistory,
  };
}

const cartAPI = () => {
  return dispatch => {
    fetch(
      `${global.ip}cart?id=${global.macAddress}&user=${global.username}&mobile=${global.mobile}`,
    )
      .then(response => response.json())
      .then(responseJson => {
        // console.log(responseJson);
        dispatch({type: 'CartCounting', data: responseJson});
      })
      .catch(err => console.log(err));
  };
};

const orderHistoryAPI = () => {
  return dispatch => {
    fetch(
      `${global.ip}orderHistory?id=${global.macAddress}&user=${global.username}&mobile=${global.mobile}`,
    )
      .then(response => response.json())
      .then(responseJson => {
        // console.log(responseJson);
        dispatch({type: 'OrderHistory', orders: responseJson});
      })
      .catch(err => console.log(err));
  };
};

function mapDispatchToProps(dispatch) {
  return {
    CartCounting: () => dispatch(cartAPI()),
    OrderHistory: () => dispatch(orderHistoryAPI()),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(ShoppingCart);
