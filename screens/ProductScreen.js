import React, {Component} from 'react';
import {
  Text,
  StyleSheet,
  View,
  TouchableOpacity,
  ScrollView,
  ToastAndroid,
} from 'react-native';
import {SliderBox} from 'react-native-image-slider-box';
import HeaderIcon from './header';
import {connect} from 'react-redux';
import Ionicons from 'react-native-vector-icons/Ionicons';

class ProductScreen extends Component {
  state = {
    images: [],
  };

  addToCart = async () => {
    var sprice = this.props.navigation.getParam('special_price');
    var oprice = this.props.navigation.getParam('price');
    var price = oprice;
    if (sprice != '0') price = sprice;
    global.getData();
    global.getMacAddress();
    const productId = this.props.navigation.getParam('productId');
    const sku = this.props.navigation.getParam('sku');

    const url = `${global.ip}addToCart?id=${global.macAddress}&productId=${productId}&sku=${sku}&price=${price}&user=${global.username}&mobile=${global.mobile}`;
    console.log(url);
    const response = await fetch(url);
    const data = await response.json();
    if (data == '1') {
      this.props.CartCounting();
      ToastAndroid.showWithGravity(
        'Item Added to your Cart..!',
        ToastAndroid.LONG,
        ToastAndroid.BOTTOM,
      );
    } else {
      ToastAndroid.showWithGravity(
        'Something Went Wrong..!',
        ToastAndroid.LONG,
        ToastAndroid.BOTTOM,
      );
    }
  };

  buyNow = async () => {
    this.addToCart()
      .then(() => {
        this.props.navigation.navigate('ShoppingCart');
      })
      .catch(err => {
        ToastAndroid.showWithGravity(
          'Something Went Wrong..!',
          ToastAndroid.LONG,
          ToastAndroid.BOTTOM,
        );
      });
  };

  render() {
    return (
      <View style={{flexDirection: 'column', flex: 1}}>
        <HeaderIcon navigation={this.props.navigation} />
        <View style={{flex: 0.9999}}>
          <ScrollView>
            <SliderBox
              images={[
                this.props.navigation.getParam('image1'),
                this.props.navigation.getParam('image2'),
                this.props.navigation.getParam('image3'),
                this.props.navigation.getParam('image4'),
                this.props.navigation.getParam('image5'),
              ]}
              sliderBoxHeight={450}
              onCurrentImagePressed={index =>
                console.warn(`image ${index} pressed`)
              }
              dotColor="#ff7604"
              inactiveDotColor="#90A4AE"
              resizeMode={'stretch'}
            />
            <View
              style={{
                marginTop: 20,
                PaddingTop: 20,
                backgroundColor: '#fff',
              }}>
              <Text style={{fontSize: 16, fontWeight: 'bold', marginLeft: 12}}>
                {this.props.navigation.getParam('product_name')}
              </Text>
              <Text style={{fontSize: 14, margin: 15}}>
                Price:{' '}
                {this.props.navigation.getParam('special_price') != '0' ? (
                  <>
                    {this.props.navigation.getParam('special_price')}{' '}
                    <Text
                      style={{
                        color: 'rgba(0,0,0,0.5)',
                        textDecorationLine: 'line-through',
                      }}>
                      {this.props.navigation.getParam('price')}
                    </Text>
                  </>
                ) : (
                  this.props.navigation.getParam('price')
                )}
                <Text style={{fontWeight: 'bold'}}>
                  {this.props.navigation.getParam('special_price') != '0' ? (
                    <>
                      {' '}
                      {100 -
                        Math.round(
                          (this.props.navigation.getParam('special_price') /
                            this.props.navigation.getParam('price')) *
                            100,
                        )}
                      % OFF{' '}
                    </>
                  ) : null}
                </Text>
              </Text>
              <View
                style={{
                  borderColor: 'rgba(0,0,0,0.1)',
                  borderWidth: 4,
                  borderTopWidth: 8,
                }}>
                <Text style={{fontSize: 16, margin: 5, fontWeight: 'bold'}}>
                  Product Description:
                </Text>
                <Text
                  style={{
                    fontSize: 16,
                    marginLeft: 7,
                    color: 'rgba(0,0,0,0.8)',
                  }}>
                  {this.props.navigation.getParam('description')}
                </Text>
              </View>
              <View
                style={{
                  borderColor: 'rgba(0,0,0,0.1)',
                  borderWidth: 4,
                }}>
                <View>
                  <Text
                    style={{fontSize: 16, fontWeight: 'bold', paddingLeft: 5}}>
                    Delivery Details
                  </Text>
                  <View style={{padding: 20}}>
                    <Text style={{marginLeft: 5, marginRight: 5}}>
                      <Ionicons
                        name="ios-arrow-forward"
                        size={12}
                        color="skyblue"
                      />{' '}
                      We deliver your order within 1-2 days of placing it across
                      india.{' '}
                    </Text>
                    <Text style={{marginLeft: 5, marginRight: 5}}>
                      <Ionicons
                        name="ios-arrow-forward"
                        size={12}
                        color="skyblue"
                      />{' '}
                      Cash on Delivery available. Get special disc
                    </Text>
                  </View>
                  <View style={{flexDirection: 'row', flex: 1, marginTop: 15}}>
                    <View style={styles.homebx}>
                      <View style={styles.homebox}>
                        <View>
                          <Ionicons
                            name="ios-rocket"
                            size={30}
                            color="skyblue"
                          />
                        </View>
                        <View style={{marginLeft: 8}}>
                          <Text style={styles.hometxt}>Secure</Text>
                          <Text style={styles.hometxt}>Payment</Text>
                        </View>
                      </View>
                    </View>
                    <View style={styles.homebx}>
                      <View style={styles.homebox}>
                        <View style={{}}>
                          <Ionicons
                            name="ios-folder"
                            size={30}
                            color="skyblue"
                          />
                        </View>
                        <View style={{marginLeft: 8}}>
                          <Text style={styles.hometxt}>Genuine</Text>
                          <Text style={styles.hometxt}>Product</Text>
                        </View>
                      </View>
                    </View>
                    <View style={styles.homebx}>
                      <View style={styles.homebox}>
                        <View style={{marginLeft: 10}}>
                          <Ionicons
                            name="ios-phone-portrait"
                            size={30}
                            color="skyblue"
                          />
                        </View>
                        <View style={{marginLeft: 8}}>
                          <Text style={styles.hometxt}>Excellent</Text>
                          <Text style={styles.hometxt}>Customer</Text>
                          <Text style={styles.hometxt}>Support</Text>
                        </View>
                      </View>
                    </View>
                  </View>
                </View>
              </View>
            </View>
          </ScrollView>
        </View>
        <View Style={{flex: 0.0001}}>
          <View style={{flexDirection: 'row'}}>
            <View style={{flex: 0.5}}>
              <TouchableOpacity
                style={styles.cart}
                onPress={() => this.addToCart()}>
                <Text style={{color: 'white'}}>Add To Cart</Text>
              </TouchableOpacity>
            </View>
            <View style={{flex: 0.5}}>
              <TouchableOpacity
                style={styles.cart}
                onPress={() => this.buyNow()}>
                <Text style={{color: 'white'}}>Buy Now</Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  img: {
    flex: 0.5,
    backgroundColor: '#fff',
    height: 300,
    flexDirection: 'column',
    marginBottom: 10,
    padding: 5,
  },
  cart: {
    height: 50,
    width: '100%',
    borderColor: 'rgba(100,100,100,0.3)',
    borderWidth: 3,
    backgroundColor: '#ff7604',
    alignItems: 'center',
    justifyContent: 'center',
  },
  hometxt: {fontWeight: 'bold', fontSize: 10},
  homebox: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    height: 80,
  },
  homebx: {
    flex: 0.33,
    backgroundColor: 'rgb(230,230,230)',
    borderColor: 'white',
    borderWidth: 4,
  },
});

const cartAPI = () => {
  return dispatch => {
    fetch(
      `${global.ip}cart?id=${global.macAddress}&user=${global.username}&mobile=${global.mobile}`,
    )
      .then(response => response.json())
      .then(responseJson => {
        console.log(responseJson);
        dispatch({type: 'CartCounting', data: responseJson});
      })
      .catch(err => console.log(err));
  };
};

function mapDispatchToProps(dispatch) {
  return {
    CartCounting: () => dispatch(cartAPI()),
  };
}

export default connect(null, mapDispatchToProps)(ProductScreen);
