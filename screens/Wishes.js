import React, {Component} from 'react';
import {
  Modal,
  StyleSheet,
  Text,
  TouchableHighlight,
  TouchableWithoutFeedback,
  ImageBackground,
  ScrollView,
  Button,
  Image,
  View,
  Alert,
  TextInput,
} from 'react-native';
import StarRating from 'react-native-star-rating';
//import {ScrollView} from 'react-native-gesture-handler';

export default class Wishes extends Component {
  constructor(props) {
    super(props);
    this.state = {TextInput_Name: ''};
  }

  Send_Data_Function = () => {
    this.props.navigation.navigate('Wishhome', {
      NameOBJ: this.state.TextInput_Name,
    });
  };

  render() {
    return (
      <View style={{flexDirection: 'column', flex: 1}}>
        <ScrollView>
          <View
            style={{
              height: 50,
              alignItems: 'center',
              justifyContent: 'center',
            }}>
            <Image
              source={require('../image/logo.png')}
              style={{height: 40, width: 150}}
              resizeMode="stretch"
            />
          </View>
          <View>
            <Text
              style={{
                fontSize: 30,
                color: '#ff7604',
                textAlign: 'center',
                paddingBottom: 15,
              }}>
              ॐ हूं हनुमते नमः ॥
            </Text>
          </View>
          <View style={{height: 570, backgroundColor: 'pink'}}>
            <ImageBackground
              source={require('../image/han2.jpg')}
              style={{width: '100%', height: '100%'}}
              resizeMode="stretch"></ImageBackground>
          </View>
          {/* <View style={{height: 80, backgroundColor: 'yellow'}}>
            <Text
              style={{
                fontSize: 20,
                textAlign: 'center',
              }}>
              ने आपको हनुमान जी का प्रसाद भेजा है ॥
            </Text>
          </View> */}
          <View style={{height: 70, marginTop: 30, marginBottom: 20}}>
            <Text
              style={{
                fontSize: 30,
                textAlign: 'center',
              }}>
              - अब आप भी हनुमान जी की पूजा कर अपने दोस्तों{' '}
              <Text>को प्रसाद भेजें</Text>
            </Text>
          </View>
        </ScrollView>
        <View
          style={{alignItems: 'center', justifyContent: 'center', padding: 5}}>
          <TextInput
            style={{
              height: 40,
              borderColor: 'gray',
              borderWidth: 1,
              borderRadius: 20,
              width: 200,
            }}
            placeholder="Enter Name here"
            onChangeText={data => this.setState({TextInput_Name: data})}
            style={styles.textInputStyle}
            underlineColorAndroid="transparent"
          />
          {/* <TextInput
            style={{
              height: 40,
              borderColor: 'gray',
              borderWidth: 1,
              borderRadius: 20,
              width: 200,
            }}
            onChangeText={text => this.setState({text})}
            value={this.state.text}
          /> */}
        </View>
        <View
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'center',
          }}>
          <TouchableWithoutFeedback onPress={this.Send_Data_Function}>
            <Text
              style={{
                fontSize: 25,
                fontWeight: 'bold',
                color: 'red',
                backgroundColor: '#ff7604',
                borderRadius: 20,
                width: 200,
                textAlign: 'center',
                padding: 15,
              }}>
              पूजा आरंभ करें
            </Text>
          </TouchableWithoutFeedback>
        </View>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  card: {
    backgroundColor: 'red',
    width: '100%',
    marginHorizontal: 10,
    marginTop: 24,
    flex: 0.5,
  },
});
