import React, {Component} from 'react';
import {
  View,
  Text,
  Image,
  StyleSheet,
  ScrollView,
  TouchableOpacity,
  Share,
  FlatList,
  TouchableWithoutFeedback,
} from 'react-native';
import Icon from 'react-native-ionicons';
import Carousel from 'react-native-snap-carousel';
import DynamicHomeScreen from './DynamicHomeScreen';
import AsyncStorage from '@react-native-community/async-storage';
import HeaderIcon from './header';
import {Posters, Products} from '../Components';

export default class Lokpriya extends Component {
  static navigationOptions = {
    title: 'Lokpriya',
    headerRight: () => <View />,
  };

  state = {
    relatedPost: [],
    postLikes: 0,
    username: '',
    mobile: '',
    like: null,
    productsByPost: [],
    postersByPost: [],
    headerShown: this.props.navigation.getParam('headerShown', false),
  };

  Interval = setInterval(async () => {
    this.setState({
      username: await AsyncStorage.getItem('username'),
      mobile: await AsyncStorage.getItem('mobile'),
    });
  }, 1000);

  handleScroll(event) {
    console.log(event.nativeEvent.contentOffset.y);
  }

  like = async (id, title) => {
    if (this.state.username && this.state.mobile) {
    } else {
      this.props.navigation.navigate('Login');
    }
    const response = await fetch(
      global.ip +
        'likePost?post_id=' +
        id +
        '&title=' +
        title +
        '&user=' +
        this.state.username +
        '&mobile=' +
        this.state.mobile,
    );
    const data = await response.json();
    if (data == 1) {
      this.likes(id);
    } else {
      alert('Not Like');
    }
  };

  unlike = async (id, title) => {
    const response = await fetch(
      global.ip +
        'unlikePost?post_id=' +
        id +
        '&title=' +
        title +
        '&user=' +
        this.state.username +
        '&mobile=' +
        this.state.mobile,
    );
    const data = await response.json();
    if (data == 1) {
      this.likes(id);
    } else {
      alert('Not UnLike');
    }
  };

  getLikeData() {
    var username = this.state.username;
    // await AsyncStorage.getItem('username');
    var mobile = this.state.mobile;
    //  await AsyncStorage.getItem('mobile');

    var id = this.props.navigation.getParam('id');
    if (username && mobile) {
      let request =
        global.ip +
        'lokpriyaLikeData?user=' +
        username +
        '&mobile=' +
        mobile +
        '&post_id=' +
        id;

      // let data = request => {
      //   return
      // if (this.state.like == null) {
      fetch(request)
        .then(response => response.json())
        .then(responseJson => {
          //  responseJson;
          // console.log(responseJson);
          this.setState({
            like: responseJson,
          });
        })
        .catch(error => {
          // console.error(error);
        });
      // }
      // };
      // console.log(data);
      // var data = this.networkCall(request);
      // console.log('like Data : ' + JSON.stringify(data));

      if (this.state.like == '1') {
        // alert(data);
        return (
          <TouchableWithoutFeedback
            onPress={() =>
              this.unlike(
                this.props.navigation.getParam('id'),
                this.props.navigation.getParam('title'),
              )
            }>
            <View>
              <Icon name="ios-heart" style={Style.like} />
              <Text style={Style.likeCount}>{this.state.postLikes + 1000}</Text>
            </View>
          </TouchableWithoutFeedback>
        );
      } else {
        return (
          <TouchableWithoutFeedback
            onPress={() =>
              this.like(
                this.props.navigation.getParam('id'),
                this.props.navigation.getParam('title'),
              )
            }>
            <View>
              <Icon name="ios-heart-empty" style={Style.like} />
              <Text style={Style.likeCount}>{this.state.postLikes + 1000}</Text>
            </View>
          </TouchableWithoutFeedback>
        );
      }
    } else {
      return (
        <TouchableWithoutFeedback
          onPress={() =>
            this.like(
              this.props.navigation.getParam('id'),
              this.props.navigation.getParam('title'),
            )
          }>
          <View>
            <Icon name="ios-heart-empty" style={Style.like} />
            <Text style={Style.likeCount}>{this.state.postLikes + 1000}</Text>
          </View>
        </TouchableWithoutFeedback>
      );
    }
  }

  get_related_post = async id => {
    const response = await fetch(global.ip + 'related_post?cus_cat=' + id);
    const data = await response.json();
    this.setState({
      relatedPost: data,
    });
  };

  componentDidMount() {
    this.get_related_post(this.props.navigation.getParam('cid'));
    this.likes(this.props.navigation.getParam('id'));
    this.getProductsByPost(this.props.navigation.getParam('catName'), '');
    this.getPostersByPost(this.props.navigation.getParam('catName'), '');
  }

  openPost = item => {
    this.GoTo_top_function();
    this.props.navigation.navigate('Lokpriya', {
      title: item.post_title,
      mySrc: item.featuredImage,
      post_content: item.post_content.replace(/<[^>]*>|&nbsp;/g, ' '),
      cid: item.customize_category,
      id: item.id,
      title: item.post_title,
      catName: item.category_name,
    });
    this.get_related_post(item.customize_category);
    this.GoTo_top_function();
    this.likes(item.id);
    this.getProductsByPost(item.category_name);
    this.getPostersByPost(item.category_name);
  };

  likes = async id => {
    const response = await fetch(global.ip + 'post_likes?post_id=' + id);
    const data = await response.json();
    let likes = data[0].likes;
    this.setState({
      postLikes: likes,
    });
  };

  _renderItem = ({item, index}) => {
    return (
      <TouchableWithoutFeedback onPress={() => this.openPost(item)}>
        <View style={styles.slide}>
          <Image source={{uri: item.featuredImage}} style={styles.slideImg} />
          <Text style={styles.title}>{item.post_title}</Text>
        </View>
      </TouchableWithoutFeedback>
    );
  };

  GoTo_top_function = () => {
    this.refs.ListView_Reference.scrollTo({animated: true}, 0);
  };

  renderDFTDP = item => {
    return (
      <TouchableWithoutFeedback
        style={{borderWidth: 4, borderColor: 'red'}}
        onPress={() => this.openProduct(item)}>
        <View
          style={{
            width: 200,
            margin: 10,
            marginBottom: 20,
            marginTop: 10,
            padding: 2,
          }}>
          <View style={{backgroundColor: 'grey'}}>
            <Image style={styles.card} source={{uri: item.productImage1}} />
          </View>
          <Text style={{paddingLeft: 10, fontSize: 14, fontWeight: 'bold'}}>
            {' '}
            {item.product_name}
          </Text>
          <Text style={{paddingLeft: 10, fontSize: 14}}>
            <Image
              source={require('../image/rupay.png')}
              style={{height: 14, width: 14}}
            />
            <Text style={{color: '#ff7604', paddingRight: 10}}>
              {item.spcial_price != '0' ? (
                <>
                  {item.spcial_price}{' '}
                  <Text
                    style={{
                      color: 'rgba(0,0,0,0.5)',
                      textDecorationLine: 'line-through',
                    }}>
                    {item.price}
                  </Text>
                </>
              ) : (
                item.price
              )}
            </Text>
            <Text style={{fontWeight: 'bold'}}>
              {item.spcial_price != '0' ? (
                <>
                  {' '}
                  {100 - Math.round((item.spcial_price / item.price) * 100)}%
                  OFF{' '}
                </>
              ) : null}
            </Text>
          </Text>
        </View>
      </TouchableWithoutFeedback>
    );
  };

  renderHomePosters = item => {
    return (
      <TouchableWithoutFeedback
        style={{borderWidth: 4, borderColor: 'red'}}
        onPress={() => this.openProduct(item)}>
        <View
          style={{
            width: 150,
            margin: 5,
            marginBottom: 20,
            marginTop: 10,
            padding: 2,
          }}>
          <View style={{backgroundColor: 'grey'}}>
            <Image style={styles.card} source={{uri: item.productImage1}} />
          </View>
          <Text style={{paddingLeft: 10, fontSize: 14, fontWeight: 'bold'}}>
            {' '}
            {item.product_name}
          </Text>
          <Text style={{paddingLeft: 10, fontSize: 14}}>
            <Image
              source={require('../image/rupay.png')}
              style={{height: 14, width: 14}}
            />
            <Text style={{color: '#ff7604', paddingRight: 10}}>
              {item.spcial_price != '0' ? (
                <>
                  {item.spcial_price}{' '}
                  <Text
                    style={{
                      color: 'rgba(0,0,0,0.5)',
                      textDecorationLine: 'line-through',
                    }}>
                    {item.price}
                  </Text>
                </>
              ) : (
                item.price
              )}
            </Text>
            <Text style={{fontWeight: 'bold'}}>
              {item.spcial_price != '0' ? (
                <>
                  {' '}
                  {100 - Math.round((item.spcial_price / item.price) * 100)}%
                  OFF{' '}
                </>
              ) : null}
            </Text>
          </Text>
        </View>
      </TouchableWithoutFeedback>
    );
  };

  openProduct = item => {
    this.props.navigation.navigate('ProductScreen', {
      product_name: item.product_name,
      image1: item.productImage1,
      image2: item.productImage2,
      image3: item.productImage3,
      image4: item.productImage4,
      image5: item.productImage5,
      image6: item.productImage6,
      price: item.price,
      special_price: item.spcial_price,
      productId: item.product_id,
      description: item.description,
      brief_description: item.brief_description.replace(/<[^>]*>|&nbsp;/g, ' '),
      sku: item.product_sku,
    });
  };

  getProductsByPost = catName => {
    fetch(`${global.ip}getProductsByPost?catName=%${catName}%`)
      .then(response => response.json())
      .then(productsByPost => {
        this.setState({productsByPost: productsByPost});
      })
      .catch(err => console.log(err));
  };

  getPostersByPost = catName => {
    fetch(`${global.ip}getPostersByPost?catName=%${catName}%`)
      .then(response => response.json())
      .then(postersByPost => {
        this.setState({postersByPost: postersByPost});
      })
      .catch(err => console.log(err));
  };

  render() {
    return (
      <>
        {this.state.headerShown && (
          <HeaderIcon navigation={this.props.navigation} />
        )}
        <ScrollView style={{}} ref="ListView_Reference">
          <View style={{marginTop: 2}}>
            <DynamicHomeScreen
              featuredImage={this.props.navigation.getParam(
                'mySrc',
                'http://leeford.in/wp-content/uploads/2017/09/image-not-found.jpg',
              )}></DynamicHomeScreen>
          </View>
          <Text style={Style.title}>
            {this.props.navigation.getParam('title', 'No Title...')}
          </Text>

          <Text style={Style.postContent}>
            {this.props.navigation.getParam(
              'post_content',
              'No Content Found..!',
            )}
          </Text>
          <View style={{flexDirection: 'row', padding: 10}}>
            <TouchableWithoutFeedback
              onPress={() => {
                Share.share({
                  title: this.props.navigation.getParam(
                    'post_title',
                    'post title ',
                  ),
                  message:
                    this.props.navigation.getParam(
                      'section_seo_url',
                      'section_url is blank',
                    ) +
                    this.props.navigation
                      .getParam('post_content')
                      .substring(0, 1400) +
                    '...👇👇👇👇ऐसी कहानिया और तथ्य जो आपने कभी सुने नहीं होंगे...कसम से हिन्दू धर्म में आस्था 10 गुना बढ़ जायेगी ! 👇👇👇👇   अभी डाउनलोड करे.. http://bit.ly/2mEG2Qi',
                });
              }}>
              <View>
                <Image
                  source={require('../image/wlogo.png')}
                  style={{height: 35, width: 35, padding: 20}}
                />
              </View>
            </TouchableWithoutFeedback>

            {this.getLikeData()}
          </View>

          <View style={Style.hr}></View>
          <View
            style={{
              justifyContent: 'center',
              alignItems: 'center',
              marginBottom: 10,
            }}>
            {this.state.relatedPost.length > 0 ? (
              <Text
                style={{
                  fontSize: 18,
                  fontWeight: '200',
                }}>
                Related Posts:
              </Text>
            ) : null}
          </View>
          <Carousel
            style={styles.slider}
            ref={c => {
              this._carousel = c;
            }}
            data={this.state.relatedPost}
            renderItem={this._renderItem}
            sliderWidth={global.width}
            itemWidth={200}
          />
          {/* <View
            style={{
              borderColor: ' rgba(100, 100, 100, 0.3)',
              borderWidth: 3,
              paddingTop: 10,
              paddingBottom: 10,
            }}>
            {this.state.productsByPost.length > 0 ? (
              <Text
                style={{
                  textAlign: 'center',
                  fontSize: 18,
                  padding: 5,
                }}>
                Special Products For You
              </Text>
            ) : null}
            <Products
              navigation={this.props.navigation}
              data={this.state.productsByPost}
            />
          </View> */}
          {/* <View
            style={{
              borderColor: ' rgba(100, 100, 100, 0.3)',
              borderWidth: 3,
              paddingTop: 10,
              paddingBottom: 10,
            }}>
            {this.state.postersByPost.length > 0 ? (
              <Text
                style={{
                  textAlign: 'center',
                  fontSize: 18,
                  padding: 5,
                }}>
                Poster For You
              </Text>
            ) : null}
            <Posters
              navigation={this.props.navigation}
              data={this.state.postersByPost}
            />
          </View> */}
        </ScrollView>
      </>
    );
  }
}

const Style = StyleSheet.create({
  title: {
    fontSize: 16,
    padding: 8,
  },
  postContent: {
    fontSize: 14,
    padding: 10,
  },
  hr: {
    borderWidth: 0.5,
    borderColor: 'rgba(0,0,0,0.3)',
    marginTop: 5,
    marginBottom: 10,
  },
  like: {
    color: 'rgb(255,0,0)',
    margin: 5,
  },
  likeCount: {
    margin: 5,
    fontSize: 10,
  },
});

const styles = StyleSheet.create({
  slide: {
    borderWidth: 1,
    borderColor: 'grey',
    padding: 5,
    margin: 0,
    marginBottom: 50,
    borderRadius: 10,
  },
  slider: {
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  slideImg: {
    width: '100%',
    height: 150,
    borderRadius: 10,
  },
  title: {
    marginTop: 3,
  },
  card: {
    height: 250,
  },
});
