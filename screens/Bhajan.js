import React, {Component} from 'react';
import {
  Text,
  View,
  TouchableWithoutFeedback,
  Image,
  Alert,
  Button,
  TouchableOpacity,
} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import {StyleSheet} from 'react-native';

export default class Bhajan extends Component {
  openBhajanList = id => {
    this.props.navigation.navigate('BhajanList', {
      catId: id,
    });
  };

  render() {
    return (
      <View>
        <View style={{flexDirection: 'row', paddingTop: 20}}>
          <View style={styles.Godrow}>
            <TouchableOpacity onPress={() => this.openBhajanList(11)}>
              <Image
                style={styles.imggod}
                source={require('../image/durga.png')}
              />
              <Text style={{textAlign: 'center'}}>Durga</Text>
            </TouchableOpacity>
          </View>
          <View style={styles.Godrow}>
            <TouchableOpacity onPress={() => this.openBhajanList(18)}>
              <Image
                style={styles.imggod}
                source={require('../image/ganesha.png')}
              />
              <Text style={{textAlign: 'center'}}>Ganesha</Text>
            </TouchableOpacity>
          </View>
          <View style={styles.Godrow}>
            <TouchableOpacity onPress={() => this.openBhajanList(5)}>
              <Image
                style={styles.imggod}
                source={require('../image/krishna.png')}
              />
              <Text style={{textAlign: 'center'}}>Krishana</Text>
            </TouchableOpacity>
          </View>
        </View>

        <View style={{flexDirection: 'row', paddingTop: 20}}>
          <View style={styles.Godrow}>
            <TouchableOpacity onPress={() => this.openBhajanList(2)}>
              <Image
                style={styles.imggod}
                source={require('../image/shiv.png')}
              />
              <Text style={{textAlign: 'center'}}>Shiva</Text>
            </TouchableOpacity>
          </View>
          <View style={styles.Godrow}>
            <TouchableOpacity onPress={() => this.openBhajanList(3)}>
              <Image
                style={styles.imggod}
                source={require('../image/laxmi.png')}
              />
              <Text style={{textAlign: 'center'}}>Lakshmi</Text>
            </TouchableOpacity>
          </View>
          <View style={styles.Godrow}>
            <TouchableOpacity onPress={() => this.openBhajanList(6)}>
              <Image
                style={styles.imggod}
                source={require('../image/sani.png')}
              />
              <Text style={{textAlign: 'center'}}>Shani</Text>
            </TouchableOpacity>
          </View>
        </View>

        <View style={{flexDirection: 'row', paddingTop: 20}}>
          <View style={styles.Godrow}>
            <TouchableOpacity onPress={() => this.openBhajanList(28)}>
              <Image
                style={styles.imggod}
                source={require('../image/santoshi.png')}
              />
              <Text style={{textAlign: 'center'}}>Santoshi Maa</Text>
            </TouchableOpacity>
          </View>
          <View style={styles.Godrow}>
            <TouchableOpacity onPress={() => this.openBhajanList(27)}>
              <Image
                style={styles.imggod}
                source={require('../image/vishnu.png')}
              />
              <Text style={{textAlign: 'center'}}>Vishnu</Text>
            </TouchableOpacity>
          </View>
          <View style={styles.Godrow}>
            <TouchableOpacity onPress={() => this.openBhajanList(1)}>
              <Image
                style={styles.imggod}
                source={require('../image/hanuman.png')}
              />
              <Text style={{textAlign: 'center'}}>Hanuman</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  imggod: {
    paddingLeft: 25,
    width: 70,
    height: 70,
    borderRadius: 35,
    justifyContent: 'center',
    alignContent: 'center',
  },
  Godrow: {
    flex: 3.3,
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
