import React, {Component} from 'react';
import {Text, View} from 'react-native';
import CatPosts from './CatPosts';
import CatVideos from './CatVideos';

export {CatPosts, CatVideos};
