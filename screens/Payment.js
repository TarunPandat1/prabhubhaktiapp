import React, {Component} from 'react';
import {
  View,
  Text,
  Image,
  TouchableWithoutFeedback,
  TouchableOpacity,
  StyleSheet,
  TextInput,
} from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';
import HeaderIcon from './header';

export default class PaymentScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {text: ''};
  }
  render() {
    return (
      <View style={{flex: 100}}>
        <View style={{flex: 6, backgroundColor: 'white'}}>
          <HeaderIcon />
        </View>
        <View style={{flex: 88}}>
          <View
            style={{
              borderColor: 'rgba(255, 255, 255, 0.3)',
              borderWidth: 2,
            }}>
            <View style={{flexDirection: 'row'}}>
              <View style={{flex: 0.2, padding: 20}}>
                <Image
                  source={require('../image/arti.jpg')}
                  style={{height: 80, width: 80}}
                />
              </View>
              <View style={{flex: 0.8, flexDirection: 'column', padding: 20}}>
                <Text>Title:Shiv</Text>
                <Text>Name:Shiv</Text>
                <Text>Class:Shiv</Text>
                <Text>Std:Shiv</Text>
              </View>
            </View>
          </View>
          <View
            style={{
              borderColor: 'rgba(255, 255, 255, 0.3)',
              borderWidth: 2,
            }}>
            <View style={{flexDirection: 'row'}}>
              <View style={{flex: 1, padding: 20}}>
                <Text style={{fontSize: 18}}>Address Detail </Text>
                <Text>Address: sector 19 Udyog Vihar Gurugram</Text>
                <Text>Udyog Vihar Gurugram</Text>
                <Text>Gurugram</Text>
                <Text>230306</Text>
              </View>
            </View>
          </View>
          <View
            style={{
              borderColor: 'rgba(255, 255, 255, 0.3)',
              borderWidth: 2,
            }}>
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'center',
              }}>
              <TouchableOpacity
                onPress={() => {
                  alert('Cash On Delivery');
                }}
                style={{height: 100, width: 150}}>
                <Text style={{color: '#09AA06'}}>Cash On Delivery</Text>
              </TouchableOpacity>
            </View>
          </View>
          {/* <View
            style={{
              borderColor: 'rgba(255, 255, 255, 0.3)',
              borderWidth: 2,
            }}>
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'center',
              }}>
              <TouchableOpacity style={styles.btn} disabled={true}>
                <Text>Online Payment</Text>
              </TouchableOpacity>
            </View>
          </View> */}
          {/* <View
            style={{
              borderColor: 'rgba(255, 255, 255, 0.3)',
              borderWidth: 2,
            }}>
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'center',
              }}>
              <TouchableOpacity style={styles.btn} disabled={true}>
                <Text>Wallet</Text>
              </TouchableOpacity>
            </View>
          </View> */}
        </View>
        <View style={{flex: 6, backgroundColor: 'white'}}>
          <TouchableOpacity
            style={styles.cart}
            onPress={() => this.props.navigation.navigate('Thanksup')}
            title="Continued">
            <Text>CONTINUED</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  img: {
    flex: 0.5,
    backgroundColor: '#fff',
    height: 300,
    flexDirection: 'column',
    marginBottom: 10,
    padding: 5,
  },
  cart: {
    height: 50,
    width: '100%',
    borderColor: 'rgba(100,100,100,0.3)',
    borderWidth: 3,
    backgroundColor: '#ff7604',
    alignItems: 'center',
    justifyContent: 'center',
  },
  btn: {
    height: 58,
    width: 360,
  },
});
