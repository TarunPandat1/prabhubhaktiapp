import React, {Component} from 'react';
import {View, Text, Image, TouchableWithoutFeedback} from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';
import Cart from './Cart';
import {connect} from 'react-redux';

class HeaderIcon extends React.Component {
  componentDidMount() {
    this.props.CartCounting();
    this.props.OrderHistory();
  }

  render() {
    return (
      <View style={{flexDirection: 'row', height: 47}}>
        <View
          style={{
            flex: 0.2,
            alignItems: 'center',
            justifyContent: 'center',
            // marginLeft: 10,
          }}>
          <TouchableWithoutFeedback
            onPress={() => this.props.navigation.openDrawer()}>
            <Ionicons name="ios-menu" size={30} color="#ff7604" />
          </TouchableWithoutFeedback>
        </View>
        <View
          style={{
            alignItems: 'center',
            justifyContent: 'center',
            flex: 0.7,
          }}>
          <Image
            source={require('../image/logo.png')}
            style={{height: 40, width: 182}}
          />
        </View>
        <View
          style={{
            flex: 0.2,
            alignItems: 'center',
            justifyContent: 'center',
          }}>
          <Cart navigation={this.props.navigation} />
        </View>
      </View>
    );
  }
}

function mapStateToProps(state) {
  return {
    counter: state.cart,
    orders: state.orderHistory,
  };
}

const cartAPI = () => {
  return (dispatch, getState) => {
    fetch(
      `${global.ip}cart?id=${global.macAddress}&user=${global.username}&mobile=${global.mobile}`,
    )
      .then(response => response.json())
      .then(responseJson => {
        // console.log(responseJson);
        dispatch({type: 'CartCounting', data: responseJson});
      })
      .catch(err => console.log(err));
  };
};

const orderHistoryAPI = () => {
  return (dispatch, getState) => {
    fetch(
      `${global.ip}orderHistory?id=${global.macAddress}&user=${global.username}&mobile=${global.mobile}`,
    )
      .then(response => response.json())
      .then(responseJson => {
        // console.log('Orders: ' + responseJson);
        dispatch({type: 'OrderHistory', orders: responseJson});
      })
      .catch(err => console.log(err));
  };
};

function mapDispatchToProps(dispatch) {
  return {
    CartCounting: () => dispatch(cartAPI()),
    OrderHistory: () => dispatch(orderHistoryAPI()),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(HeaderIcon);
