import React, {Component} from 'react';
import {
  Text,
  View,
  StyleSheet,
  Image,
  ActivityIndicator,
  FlatList,
  TouchableOpacity,
  TouchableWithoutFeedback,
  Alert,
  Share,
} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import DynamicHomeScreen from './DynamicHomeScreen';
import HeaderIcon from './header';
import Ionicons from 'react-native-vector-icons/Ionicons';

export default class BhajanList extends Component {
  static navigationOptions = {
    headerShown: false,
    title: 'Videos',
  };

  offset = 0;

  state = {
    post: [],
    newPost: [],
    noPost: '',
    loading: true,
  };

  fetch = async o => {
    let catId = this.props.navigation.getParam('catId');
    const response = await fetch(
      global.ip + 'bhajanVideos?catId=' + catId + '&offset=' + o,
    );
    const data = await response.json();
    if (o == 0) {
      id = this.props.navigation.getParam('catId').toString() + 'Bhajan';
      await AsyncStorage.setItem(id, JSON.stringify(data));
    }
    this.setState(
      {
        post: this.state.post.concat(data),
        loading: false,
      },
      () => {
        console.log('Post found successfully..!');
      },
    );
  };

  Interval = setInterval(() => {
    let alldata = this.state.post;
    if (alldata.length == 0) {
      this.setState({
        noPost: 'No Related Posts Found..!',
      });
    } else {
      this.setState({
        noPost: '',
      });
    }
  }, 100);

  isCloseToBottom = ({layoutMeasurement, contentOffset, contentSize}) => {
    return (
      layoutMeasurement.height + contentOffset.y >= contentSize.height - 1500
    );
  };

  componentDidMount() {
    this.fetch(this.offset).catch(err => {
      console.log(err);
      this.get_post();
    });
  }

  get_post = async () => {
    id = this.props.navigation.getParam('catId').toString() + 'Bhajan';
    var l = await AsyncStorage.getItem(id);
    this.setState({
      post: JSON.parse(l),
    });
  };

  openPost = item => {
    this.props.navigation.navigate('Video', {
      title: item.post_title,
      mySrc: item.featuredImage,
      post_content: item.post_content.replace(/<[^>]*>|&nbsp;/g, ' '),
      youtube_link: item.youtube_link,
      cid: item.customize_category,
      id: item.id,
      catName: item.category_name,
    });
  };

  getMorePost = () => {
    this.setState({
      loading: true,
    });
    this.offset = this.offset + 10;
    this.fetch(this.offset).catch(err => {
      console.log(err);
    });
    console.log('scroll');
  };

  renderFooter = () => {
    return (
      <View>
        {this.state.loading && <ActivityIndicator size="large" color="cyan" />}
      </View>
    );
  };

  render() {
    return (
      <View style={styles.container}>
        {this.state.noPost != '' && (
          <Text style={{textAlign: 'center', fontSize: 18, padding: 5}}>
            {this.state.noPost}
          </Text>
        )}

        <FlatList
          onScroll={({nativeEvent}) => {
            if (this.isCloseToBottom(nativeEvent)) {
              this.getMorePost();
            }
          }}
          data={this.state.post}
          keyExtractor={(item, index) => index.toString()}
          renderItem={({item}) => (
            <TouchableWithoutFeedback
              onPress={() => {
                this.openPost(item);
              }}>
              <View style={styles.main}>
                <View style={styles.row}>
                  <View>
                    <Image
                      style={styles.profilePic}
                      source={{uri: item.image}}
                    />
                  </View>
                  <View>
                    <Text
                      style={{fontWeight: '600', fontSize: 10, paddingLeft: 4}}>
                      {item.category_name}
                    </Text>
                    <Text style={styles.title}>{item.post_title}</Text>
                  </View>
                </View>
                <View>
                  <DynamicHomeScreen
                    featuredImage={item.featuredImage}></DynamicHomeScreen>
                </View>
              </View>
            </TouchableWithoutFeedback>
          )}
          ListFooterComponent={this.renderFooter}
        />
      </View>
    );
  }
}
const styles = StyleSheet.create({
  title: {
    fontSize: 18,
    padding: 5,
  },
  row: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    marginRight: 10,
    padding: 10,
    width: '90%',
  },
  profilePic: {
    width: 50,
    height: 50,
    borderRadius: 25,
    marginRight: 5,
    backgroundColor: 'rgba(100,100,100,0.3)',
  },
  image: {
    height: 400,
    backgroundColor: 'rgba(100,100,100,0.3)',
  },
  main: {
    marginBottom: 20,
    borderTopColor: 'rgba(0,0,0,0.1)',
    borderTopWidth: 1,
  },
  container: {
    flex: 1,
  },
});
