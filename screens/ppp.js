import React, { Component } from 'react';
import {
  Text,
  StyleSheet,
  View,
  Image,
  TouchableOpacity,
  TouchableWithoutFeedback,
  ActivityIndicator,
  FlatList,
  Alert,
  Modal,
  Button,
} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import HeaderIcon from './header';
import { TouchableHighlight } from 'react-native-gesture-handler';
export default class ProductHome extends Component {
  static navigationOptions = {
    headerShown: false,
  };

  offset = 0;
  sorts = null;
  filter = null;

  state = {
    products: [],
    loading: true,
    width: global.width,
    height: 100,
    modalSortVisible: false,
    modalFilterVisible: false,
    modalmmmVisible: false,
  };

  fetch = async (s = null, f = null, o) => {
    const response = await fetch(global.ip + 'products?sort=' + s + '&filter=' + f + '&offset=' + o);
    const data = await response.json();
    if (o == 0) {
      await AsyncStorage.setItem('products', JSON.stringify(data));
    }
    this.setState(
      {
        products: this.state.products.concat(data),
        loading: false,
      },
      () => {
        console.log('products found successfully..!');
      },
    );
  };

  isCloseToBottom = ({ layoutMeasurement, contentOffset, contentSize }) => {
    return (
      layoutMeasurement.height + contentOffset.y >= contentSize.height - 1500
    );
  };

  componentDidMount() {
    this.fetch(this.sorts, this.filter, this.offset).catch(err => {
      console.log(err);
      Alert.alert(
        'Internet Connection Required',
        'Please connect to the internet then try again...!',
        [{ text: 'OK', onPress: () => console.log('OK Pressed') }],
        { cancelable: false },
      );
      this.get_products();
    });
  }

  get_products = async () => {
    var l = await AsyncStorage.getItem('products');
    this.setState({
      products: JSON.parse(l),
    });
  };

  openProduct = item => {
    this.props.navigation.navigate('ProductScreen', {
      product_name: item.product_name,
      image1: item.productImage1,
      image2: item.productImage2,
      image3: item.productImage3,
      image4: item.productImage4,
      image5: item.productImage5,
      image6: item.productImage6,
      price: item.price,
      special_price: item.spcial_price,
      productId: item.product_id,
      description: item.description,
      brief_description: item.brief_description.replace(/<[^>]*>|&nbsp;/g, ' '),
      sku: item.product_sku,
    });
  };

  getMoreProducts = () => {
    this.setState({
      loading: true,
    });

    this.offset = this.offset + 10;
    this.fetch(this.sorts, this.filters, this.offset).catch(err => {
      console.log(err);
    });
  };

  openSortModal() {
    this.setState({ modalSortVisible: true });
  }
  openFilterModal() {
    this.setState({ modalFilterVisible: true });
  }

  closeSortModal() {
    this.setState({ modalSortVisible: false });
  }
  closeFilterModal() {
    this.setState({ modalFilterVisible: false });
  }
  openmmmModal() {
    this.setState({ modalmmmVisible: true });
  }

  sort = s => {
    this.setState({
      products: [],
      loading: true,
      modalSortVisible: false,
    })
    this.sorts = s;
    this.offset = 0;
    this.fetch(this.sorts, this.filter, this.offset).catch(err => {
      console.log(err);
    });
  }

  filters = id => {
    this.setState({
      products: [],
      loading: true,
      modalFilterVisible: false,
    })
    this.filter = id;
    this.offset = 0;
    this.fetch(this.sorts, this.filter, this.offset).catch(err => {
      console.log(err);
    });
  }

  renderFlatList = item => {
    return (
      <TouchableWithoutFeedback
        style={{ borderWidth: 4, borderColor: 'red' }}
        onPress={() => this.openProduct(item)}>
        <View style={{ width: '50%', marginBottom: 20, marginTop: 10, padding: 2 }}>
          <Image style={styles.card} source={{ uri: item.productImage1 }} />
          <Text style={{ paddingLeft: 10, fontSize: 14, fontWeight: 'bold' }}>
            {' '}
            {item.product_name}
          </Text>
          <Text style={{ paddingLeft: 10, fontSize: 14 }}>
            <Image
              source={require('../image/rupay.png')}
              style={{ height: 14, width: 14 }}
            />
            <Text style={{ color: '#ff7604', paddingRight: 10 }}>
              {item.spcial_price != '0' ? <>{item.spcial_price} <Text style={{ color: 'rgba(0,0,0,0.5)', textDecorationLine: 'line-through' }} >{item.price}</Text></> : item.price}
            </Text>
            <Text style={{ fontWeight: 'bold' }}>
              {item.spcial_price != '0' ? <> {item.spcial_price / item.price * 100}% OFF </> : null}
            </Text>

          </Text>
        </View>
      </TouchableWithoutFeedback>
    );
  };

  spearator = () => {
    return <View style={{ height: 1, backgroundColor: 'rgba(0,0,0,0.1)' }} />;
  };

  renderFooter = () => {
    return (
      <View style={{ marginTop: 30 }}>
        {this.state.loading && <ActivityIndicator size="large" color="cyan" />}
      </View>
    );
  };

  render() {
    return (
      <View style={{ flex: 1, flexDirection: 'column' }}>
        <View style={{ flex: 0.93 }}>
          <HeaderIcon navigation={this.props.navigation} />
          <View style={{}}>
            {this.state.products.length == 0 && this.state.loading == false ? <Text style={{ flez: 1, margin: 15, fontSize: 25, textAlign: 'center', marginTop: 40 }} >No Related Products Found Yet..!</Text>: null}
            <FlatList
              onScroll={({ nativeEvent }) => {
                if (this.isCloseToBottom(nativeEvent)) {
                  this.getMoreProducts();
                }
              }}
              data={this.state.products}
              keyExtractor={(item, index) => index.toString()}
              renderItem={({ item }) => this.renderFlatList(item)}
              ListFooterComponent={this.renderFooter}
              ItemSeparatorComponent={this.spearator}
              numColumns={2}
            />
          </View>
        </View>
        <View style={{ flex: 0.07 }}>
          <View style={{ flexDirection: 'row' }}>
            <View style={{ flex: 0.5 }}>
              <Modal
                visible={this.state.modalSortVisible}
                animationType={'slide'}
                transparent={true}
                onRequestClose={() => this.closeSortModal()}>
                <View style={styles.modalContainer}></View>
                <View style={styles.modallower}>
                  <View style={styles.innerContainer}>
                    <Text
                      style={{
                        fontWeight: 'bold',
                        fontSize: 20,
                        marginBottom: 10,
                      }}>
                      SORT BY{' '}
                    </Text>

                    <TouchableOpacity
                      style={styles.sortfilterlist}
                      onPress={() => this.sort('cost high to low')}>
                      <Text style={styles.content}>cost high to low</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                      style={styles.sortlist}
                      onPress={() => this.sort('cost low to high')}>
                      <Text style={styles.content}>cost low to high</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                      style={styles.sortfilterlist}
                      onPress={() => this.sort('most popular')}>
                      <Text style={styles.content}>most popular</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                      style={styles.sortlist}
                      onPress={() => this.sort()}>
                      <Text style={styles.content}>clear sort</Text>
                    </TouchableOpacity>
                  </View>
                </View>
              </Modal>
              <Modal
                visible={this.state.modalFilterVisible}
                animationType={'slide'}
                transparent={true}
                onRequestClose={() => this.closeFilterModal()}>
                <View style={styles.modalContainer}></View>
                <View style={styles.modallower}>
                  <View style={styles.innerContainer}>
                    <Text
                      style={{
                        fontWeight: 'bold',
                        fontSize: 20,
                        marginBottom: 10,
                      }}>
                      FILTER BY{' '}
                    </Text>

                    {/* <View
                      style={{
                        flex: 1,
                        flexDirection: 'row',
                        marginBottom: 50,
                      }}>
                      <View style={{ flex: 0.5 }}>
                        <TouchableOpacity
                          style={styles.sortfilterlist}
                          onPress={() => Alert.alert('Popular Product')}>
                          <Text style={styles.content}>Popular Product</Text>
                        </TouchableOpacity>
                      </View>
                      <View style={{ flex: 0.5 }}>
                        <TouchableOpacity
                          style={styles.sortfilterlist}
                          onPress={() => Alert.alert('Deal Of The Days')}>
                          <Text style={styles.content}>Deal Of The Days</Text>
                        </TouchableOpacity>
                      </View>
                    </View> */}
                    <View
                      style={{
                        flex: 1,
                        flexDirection: 'row',
                        marginBottom: 50,
                      }}>
                      <View style={{ flex: 0.5 }}>
                        <TouchableOpacity
                          style={styles.sortfilterlist}
                          onPress={() => this.filters(7)}>
                          <Text style={styles.content}>Astrology</Text>
                        </TouchableOpacity>
                      </View>
                      <View style={{ flex: 0.5 }}>
                        <TouchableOpacity
                          style={styles.sortfilterlist}
                          onPress={() => this.filters(19)}>
                          <Text style={styles.content}>Ramayan</Text>
                        </TouchableOpacity>
                      </View>
                    </View>
                    <View
                      style={{
                        flex: 1,
                        flexDirection: 'row',
                        marginBottom: 50,
                      }}>
                      <View style={{ flex: 0.5 }}>
                        <TouchableOpacity
                          style={styles.sortfilterlist}
                          onPress={() => this.filters(27)}>
                          <Text style={styles.content}>Vishnu</Text>
                        </TouchableOpacity>
                      </View>
                      <View style={{ flex: 0.5 }}>
                        <TouchableOpacity
                          style={styles.sortfilterlist}
                          onPress={() => this.filters(23)}>
                          <Text style={styles.content}>Mahabharat</Text>
                        </TouchableOpacity>
                      </View>
                    </View>
                    <View
                      style={{
                        flex: 1,
                        flexDirection: 'row',
                        marginBottom: 50,
                      }}>
                      <View style={{ flex: 0.5 }}>
                        <TouchableOpacity
                          style={styles.sortfilterlist}
                          onPress={() => this.filters(0)}>
                          <Text style={styles.content}>Poster</Text>
                        </TouchableOpacity>
                      </View>
                      <View style={{ flex: 0.5 }}>
                        <TouchableOpacity
                          style={styles.sortfilterlist}
                          onPress={() => this.filters()}>
                          <Text style={styles.content}>Clear Filter</Text>
                        </TouchableOpacity>
                      </View>
                    </View>

                  </View>
                </View>
              </Modal>
              <TouchableHighlight
                underlayColor='rgb(73,182,77)'
                style={styles.cartsort}
                onPress={() => this.openSortModal()}
                title="Open modal">
                <Text>Sort</Text>
              </TouchableHighlight>
            </View>
            <View style={{ flex: 0.5 }}>
              <TouchableHighlight
                underlayColor='rgb(73,182,77)'
                style={styles.cartsort}
                onPress={() => this.openFilterModal()}>
                <Text>Filter</Text>
              </TouchableHighlight>
            </View>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  img: {
    backgroundColor: '#fff',
    height: 300,
    flexDirection: 'column',
    marginBottom: 10,
    padding: 5,
  },
  card: {
    height: 250,
  },
  cartsort: {
    height: 50,
    width: '100%',
    borderColor: 'rgba(100,100,100,0.3)',
    borderWidth: 3,
    backgroundColor: '#ff7604',
    alignItems: 'center',
    justifyContent: 'center',
  },
  modalContainer: {
    flex: 0.999,
    backgroundColor: 'transparent',
  },
  modallower: {
    borderWidth: 0.2,
    borderRadius: 5,
    height: 250,
    borderColor: 'grey',
    backgroundColor: 'white',
  },
  innerContainer: {
    padding: 25,
  },
  modallowerfilt: {
    borderWidth: 0.2,
    borderRadius: 0.2,
    borderColor: 'grey',
  },
  sortfilterlist: {
    borderColor: 'grey',
    borderWidth: 1,
    borderRadius: 10,
    width: 170,
    marginBottom: 20,
    height: 30,
  },
  content: {
    fontSize: 16,
    textAlign: 'center',
  },
  sortlist: {
    borderColor: 'grey',
    borderWidth: 1,
    borderRadius: 10,
    width: 170,
    marginBottom: 20,
    height: 30,
    marginLeft: 150,
  },
});
