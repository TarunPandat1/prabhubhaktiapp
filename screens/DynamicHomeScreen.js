import React, {Component} from 'react';
import Image from 'react-native-scalable-image';
import {View} from 'react-native';

export default class DynamicHomeScreen extends Component {
  render() {
    return (
      <View>
        <Image width={global.width} source={{uri: this.props.featuredImage}} />
      </View>
    );
  }
}
