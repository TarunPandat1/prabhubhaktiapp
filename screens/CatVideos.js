import React, {Component} from 'react';
import {
  Text,
  View,
  StyleSheet,
  Image,
  ActivityIndicator,
  FlatList,
  TouchableOpacity,
  TouchableWithoutFeedback,
  Alert,
  Share,
} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import DynamicHomeScreen from './DynamicHomeScreen';
import HeaderIcon from './header';
import Ionicons from 'react-native-vector-icons/Ionicons';

export default class CatVideos extends Component {
  static navigationOptions = {
    headerShown: false,
    title: 'Videos',
  };

  offset = 0;

  state = {
    post: [],
    newPost: [],
    loading: true,
  };

  fetch = async o => {
    let catId = this.props.navigation.getParam('catId');
    const response = await fetch(
      global.ip + 'catVideos?catId=' + catId + '&offset=' + o,
    );
    const data = await response.json();
    if (o == 0) {
      id = this.props.navigation.getParam('catId').toString() + 'CatVideos';
      await AsyncStorage.setItem(id, JSON.stringify(data));
    }
    this.setState(
      {
        post: this.state.post.concat(data),
        loading: false,
      },
      () => {
        console.log('Post found successfully..!');
      },
    );
  };

  isCloseToBottom = ({layoutMeasurement, contentOffset, contentSize}) => {
    return (
      layoutMeasurement.height + contentOffset.y >= contentSize.height - 1500
    );
  };

  componentDidMount() {
    this.fetch(this.offset).catch(err => {
      console.log(err);
      this.get_post();
    });
  }

  get_post = async () => {
    id = this.props.navigation.getParam('catId').toString() + 'CatVideos';
    var l = await AsyncStorage.getItem(id);
    this.setState({
      post: JSON.parse(l),
    });
  };

  openPost = item => {
    this.props.navigation.navigate('Video', {
      title: item.post_title,
      mySrc: item.featuredImage,
      post_content: item.post_content.replace(/<[^>]*>|&nbsp;/g, ' '),
      youtube_link: item.youtube_link,
      cid: item.customize_category,
      id: item.id,
      catName: item.category_name,
    });
  };

  getMorePost = () => {
    this.setState({
      loading: true,
    });
    this.offset = this.offset + 10;
    this.fetch(this.offset).catch(err => {
      console.log(err);
    });
    console.log('scroll');
  };

  renderFooter = () => {
    return (
      <View>
        {this.state.loading && <ActivityIndicator size="large" color="cyan" />}
      </View>
    );
  };

  render() {
    return (
      <View style={styles.container}>
        <FlatList
          onScroll={({nativeEvent}) => {
            if (this.isCloseToBottom(nativeEvent)) {
              this.getMorePost();
            }
          }}
          data={this.state.post}
          keyExtractor={(item, index) => index.toString()}
          renderItem={({item}) => (
            <TouchableWithoutFeedback
              onPress={() => {
                this.openPost(item);
              }}>
              <View style={styles.main}>
                <View style={styles.row}>
                  <View>
                    <Image
                      style={styles.profilePic}
                      source={{uri: item.image}}
                    />
                  </View>
                  <View>
                    <Text
                      style={{fontWeight: '600', fontSize: 10, paddingLeft: 4}}>
                      {item.category_name}
                    </Text>
                    <Text style={styles.title}>{item.post_title}</Text>
                  </View>
                </View>
                <View>
                  <DynamicHomeScreen
                    featuredImage={item.featuredImage}></DynamicHomeScreen>
                </View>
                {/* <View>
                  <TouchableWithoutFeedback
                    onPress={() => {
                      Share.share({
                        title: item.post_title,
                        message:
                          item.post_title +
                          item.section_seo_url +
                          (
                            item.post_content.substring(0, 1400) + '  ...'
                          ).replace(/<[^>]*>|&nbsp;/g, ' ') +
                          'ऐसी कहानिया और तथ्य जो आपने कभी सुने नहीं होंगे...कसम से हिन्दू धर्म में आस्था 10 गुना बढ़ जायेगी ! 👇👇👇👇   अभी डाउनलोड करे.. http://bit.ly/2mEG2Qi',
                        url: item.featuredImage,
                      });
                    }}>
                    <View style={{justifyContent: 'center', padding: 10}}>
                      <Image
                        source={require('../image/wlogo.png')}
                        style={{height: 22, width: 22}}
                      />
                    </View>
                  </TouchableWithoutFeedback>
                </View> */}
              </View>
            </TouchableWithoutFeedback>
          )}
          ListFooterComponent={this.renderFooter}
        />
      </View>
    );
  }
}
const styles = StyleSheet.create({
  title: {
    fontSize: 18,
    padding: 5,
  },
  row: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    marginRight: 10,
    padding: 10,
    width: '90%',
  },
  profilePic: {
    width: 50,
    height: 50,
    borderRadius: 25,
    marginRight: 5,
    backgroundColor: 'rgba(100,100,100,0.3)',
  },
  image: {
    height: 400,
    backgroundColor: 'rgba(100,100,100,0.3)',
  },
  main: {
    // marginBottom: 20,
    borderTopColor: 'rgba(0,0,0,0.1)',
    borderTopWidth: 1,
  },
  container: {
    flex: 1,
  },
});
