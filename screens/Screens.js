import HomeScreen from './HomeScreen';
import ProductScreen from './ProductScreen';
import Lokpriya from './Lokpriya';
import Profile from './Profile';
import Login from './Login';
import header from './header';
import Slider from './Slider';
import SplashScreen from './SplashScreen';
import Testing from './Testing';
import Videos from './Videos';
import Video from './Video';
import DynamincHomeScreen from './DynamicHomeScreen';

export {
  HomeScreen,
  ProductScreen,
  Lokpriya,
  Profile,
  Login,
  header,
  Slider,
  Testing,
  Videos,
  Video,
  DynamincHomeScreen,
  SplashScreen,
};
