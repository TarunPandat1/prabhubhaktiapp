import React, {Component} from 'react';
import {
  Text,
  View,
  StyleSheet,
  Image,
  TouchableWithoutFeedback,
  Linking,
  RefreshControl,
} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import firebase from 'react-native-firebase';
import HeaderIcon from './header';
import {ScrollView, FlatList} from 'react-native-gesture-handler';
import {Posts, Posters, Products} from '../Components/';

export class HomeScreen extends Component {
  static navigationOptions = {
    headerShown: false,
  };

  state = {
    panchang: '',
    sunrise: '',
    sunset: '',
    moonrise: '',
    moonset: '',
    auspicious: '',
    rahukaalam: '',
    todaySpecial: [],
    mandirPost: [],
    dealOfTheDayProducts: [],
    homePosters: [],
    refreshing: false,
  };

  getTodaySpecial = () => {
    fetch(global.ip + 'todaySpecial')
      .then(response => response.json())
      .then(responseJson => {
        this.setState({
          todaySpecial: responseJson,
        });
      });
  };

  getMandirPost = () => {
    fetch(`${global.ip}mandirPost`)
      .then(response => response.json())
      .then(responseJson => {
        this.setState({mandirPost: responseJson});
      })
      .catch(err => {
        console.log(err);
      });
  };

  getDFTDP = () => {
    fetch(`${global.ip}dealOfTheDayProducts`)
      .then(response => response.json())
      .then(products => {
        this.setState({dealOfTheDayProducts: products});
      })
      .catch(err => {
        console.log(err);
      });
  };

  getHomePosters = () => {
    fetch(`${global.ip}homePosters`)
      .then(response => response.json())
      .then(products => {
        this.setState({homePosters: products});
      })
      .catch(err => {
        console.log(err);
      });
  };

  // FIREBASE NOTIFICATION----------------------------------------------------------------------------------------------------------------------------------------------------

  async componentDidMount() {
    this.checkPermission();
    this.createNotificationListeners(); //add this line

    Linking.getInitialURL()
      .then(url => {
        if (url) {
          console.log('Initial url is: ' + url);
          this.getDataByLink(url).catch(err => {
            this.getVideoByLink(url).catch(err => {
              this.props.navigation.navigate('Feed');
            });
          });
        }
      })
      .catch(err => console.error('An error occurred', err));
    this.getPanchang();
    this.getTodaySpecial();
    this.getMandirPost();
    this.getDFTDP();
    this.getHomePosters();
  }

  getDataByLink = async url => {
    const response = await fetch(global.ip + 'postByLink?url=' + url);
    const data = await response.json();
    this.openPost(data[0], true);
  };

  getVideoByLink = async url => {
    const response = await fetch(global.ip + 'videoByLink?url=' + url);
    const data = await response.json();
    this.openVideo(data[0], true);
  };

  openPost = (item, headerShown) => {
    this.props.navigation.navigate('Lokpriya', {
      title: item.post_title,
      mySrc: item.featuredImage,
      section_seo_url: item.section_seo_url,
      post_content: item.post_content.replace(/<[^>]*>|&nbsp;/g, ' '),
      cid: item.customize_category,
      id: item.id,
      title: item.post_title,
      catName: item.category_name,
      headerShown: headerShown,
    });
  };

  openVideo = item => {
    this.props.navigation.navigate('Video', {
      title: item.post_title,
      mySrc: item.featuredImage,
      post_content: item.post_content.replace(/<[^>]*>|&nbsp;/g, ' '),
      youtube_link: item.youtube_link,
      cid: item.customize_category,
      section_url: item.section_seo_url,
      id: item.id,
      catName: item.category_name,
      headerShown: true,
    });
  };

  getPanchang = () => {
    fetch(global.ip + 'panchang')
      .then(response => response.json())
      .then(responseJson => {
        this.setState({
          panchang: responseJson[0].post_title,
          sunrise: `${responseJson[0].sunrise_hour} : ${responseJson[0].sunrise_minute}`,
          sunset: `${responseJson[0].sunset_hour} : ${responseJson[0].sunset_minute}`,
          moonrise: `${responseJson[0].moonrise_hour} : ${responseJson[0].moonrise_minute}`,
          moonset: `${responseJson[0].moonset_hour} : ${responseJson[0].moonset_minute}`,
          auspicious: `${responseJson[0].auspicious_time_hour} : ${responseJson[0].auspicious_time_minute} ${responseJson[0].auspicious_end_hour} : ${responseJson[0].auspicious_end_minute}`,
          rahukaalam: `${responseJson[0].rahukaalam_hour} : ${responseJson[0].rahukaalam_minute} से ${responseJson[0].rahukaalam_end_hour} : ${responseJson[0].rahukaalam_end_minute}`,
        });
      })
      .catch(err => console.log(err));
  };

  //1
  async checkPermission() {
    const enabled = await firebase.messaging().hasPermission();
    if (enabled) {
      this.getToken();
    } else {
      this.requestPermission();
    }
  }

  //3
  async getToken() {
    let fcmToken = await AsyncStorage.getItem('fcmToken');
    if (!fcmToken) {
      fcmToken = await firebase.messaging().getToken();
      if (fcmToken) {
        // user has a device token
        console.log('fcmToken:', fcmToken);
        await AsyncStorage.setItem('fcmToken', fcmToken);
      }
    }
    console.log('fcmToken:', fcmToken);
  }

  //2
  async requestPermission() {
    try {
      await firebase.messaging().requestPermission();
      // User has authorised
      this.getToken();
    } catch (error) {
      // User has rejected permissions
      console.log('permission rejected');
    }
  }

  async createNotificationListeners() {
    /*
     * Triggered when a particular notification has been received in foreground
     * */
    this.notificationListener = firebase
      .notifications()
      .onNotification(notification => {
        console.log('onNotification:');

        const localNotification = new firebase.notifications.Notification({
          sound: 'sampleaudio',
          show_in_foreground: true,
        })
          .setSound('sampleaudio')
          .setNotificationId(notification.notificationId)
          .setTitle(notification.title)
          .setBody(notification.body)
          .setData(notification.data)
          .android.setChannelId('fcm_FirebaseNotifiction_default_channel') // e.g. the id you chose above
          .android.setSmallIcon('@drawable/ic_launcher') // create this icon in Android Studio
          .android.setColor('#000000') // you can set a color here
          .android.setPriority(firebase.notifications.Android.Priority.High);

        firebase
          .notifications()
          .displayNotification(localNotification)
          .catch(err => console.error(err));
      });

    const channel = new firebase.notifications.Android.Channel(
      'fcm_FirebaseNotifiction_default_channel',
      'Demo app name',
      firebase.notifications.Android.Importance.High,
    )
      .setDescription('Demo app description')
      .setSound('sampleaudio');
    firebase.notifications().android.createChannel(channel);

    /*
     * If your app is in background, you can listen for when a notification is clicked / tapped / opened as follows:
     * */
    this.notificationOpenedListener = firebase
      .notifications()
      .onNotificationOpened(notificationOpen => {
        const {data} = notificationOpen.notification;
        console.log('onNotificationOpened:');
        console.log(notificationOpen.notification);

        this.getDataByLink(data.link).catch(err => {
          this.getVideoByLink(data.link).catch(err => {
            this.props.navigation.navigate('Feed');
          });
        });
      });

    /*
     * If your app is closed, you can check if it was opened by a notification being clicked / tapped / opened as follows:
     * */
    const notificationOpen = await firebase
      .notifications()
      .getInitialNotification();
    if (notificationOpen) {
      const {data} = notificationOpen.notification;
      console.log('getInitialNotification:');
      console.log(notificationOpen.notification);
      this.getDataByLink(data.link).catch(err => {
        this.getVideoByLink(data.link).catch(err => {
          this.props.navigation.navigate('Feed');
        });
      });
    }
    /*
     * Triggered for data only payload in foreground
     * */
    this.messageListener = firebase.messaging().onMessage(message => {
      //process data message
      console.log('JSON.stringify:', JSON.stringify(message));
    });
  }

  // FIREBASE NOTIFICATION----------------------------------------------------------------------------------------------------------------------------------------------------

  openCustCat = id => {
    this.props.navigation.navigate('Cat', {
      catId: id,
    });
  };

  openProduct = item => {
    this.props.navigation.navigate('ProductScreen', {
      product_name: item.product_name,
      image1: item.productImage1,
      image2: item.productImage2,
      image3: item.productImage3,
      image4: item.productImage4,
      image5: item.productImage5,
      image6: item.productImage6,
      price: item.price,
      special_price: item.spcial_price,
      productId: item.product_id,
      description: item.description,
      brief_description: item.brief_description.replace(/<[^>]*>|&nbsp;/g, ' '),
      sku: item.product_sku,
    });
  };

  onRefresh = () => {
    this.setState({
      panchang: '',
      sunrise: '',
      sunset: '',
      moonrise: '',
      moonset: '',
      auspicious: '',
      rahukaalam: '',
      todaySpecial: [],
      mandirPost: [],
      dealOfTheDayProducts: [],
      homePosters: [],
      refreshing: true,
    });
    this.getPanchang();
    this.getTodaySpecial();
    this.getMandirPost();
    this.getDFTDP();
    this.getHomePosters();
    setTimeout(() => {
      this.setState({refreshing: false});
    }, 2000);
  };

  render() {
    return (
      <View>
        <HeaderIcon navigation={this.props.navigation} />

        <ScrollView
          refreshControl={
            <RefreshControl
              refreshing={this.state.refreshing}
              onRefresh={this.onRefresh.bind(this)}
            />
          }
          scrollEventThrottle={16}>
          <View style={{backgroundColor: 'rgba(100,100,100,0.3)'}}>
            <View
              style={{borderRadius: 20, backgroundColor: '#fff', margin: 5}}>
              <View
                style={{
                  flexDirection: 'row',
                  paddingTop: 10,
                  borderColor: 'rgba(100,100,100,0.3)',
                  paddingBottom: 10,
                }}>
                <View style={{flex: 2.5}}>
                  <TouchableWithoutFeedback
                    onPress={() =>
                      this.props.navigation.navigate('Hanumancategory', {
                        catId: 1,
                      })
                    }>
                    <View style={styles.fourimag}>
                      <Image
                        style={styles.imggod}
                        source={require('../image/hanuman.png')}
                      />
                      <Text style={styles.txt}>Hanuman</Text>
                    </View>
                  </TouchableWithoutFeedback>
                </View>
                <View style={{flex: 2.5}}>
                  <TouchableWithoutFeedback
                    onPress={() =>
                      this.props.navigation.navigate('Durgacategory')
                    }>
                    <View style={styles.fourimag}>
                      <Image
                        style={styles.imggod}
                        source={require('../image/durga.png')}
                      />
                      <Text style={styles.txt}>Durga</Text>
                    </View>
                  </TouchableWithoutFeedback>
                </View>
                <View style={{flex: 2.5}}>
                  <TouchableWithoutFeedback
                    onPress={() =>
                      this.props.navigation.navigate('Shivacategory')
                    }>
                    <View style={styles.fourimag}>
                      <Image
                        style={styles.imggod}
                        source={require('../image/shiv.png')}
                      />
                      <Text style={styles.txt}>Shiva</Text>
                    </View>
                  </TouchableWithoutFeedback>
                </View>
                <View style={{flex: 2.5}}>
                  <TouchableWithoutFeedback
                    onPress={() =>
                      this.props.navigation.navigate('Shanicategory')
                    }>
                    <View style={styles.fourimag}>
                      <Image
                        style={styles.imggod}
                        source={require('../image/sani.png')}
                      />
                      <Text style={styles.txt}>Shani</Text>
                    </View>
                  </TouchableWithoutFeedback>
                </View>
              </View>
              <View
                style={{
                  flexDirection: 'row',
                  paddingBottom: 10,
                }}>
                <View style={{flex: 2.5}}>
                  <TouchableWithoutFeedback
                    onPress={() =>
                      this.props.navigation.navigate('Krishnacategory')
                    }>
                    <View style={styles.fourimag}>
                      <Image
                        style={styles.imggod}
                        source={require('../image/krishna.png')}
                      />
                      <Text style={styles.txt}>Krishna</Text>
                    </View>
                  </TouchableWithoutFeedback>
                </View>
                <View style={{flex: 2.5}}>
                  <TouchableWithoutFeedback
                    onPress={() =>
                      this.props.navigation.navigate('Ganeshcategory')
                    }>
                    <View style={styles.fourimag}>
                      <Image
                        style={styles.imggod}
                        source={require('../image/ganesha.png')}
                      />
                      <Text style={styles.txt}>Ganesh</Text>
                    </View>
                  </TouchableWithoutFeedback>
                </View>
                <View style={{flex: 2.5, paddingLeft: 2}}>
                  <TouchableWithoutFeedback
                    onPress={() =>
                      this.props.navigation.navigate('Laxmicategory')
                    }>
                    <View style={styles.fourimag}>
                      <Image
                        style={styles.imggod}
                        source={require('../image/laxmi.png')}
                      />
                      <Text style={styles.txt}>Laxmi</Text>
                    </View>
                  </TouchableWithoutFeedback>
                </View>
                <View style={{flex: 2.5}}>
                  <TouchableWithoutFeedback
                    onPress={() =>
                      this.props.navigation.navigate('Vishnucategory')
                    }>
                    <View style={styles.fourimag}>
                      <Image
                        style={styles.imggod}
                        source={require('../image/vishnu.png')}
                      />
                      <Text style={styles.txt}>Vishnu</Text>
                    </View>
                  </TouchableWithoutFeedback>
                </View>
              </View>
            </View>
          </View>
          <View
            style={{
              borderColor: 'rgba(100,100,100,0.3)',
              borderWidth: 5,
              paddingBottom: 12,
            }}>
            <View style={{alignItems: 'center', justifyContent: 'center'}}>
              <Text style={{fontSize: 22, fontWeight: 'bold', padding: 7}}>
                आज का पंचांग
              </Text>
            </View>
            <View style={{flexDirection: 'row'}}>
              <View style={{flex: 0.4}}>
                <Image
                  style={styles.imag}
                  source={{
                    uri:
                      'https://hindi.hwnews.in/wp-content/uploads/2017/07/panchang.png',
                  }}
                />
              </View>
              <View style={{flex: 0.8}}>
                <Text
                  style={{textAlign: 'center', color: '#ff7604', fontSize: 20}}>
                  {' '}
                  {this.state.panchang}
                </Text>
                <View style={{flexDirection: 'row'}}>
                  <View style={{flex: 0.5}}>
                    <View style={{flexDirection: 'row'}}>
                      <Text>सूर्योदय :-</Text>
                      <Text>{this.state.sunrise}</Text>
                    </View>
                    <View style={{flexDirection: 'row'}}>
                      <Text>सूर्यास्त :-</Text>
                      <Text>{this.state.sunset}</Text>
                    </View>
                    <View style={{flexDirection: 'column'}}>
                      <Text style={{textAlign: 'center'}}>शुभ मुहर्त:-</Text>
                      <Text style={{textAlign: 'center'}}>राहू काल:-</Text>
                    </View>
                  </View>
                  <View style={{flex: 0.5}}>
                    <View style={{flexDirection: 'row'}}>
                      <Text>चंद्रोदय:- </Text>
                      <Text>{this.state.moonrise}</Text>
                    </View>
                    <View style={{flexDirection: 'row'}}>
                      <Text>चन्द्रास्त:-</Text>
                      <Text>{this.state.moonset}</Text>
                    </View>
                    <View style={{flexDirection: 'column'}}>
                      <Text>{this.state.auspicious}</Text>

                      <Text>{this.state.rahukaalam}</Text>
                    </View>
                  </View>
                </View>
              </View>
            </View>
          </View>

          {/* <View
            style={{
              borderColor: ' rgba(100, 100, 100, 0.3)',
            }}>
            {this.state.todaySpecial.length > 0 ? (
              <Text style={styles.txthead}>Today Special</Text>
            ) : null}
            <Posts
              navigation={this.props.navigation}
              data={this.state.todaySpecial}
            />
          </View> */}

          <View
            style={{
              borderColor: 'rgba(100,100,100,0.3)',
              borderWidth: 5,
            }}>
            <TouchableWithoutFeedback onPress={() => this.openCustCat(31)}>
              <View>
                <Text
                  style={{
                    textAlign: 'center',
                    fontSize: 18,
                    padding: 10,
                  }}>
                  Astrology For You
                </Text>
                <Image
                  resizeMode="stretch"
                  style={styles.img}
                  source={require('../image/rashifal.jpg')}
                />
              </View>
            </TouchableWithoutFeedback>
            <View
              style={{
                flex: 1,
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'center',
              }}>
              <TouchableWithoutFeedback onPress={() => this.openCustCat(7)}>
                <View style={styles.txtbox}>
                  <View style={styles.txtimg}>
                    <Image
                      resizeMode="stretch"
                      style={{height: 95, width: '100%'}}
                      source={require('../image/rectangle7.png')}
                    />
                  </View>
                  <Text>Astrology</Text>
                </View>
              </TouchableWithoutFeedback>
              <TouchableWithoutFeedback onPress={() => this.openCustCat(27)}>
                <View style={styles.txtbox}>
                  <View style={styles.txtimg}>
                    <Image
                      resizeMode="stretch"
                      style={{height: 95, width: '100%'}}
                      source={require('../image/rectangle8.png')}
                    />
                  </View>
                  <Text>Vastu</Text>
                </View>
              </TouchableWithoutFeedback>
              <TouchableWithoutFeedback onPress={() => this.openCustCat(30)}>
                <View style={styles.txtbox}>
                  {/* <Image
                    resizeMode="center"
                    style={styles.imggodchalisha}
                    source={require('../image/bhajanHome.jpg')}
                  /> */}
                  <View style={styles.txtimg}>
                    <Image
                      resizeMode="stretch"
                      style={{height: 95, width: '100%'}}
                      source={require('../image/rectangle9.png')}
                    />
                  </View>
                  <Text>Palmistry</Text>
                </View>
              </TouchableWithoutFeedback>
            </View>
            {/* <View
              style={{
                borderColor: ' rgba(100, 100, 100, 0.3)',
                borderWidth: 3,
                paddingTop: 10,
                paddingBottom: 10,
              }}>
              {this.state.dealOfTheDayProducts.length > 0 ? (
                <Text style={styles.txthead}>Deal Of The Day</Text>
              ) : null}
              <Products
                navigation={this.props.navigation}
                data={this.state.dealOfTheDayProducts}
              />
            </View> */}
            <View>
              <View
                style={{
                  alignItems: 'center',
                  justifyContent: 'center',
                  padding: 5,
                }}>
                <Text style={styles.txthead}> Divinity For God</Text>
              </View>
              <View
                style={{
                  flex: 1,
                  flexDirection: 'row',
                  alignItems: 'center',
                  justifyContent: 'center',
                }}>
                <TouchableWithoutFeedback
                  onPress={() => this.props.navigation.navigate('Arti')}>
                  <View style={styles.txtbox}>
                    <View style={styles.divtxtimg}>
                      <Image
                        resizeMode="stretch"
                        style={{height: 100, width: '90%'}}
                        source={require('../image/aarti-color.jpg')}
                      />
                    </View>
                    <Text>Aarti</Text>
                  </View>
                </TouchableWithoutFeedback>
                <TouchableWithoutFeedback
                  onPress={() => this.props.navigation.navigate('Bhajan')}>
                  <View style={styles.txtbox}>
                    <View style={styles.divtxtimg}>
                      <Image
                        resizeMode="stretch"
                        style={{height: 100, width: '90%'}}
                        source={require('../image/HomeBhajan.jpg')}
                      />
                    </View>
                    <Text>Bhajan</Text>
                  </View>
                </TouchableWithoutFeedback>
                <TouchableWithoutFeedback
                  onPress={() => this.props.navigation.navigate('Chalisa')}>
                  <View style={styles.txtbox}>
                    <View style={styles.divtxtimg}>
                      <Image
                        resizeMode="stretch"
                        style={{height: 100, width: '90%'}}
                        source={require('../image/chalisa.png')}
                      />
                    </View>
                    <Text>Chalisa</Text>
                  </View>
                </TouchableWithoutFeedback>
              </View>
            </View>
          </View>

          <View
            style={{
              borderColor: 'rgba(100,100,100,0.3)',
              borderWidth: 3,
              // marginBottom: 30,
            }}>
            <View
              style={{
                alignItems: 'center',
                justifyContent: 'center',
                padding: 5,
              }}>
              <Text style={styles.txthead}> knowledge For You</Text>
            </View>
            <View>
              <TouchableWithoutFeedback onPress={() => this.openCustCat(16)}>
                <View
                  style={{
                    borderColor: 'rgba(100,100,100,0.3)',
                    borderWidth: 3,
                  }}>
                  <Image
                    resizeMode="stretch"
                    style={styles.img}
                    source={require('../image/mandir.jpg')}
                  />
                </View>
              </TouchableWithoutFeedback>
            </View>
            <View style={{flex: 1, flexDirection: 'row'}}>
              <TouchableWithoutFeedback onPress={() => this.openCustCat(19)}>
                <View
                  style={{
                    flex: 0.5,
                    height: 150,
                    width: '100%',
                    alignItems: 'center',
                    justifyContent: 'center',
                  }}>
                  <View
                    style={{
                      height: 95,
                      width: '90%',
                      backgroundColor: 'white',
                      borderRadius: 10,
                    }}>
                    <Image
                      style={{height: 95, width: '100%'}}
                      source={require('../image/ramayan2.jpg')}
                    />
                  </View>
                  <Text>Ramayan</Text>
                </View>
              </TouchableWithoutFeedback>
              <TouchableWithoutFeedback onPress={() => this.openCustCat(23)}>
                <View
                  style={{
                    flex: 0.5,
                    height: 150,
                    width: '100%',
                    alignItems: 'center',
                    justifyContent: 'center',
                  }}>
                  <View
                    style={{
                      height: 95,
                      width: '90%',
                      backgroundColor: 'blue',
                      borderRadius: 10,
                    }}>
                    <Image
                      style={{height: 95, width: '100%'}}
                      source={require('../image/mahabharat.jpg')}
                    />
                  </View>
                  <Text>Mahabharat</Text>
                </View>
              </TouchableWithoutFeedback>
            </View>
          </View>
          {/* <View
            style={{
              borderColor: ' rgba(100, 100, 100, 0.3)',
              borderWidth: 3,
              paddingTop: 10,
              paddingBottom: 10,
            }}>
            {this.state.homePosters.length > 0 ? (
              <Text style={styles.txthead}>Poster For You</Text>
            ) : null}
            <Posters
              navigation={this.props.navigation}
              data={this.state.homePosters}
            />
          </View>

          <View
            style={{
              borderColor: ' rgba(100, 100, 100, 0.3)',
              borderWidth: 3,
              paddingTop: 10,
            }}>
            {this.state.mandirPost.length > 0 ? (
              <Text
                style={{
                  textAlign: 'center',
                  fontSize: 18,
                  padding: 5,
                }}>
                Mandir Darshan
              </Text>
            ) : null}
            <Posts
              navigation={this.props.navigation}
              data={this.state.mandirPost}
            />
          </View> */}
          <View
            style={{
              paddingTop: 10,
              borderColor: 'rgba(100,100,100,0.3)',
              borderWidth: 3,
              paddingBottom: 10,
              marginBottom: 48,
            }}>
            <View
              style={{
                flexDirection: 'row',
              }}>
              <View style={{flex: 2.5}}>
                <TouchableWithoutFeedback onPress={() => this.openCustCat(13)}>
                  <View style={styles.fourimag}>
                    <Image
                      style={styles.imggod}
                      source={require('../image/pauranik.jpg')}
                    />
                    <Text style={styles.txt}>Pauranik Katha</Text>
                  </View>
                </TouchableWithoutFeedback>
              </View>

              <View
                style={{
                  flex: 2.5,
                }}>
                <TouchableWithoutFeedback onPress={() => this.openCustCat(14)}>
                  <View style={styles.fourimag}>
                    <Image
                      style={styles.imggod}
                      source={require('../image/sharda.png')}
                    />
                    <Text style={styles.txt}>Suvichar</Text>
                  </View>
                </TouchableWithoutFeedback>
              </View>
              <View style={{flex: 2.5}}>
                <TouchableWithoutFeedback onPress={() => this.openCustCat(29)}>
                  <View style={styles.fourimag}>
                    <Image
                      style={styles.imggod}
                      source={require('../image/Sanskriti.jpg')}
                    />
                    <Text style={styles.txt}>Sanskriti</Text>
                  </View>
                </TouchableWithoutFeedback>
              </View>
              <View style={{flex: 2.5}}>
                <TouchableWithoutFeedback onPress={() => this.openCustCat(26)}>
                  <View style={styles.fourimag}>
                    <Image
                      style={styles.imggod}
                      source={require('../image/Bhakt.jpg')}
                    />
                    <Text style={styles.txt}>Bakt Katha</Text>
                  </View>
                </TouchableWithoutFeedback>
              </View>
            </View>
          </View>
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  img: {
    width: global.width,
    height: 150,
    backgroundColor: 'rgba(100,100,100,0.3)',
    resizeMode: 'stretch',
  },
  txt: {textAlign: 'center', fontSize: 12},
  txticon: {textAlign: 'center'},
  imag: {
    padding: 10,
    width: 100,
    height: 100,
    //backgroundColor: 'rgba(100,100,100,0.3)',
    resizeMode: 'center',
  },
  slide: {
    borderWidth: 1,
    borderColor: 'grey',
    padding: 5,
    marginBottom: 10,
    marginTop: 10,
    borderRadius: 10,
  },
  slider: {
    width: '50%',
    borderRadius: 20,
  },
  divtxtimg: {height: 120, width: '100%'},
  slideImg: {
    width: '50%',
    height: 60,
    borderRadius: 30,
  },
  title: {
    marginTop: 10,
  },
  imggod: {
    width: 50,
    height: 50,
    borderRadius: 25,
    justifyContent: 'center',
    alignContent: 'center',
  },
  imggodchalisha: {
    resizeMode: 'stretch',
    width: '100%',
    height: 80,
  },
  txthead: {
    textAlign: 'center',
    fontSize: 18,
    padding: 5,
  },
  txtbox: {
    flex: 0.33,
    //borderColor: ' rgba(100, 100, 100, 0.3)',
    //borderWidth: 3,
    height: 150,
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  txtimg: {
    height: 95,
    width: '90%',
    backgroundColor: 'pink',
  },
  container: {
    flex: 1,
  },
  title: {
    fontSize: 14,
    padding: 5,
  },
  row: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    marginRight: 10,
    padding: 10,
    width: '90%',
  },
  profilePic: {
    width: 50,
    height: 50,
    borderRadius: 25,
    marginRight: 5,
    backgroundColor: 'rgba(100,100,100,0.3)',
  },
  fourimag: {
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },
  card: {
    height: 250,
  },
});
