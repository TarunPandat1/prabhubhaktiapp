import React, {Component} from 'react';
import {
  Text,
  View,
  TouchableWithoutFeedback,
  Image,
  Alert,
  Button,
  TouchableOpacity,
} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import {StyleSheet} from 'react-native';

export default class Arti extends Component {
  openArtiList = id => {
    this.props.navigation.navigate('ArtiList', {
      catId: id,
    });
  };

  render() {
    return (
      <View>
        <View style={{flexDirection: 'row', paddingTop: 20}}>
          <View
            style={{
              flex: 3.3,
              alignItems: 'center',
              justifyContent: 'center',
            }}>
            <TouchableOpacity onPress={() => this.openArtiList(11)}>
              <Image
                style={styles.imggod}
                source={require('../image/durga.png')}
              />
              <Text style={{textAlign: 'center'}}>Durga</Text>
            </TouchableOpacity>
          </View>
          <View
            style={{flex: 3.3, alignItems: 'center', justifyContent: 'center'}}>
            <TouchableOpacity onPress={() => this.openArtiList(18)}>
              <Image
                style={styles.imggod}
                source={require('../image/ganesha.png')}
              />
              <Text style={{textAlign: 'center'}}>Ganesha</Text>
            </TouchableOpacity>
          </View>
          <View
            style={{flex: 3.3, alignItems: 'center', justifyContent: 'center'}}>
            <TouchableOpacity onPress={() => this.openArtiList(5)}>
              <Image
                style={styles.imggod}
                source={require('../image/krishna.png')}
              />
              <Text style={{textAlign: 'center'}}>Krishana</Text>
            </TouchableOpacity>
          </View>
        </View>
        <View style={{flexDirection: 'row', paddingTop: 20}}>
          <View
            style={{flex: 3.3, alignItems: 'center', justifyContent: 'center'}}>
            <TouchableOpacity onPress={() => this.openArtiList(1)}>
              <Image
                style={styles.imggod}
                source={require('../image/hanuman.png')}
              />
              <Text style={{textAlign: 'center'}}>Hanuman</Text>
            </TouchableOpacity>
          </View>
          <View
            style={{flex: 3.3, alignItems: 'center', justifyContent: 'center'}}>
            <TouchableOpacity onPress={() => this.openArtiList(2)}>
              <Image
                style={styles.imggod}
                source={require('../image/shiv.png')}
              />
              <Text style={{textAlign: 'center'}}>Shiva</Text>
            </TouchableOpacity>
          </View>
          <View
            style={{flex: 3.3, alignItems: 'center', justifyContent: 'center'}}>
            <TouchableOpacity onPress={() => this.openArtiList(3)}>
              <Image
                style={styles.imggod}
                source={require('../image/laxmi.png')}
              />
              <Text style={{textAlign: 'center'}}>Lakshmi</Text>
            </TouchableOpacity>
          </View>
        </View>
        <View style={{flexDirection: 'row', paddingTop: 20}}>
          <View
            style={{flex: 3.3, alignItems: 'center', justifyContent: 'center'}}>
            <TouchableOpacity onPress={() => this.openArtiList(6)}>
              <Image
                style={styles.imggod}
                source={require('../image/sani.png')}
              />
              <Text style={{textAlign: 'center'}}>Shani</Text>
            </TouchableOpacity>
          </View>
          <View
            style={{flex: 3.3, alignItems: 'center', justifyContent: 'center'}}>
            <TouchableOpacity onPress={() => this.openArtiList(27)}>
              <Image
                style={styles.imggod}
                source={require('../image/vishnu.png')}
              />
              <Text style={{textAlign: 'center'}}>Vishnu</Text>
            </TouchableOpacity>
          </View>
          <View
            style={{flex: 3.3, alignItems: 'center', justifyContent: 'center'}}>
            <TouchableOpacity onPress={() => this.openArtiList(28)}>
              <Image
                style={styles.imggod}
                source={require('../image/santoshi.png')}
              />
              <Text style={{textAlign: 'center'}}>Santoshi Maa</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  imggod: {
    width: 70,
    height: 70,
    borderRadius: 35,
  },
});
