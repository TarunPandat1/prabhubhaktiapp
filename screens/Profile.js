import React, { Component } from 'react';
import { Text, StyleSheet, View, Button, Image, ScrollView } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import HeaderIcon from './header';
import { connect } from 'react-redux';

class Profile extends Component {
  state = {
    username: '',
    password: '',
    id: '',
    email: '',
    address1: '',
    address2: '',
    city: '',
    state: '',
    pincode: '',
    country: '',
    coupon_code: '',
    image: 'https://www.prabhibhakti.com',
    myimg: 'https://facebook.github.io/react/logo-og.png',
  };

  getData = async () => {
    var value = await AsyncStorage.getItem('username');

    if (value == null) {
      this.props.navigation.replace('Login');
    }

    this.setState({
      id: await AsyncStorage.getItem('id'),
      username: await AsyncStorage.getItem('username'),
      mobile: await AsyncStorage.getItem('mobile'),
      email: await AsyncStorage.getItem('email'),
      address1: await AsyncStorage.getItem('address1'),
      address2: await AsyncStorage.getItem('address2'),
      city: await AsyncStorage.getItem('city'),
      state: await AsyncStorage.getItem('state'),
      pincode: await AsyncStorage.getItem('pincode'),
      country: await AsyncStorage.getItem('country'),
      coupon_code: String(await AsyncStorage.getItem('coupon_used')),
      image: await AsyncStorage.getItem('image'),
    });
  };

  componentDidMount() {
    this.getData();
  }

  timeInterval = setInterval(async () => {
    var value = await AsyncStorage.getItem('username');

    if (value == null) {
      this.props.navigation.replace('Login');
    }
    this.setState({
      id: await AsyncStorage.getItem('id'),
      username: await AsyncStorage.getItem('username'),
      mobile: await AsyncStorage.getItem('mobile'),
      email: await AsyncStorage.getItem('email'),
      address1: await AsyncStorage.getItem('address1'),
      address2: await AsyncStorage.getItem('address2'),
      city: await AsyncStorage.getItem('city'),
      state: await AsyncStorage.getItem('state'),
      pincode: await AsyncStorage.getItem('pincode'),
      country: await AsyncStorage.getItem('country'),
      coupon_code: String(await AsyncStorage.getItem('coupon_used')),
      image: await AsyncStorage.getItem('image'),
    });
  }, 1000);

  logout = async () => {
    await AsyncStorage.removeItem('id');
    await AsyncStorage.removeItem('username');
    await AsyncStorage.removeItem('mobile');
    await AsyncStorage.removeItem('email');
    await AsyncStorage.removeItem('address1');
    await AsyncStorage.removeItem('address2');
    await AsyncStorage.removeItem('city');
    await AsyncStorage.removeItem('state');
    await AsyncStorage.removeItem('pincode');
    await AsyncStorage.removeItem('country');
    await AsyncStorage.removeItem('coupon_used');
    await AsyncStorage.removeItem('image');
    this.props.navigation.replace('Login');
  };

  render() {
    return (
      <View>
        <HeaderIcon navigation={this.props.navigation} />
        <ScrollView style={{ marginBottom: 55 }}>
          <View style={styles.main}>
            <Image source={{ uri: this.state.image }} style={styles.ppic} />

            <View style={styles.row}>
              <View style={styles.col1}>
                <Text style={styles.details}>Username: </Text>
              </View>

              <View style={styles.col2}>
                <Text style={styles.details}> {this.state.username} </Text>
              </View>
            </View>

            <View style={styles.row}>
              <View style={styles.col1}>
                <Text style={styles.details}>Email: </Text>
              </View>

              <View style={styles.col2}>
                <Text style={styles.details}> {this.state.email} </Text>
              </View>
            </View>

            <View style={styles.row}>
              <View style={styles.col1}>
                <Text style={styles.details}>Mobile: </Text>
              </View>

              <View style={styles.col2}>
                <Text style={styles.details}> {this.state.mobile} </Text>
              </View>
            </View>

            <View style={styles.row}>
              <View style={styles.col1}>
                <Text style={styles.details}>Address: </Text>
              </View>

              <View style={styles.col2}>
                <Text style={styles.details}>
                  {' '}
                  {this.state.address1 + ', ' + this.state.address2}{' '}
                </Text>
              </View>
            </View>

            <View style={styles.row}>
              <View style={styles.col1}>
                <Text style={styles.details}>State: </Text>
              </View>

              <View style={styles.col2}>
                <Text style={styles.details}> {this.state.state} </Text>
              </View>
            </View>

            <View style={styles.row}>
              <View style={styles.col1}>
                <Text style={styles.details}>City: </Text>
              </View>

              <View style={styles.col2}>
                <Text style={styles.details}> {this.state.city} </Text>
              </View>
            </View>

            <View style={styles.row}>
              <View style={styles.col1}>
                <Text style={styles.details}>Pincode: </Text>
              </View>

              <View style={styles.col2}>
                <Text style={styles.details}> {this.state.pincode} </Text>
              </View>
            </View>

            {/* <View style={styles.row}>
            <View style={styles.col1}>
              <Text style={styles.details}>Country: </Text>
            </View>

            <View style={styles.col2}>
              <Text style={styles.details}> {this.state.country} </Text>
            </View>
          </View> */}

            {/* <Button title="Logout" onPress={this.logout} /> */}
          </View>
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  ppic: {
    width: 200,
    height: 200,
    borderRadius: 100,
    margin: 15,
    backgroundColor: 'rgba(100,100,100,0.3)',
  },
  main: {
    margin: 20,
    justifyContent: 'center',
    alignContent: 'center',
  },
  details: {
    fontSize: 18,
    margin: 10,
  },
  row: {
    flexDirection: 'row',
  },
  col1: {
    flex: 1,
  },
  col2: {
    flex: 2,
  },
  col3: {
    flex: 3,
  },
  col4: {
    flex: 4,
  },
  col5: {
    flex: 5,
  },
});

export default Profile;
