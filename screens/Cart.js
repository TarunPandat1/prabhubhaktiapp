import React, {Component} from 'react';
import Ionicons from 'react-native-vector-icons/Ionicons';
import {View, Text, TouchableWithoutFeedback} from 'react-native';
import {connect} from 'react-redux';

class Cart extends Component {
  componentDidMount() {
    console.log(this.props.counter);
  }
  render() {
    return (
      <TouchableWithoutFeedback
        onPress={() =>
          this.props.navigation.navigate('ShoppingCart', {
            id: Math.random(),
          })
        }>
        <View>
          <View
            style={{
              backgroundColor: 'rgba(0,200, 0, 0.4)',
              borderRadius: 10,
              width: 20,
              height: 20,
              top: 10,
              left: -10,
              zIndex: 1000,
            }}>
            <Text
              style={{
                textAlign: 'center',
                alignItems: 'center',
                color: 'white',
                fontSize: 14,
                zIndex: 2000,
              }}>
              {this.props.counter.length}
            </Text>
          </View>
          <Ionicons name="ios-cart" size={25} color="#ff7604" />
        </View>
      </TouchableWithoutFeedback>
    );
  }
}

function mapStateToProps(state) {
  return {
    counter: state.cart,
    orderHistory: state.orderHistory,
  };
}

const cartAPI = () => {
  return (dispatch, getState) => {
    fetch(
      `${global.ip}cart?id=${global.macAddress}&user=${global.username}&mobile=${global.mobile}`,
    )
      .then(response => response.json())
      .then(responseJson => {
        // console.log(responseJson);
        dispatch({type: 'CartCounting', data: responseJson});
      })
      .catch(err => console.log(err));
  };
};

const orderHistoryAPI = () => {
  return (dispatch, getState) => {
    fetch(
      `${global.ip}orderHistory?id=${global.macAddress}&user=${global.username}&mobile=${global.mobile}`,
    )
      .then(response => response.json())
      .then(responseJson => {
        // console.log(responseJson);
        dispatch({type: 'OrderHistory', data: responseJson});
      })
      .catch(err => console.log(err));
  };
};

function mapDispatchToProps(dispatch) {
  return {
    CartCounting: () => dispatch(cartAPI()),
    OrderHistory: () => dispatch(orderHistoryAPI()),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Cart);
