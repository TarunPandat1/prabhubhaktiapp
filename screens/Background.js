import React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {SliderBox} from 'react-native-image-slider-box';

export default class SlideshowTest extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      images: [
        'https://i.imgur.com/pj0x3Uc.jpg',
        'https://i.imgur.com/Gz8U10P.jpg',
      ],
    };
  }

  render() {
    return (
      <SliderBox
        images={this.state.images}
        sliderBoxHeight={125}
        onCurrentImagePressed={index => console.warn(`image ${index} pressed`)}
        dotColor="#ff7604"
        inactiveDotColor="#90A4AE"
        resizeMode={'stretch'}
        // dotStyle={{
        //   width: 15,
        //   height: 15,
        //   borderRadius: 15,
        //   marginHorizontal: 10,
        //   padding: 0,
        //   margin: 0,
        // }}
      />
    );
  }
}
