import React, {Component} from 'react';
import {View, Text, Image, StyleSheet, StatusBar} from 'react-native';

export class SplashScreen extends Component {
  componentDidMount() {
    setTimeout(() => {
      this.props.navigation.replace('Main');
    }, 2000);
  }

  render() {
    return (
      <View style={Styles.main}>
        <StatusBar backgroundColor="#ff7604" barStyle="light-content" />
        <Image style={Styles.img} source={require('../image/logo.png')} />
      </View>
    );
  }
}

const Styles = StyleSheet.create({
  main: {
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
    margin: 10,
  },
  img: {
    width: 250,
    height: 50,
  },
});
