import React, {Component} from 'react';
import {
  Text,
  View,
  StyleSheet,
  Image,
  ActivityIndicator,
  FlatList,
  TouchableWithoutFeedback,
} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import DynamicHomeScreen from './DynamicHomeScreen';

export default class Godrelated extends Component {
  static navigationOptions = {
    headerShown: false,
    title: 'Videos',
    headerstyle: {
      backgroundColor: 'red',
    },
  };
  offset = 0;

  state = {
    post: [],
    newPost: [],
    loading: true,
    DataNotFound: false,
  };

  fetch = async o => {
    let catId = this.props.navigation.getParam('catId');
    let postCat = this.props.navigation.getParam('postCat');
    const url = `${global.ip}custCateVideos?catId=${catId}&postCat=${postCat}&offset=${o}`;
    const response = await fetch(url);
    const data = await response.json();
    if (data.length == 0 && o == 0) {
      this.setState({
        DataNotFound: true,
      });
    }
    if (o == 0) {
      id =
        this.props.navigation.getParam('catId').toString() +
        `CustCate${(postCat = this.props.navigation.getParam(
          'postCat',
        ))}Videos`;
      await AsyncStorage.setItem(id, JSON.stringify(data));
    }
    this.setState(
      {
        post: this.state.post.concat(data),
        loading: false,
      },
      () => {
        console.log('Post found successfully..!');
      },
    );
  };

  isCloseToBottom = ({layoutMeasurement, contentOffset, contentSize}) => {
    return (
      layoutMeasurement.height + contentOffset.y >= contentSize.height - 1500
    );
  };

  componentDidMount() {
    this.fetch(this.offset).catch(err => {
      console.log(err);
      this.get_post();
    });
  }

  get_post = async () => {
    id =
      this.props.navigation.getParam('catId').toString() +
      `CustCate${(postCat = this.props.navigation.getParam('postCat'))}Videos`;
    var l = await AsyncStorage.getItem(id);
    this.setState({
      post: JSON.parse(l),
    });
  };

  openPost = item => {
    this.props.navigation.navigate('Video', {
      title: item.post_title,
      mySrc: item.featuredImage,
      post_content: item.post_content.replace(/<[^>]*>|&nbsp;/g, ' '),
      youtube_link: item.youtube_link,
      cid: item.customize_category,
      id: item.id,
      catName: item.category_name,
      headerShown: true,
    });
  };

  getMorePost = () => {
    this.setState({
      loading: true,
    });
    this.offset = this.offset + 10;
    this.fetch(this.offset).catch(err => {
      console.log(err);
    });
    console.log('scroll');
  };

  renderFooter = () => {
    return (
      <View>
        {this.state.loading && <ActivityIndicator size="large" color="cyan" />}
      </View>
    );
  };

  separator = () => {
    return <View style={{height: 1, backgroundColor: '', margin: 3}} />;
  };

  render() {
    return (
      <View style={styles.container}>
        {this.state.DataNotFound && (
          <Text style={{textAlign: 'center', fontSize: 20, margin: 5}}>
            No Related Post Found..!
          </Text>
        )}
        <FlatList
          onScroll={({nativeEvent}) => {
            if (this.isCloseToBottom(nativeEvent)) {
              this.getMorePost();
            }
          }}
          data={this.state.post}
          keyExtractor={(item, index) => index.toString()}
          renderItem={({item}) => (
            <TouchableWithoutFeedback
              onPress={() => {
                this.openPost(item);
              }}>
              <View style={styles.main}>
                <View style={styles.row}>
                  <View>
                    <View></View>
                    <Image
                      style={styles.profilePic}
                      source={{uri: item.image}}
                    />
                  </View>
                  <View>
                    <Text
                      style={{fontWeight: '600', fontSize: 10, paddingLeft: 4}}>
                      {item.category_name}
                    </Text>
                    <Text style={styles.title}>{item.post_title}</Text>
                  </View>
                </View>
                <View>
                  <DynamicHomeScreen
                    featuredImage={item.featuredImage}></DynamicHomeScreen>
                </View>
              </View>
            </TouchableWithoutFeedback>
          )}
          ListFooterComponent={this.renderFooter}
          ItemSeparatorComponent={this.separator}
        />
      </View>
    );
  }
}
const styles = StyleSheet.create({
  title: {
    fontSize: 18,
    padding: 5,
  },
  row: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    marginRight: 10,
    padding: 10,
    width: '90%',
  },
  profilePic: {
    width: 50,
    height: 50,
    borderRadius: 25,
    marginRight: 5,
    backgroundColor: 'rgba(100,100,100,0.3)',
  },
  image: {
    height: 400,
    backgroundColor: 'rgba(100,100,100,0.3)',
  },
  main: {
    // marginBottom: 20,
    borderTopColor: 'rgba(0,0,0,0.1)',
    borderTopWidth: 1,
  },
  container: {
    flex: 1,
  },
});
