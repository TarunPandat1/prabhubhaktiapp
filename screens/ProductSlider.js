import React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {SliderBox} from 'react-native-image-slider-box';

export default class ProductSlider extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      images: [
        this.props.navigation.getParam('image1'),
        this.props.navigation.getParam('image2'),
        this.props.navigation.getParam('image3'),
        this.props.navigation.getParam('image4'),
        this.props.navigation.getParam('image5'),
        this.props.navigation.getParam('image6'),
      ],
    };
  }

  render() {
    return (
      <SliderBox
        images={this.state.images}
        sliderBoxHeight={450}
        onCurrentImagePressed={index => console.warn(`image ${index} pressed`)}
        dotColor="#ff7604"
        inactiveDotColor="#90A4AE"
        resizeMode={'stretch'}
        // dotStyle={{
        //   width: 15,
        //   height: 15,
        //   borderRadius: 15,
        //   marginHorizontal: 10,
        //   padding: 0,
        //   margin: 0,
        // }}
      />
    );
  }
}
