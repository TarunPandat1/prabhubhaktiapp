import React, {Component} from 'react';
import {
  View,
  Text,
  Image,
  TouchableWithoutFeedback,
  TouchableOpacity,
  StyleSheet,
  TextInput,
} from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';
import HeaderIcon from './header';

export default class Thanksup extends React.Component {
  constructor(props) {
    super(props);
    this.state = {text: ''};
  }
  render() {
    return (
      <View style={{flex: 100}}>
        <View style={{flex: 6, backgroundColor: 'white'}}>
          <HeaderIcon navigation={this.props.navigation} />
        </View>
        <View style={{flex: 88}}>
          <View style={styles.TopImg}>
            <Image
              source={require('../image/thank.png')}
              style={{height: 266, width: 266}}
            />
          </View>
          <View>
            <Text style={styles.content}>Thank You for</Text>
            <Text style={styles.content}>the order</Text>
          </View>
          <View style={{paddingTop: 50}}>
            <Text style={styles.contentsmall}>
              Your Order has been confirmed and
            </Text>
            <Text style={styles.contentsmall}>
              will delivered to you in next 4-5 days
            </Text>
          </View>
        </View>
        <View style={{flex: 6, backgroundColor: 'white'}}>
          <TouchableOpacity
            style={styles.cart}
            onPress={() => this.props.navigation.navigate('Product')}
            title="Continued">
            <Text style={{color: 'white'}}>CONTINUED SHOPPING</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  img: {
    flex: 0.5,
    backgroundColor: '#fff',
    height: 300,
    flexDirection: 'column',
    marginBottom: 10,
    padding: 5,
  },
  cart: {
    height: 50,
    width: '100%',
    borderColor: 'rgba(100,100,100,0.3)',
    borderWidth: 3,
    backgroundColor: '#ff7604',
    alignItems: 'center',
    justifyContent: 'center',
  },
  btn: {
    height: 50,
    width: 100,
  },
  TopImg: {
    height: 300,
    borderColor: 'rgba(255, 255, 255, 0.3)',
    borderWidth: 2,
    justifyContent: 'center',
    alignItems: 'center',
  },
  SecondImg: {
    borderColor: 'rgba(255, 255, 255, 0.3)',
    borderWidth: 2,
  },
  content: {
    fontSize: 36,
    textAlign: 'center',
  },
  contentsmall: {
    fontSize: 18,
    textAlign: 'center',
  },
});
