import React, {Component} from 'react';
import {
  View,
  Text,
  Image,
  TouchableWithoutFeedback,
  TouchableOpacity,
  StyleSheet,
  TextInput,
} from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';
import HeaderIcon from './header';

export default class OrderScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {text: ''};
  }
  render() {
    return (
      <View style={{flex: 100}}>
        <View style={{flex: 6, backgroundColor: 'white'}}>
          <HeaderIcon />
        </View>
        <View style={{flex: 86}}>
          <View
            style={{
              borderColor: 'rgba(255, 255, 255, 0.3)',
              borderWidth: 4,
            }}>
            <View style={{flexDirection: 'row', flex: 1}}>
              <View style={{flex: 0.2, padding: 20}}>
                <Image
                  source={require('../image/arti.jpg')}
                  style={{height: 80, width: 80}}
                />
              </View>
              <View style={{flex: 0.8, flexDirection: 'column', padding: 20}}>
                <Text>Title:Shiv</Text>
                <Text>Name:Shiv</Text>
                <Text>Class:Shiv</Text>
                <Text>Std:Shiv</Text>
              </View>
            </View>
          </View>
        </View>
        <View style={styles.subcoopan}>
          <View style={styles.subcoopanpart1}>
            <TextInput
              style={{height: 40}}
              placeholder="Enter Coupon"
              onChangeText={text => this.setState({text})}
              value={this.state.text}
            />
          </View>
          <View style={styles.subcoopanpart2}>
            <TouchableOpacity
              onPress={() => {
                alert('You tapped the button!');
              }}
              style={styles.apply}>
              <Text style={{paddingTop: 10, textAlign: 'center'}}>Apply</Text>
            </TouchableOpacity>
          </View>
        </View>
        <View style={styles.subtotal}>
          <View style={styles.subtotalpart}>
            <Text>Subtotal</Text>
            <Text>Delivery Charge</Text>
            <Text>Total Amount of Pay</Text>
          </View>
          <View style={styles.subtotalpart}>
            <Text>4200</Text>
            <Text>Free</Text>
            <Text>4200</Text>
          </View>
        </View>
        <View style={{flex: 8, backgroundColor: 'white'}}>
          <View Style={{flex: 0.0001}}>
            <View style={{}}>
              <TouchableOpacity
                style={styles.cart}
                onPress={() => this.props.navigation.navigate('AddressScreen')}
                title="Continued">
                <Text>CONTINUED</Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  img: {
    flex: 0.5,
    backgroundColor: '#fff',
    height: 300,
    flexDirection: 'column',
    marginBottom: 10,
    padding: 5,
  },
  cart: {
    height: 50,
    width: '100%',
    borderColor: 'rgba(100,100,100,0.3)',
    borderWidth: 3,
    backgroundColor: '#ff7604',
    alignItems: 'center',
    justifyContent: 'center',
  },
  subtotal: {
    padding: 10,
    backgroundColor: 'white',
    height: 90,
    flexDirection: 'row',
  },
  subtotalpart: {
    flex: 0.5,
    justifyContent: 'center',
    alignItems: 'center',
  },
  subcoopan: {
    padding: 10,
    backgroundColor: 'white',
    height: 50,
    flexDirection: 'row',
  },
  subcoopanpart1: {
    flex: 0.7,
    justifyContent: 'center',
    alignItems: 'center',
  },
  subcoopanpart2: {
    flex: 0.2,
    justifyContent: 'center',
    alignItems: 'center',
  },
  apply: {
    height: 40,
    width: 100,
    backgroundColor: '#ff7604',
    borderRadius: 10,
  },
});
