import React, {Component} from 'react';
import {
  View,
  Text,
  Image,
  StyleSheet,
  TouchableWithoutFeedback,
  Alert,
  ScrollView,
} from 'react-native';
import HeaderIcon from './header';

export default class Shanicategory extends Component {
  openCustCate = (catId, postCat) => {
    this.props.navigation.navigate('CustCate', {
      catId: catId,
      postCat: postCat,
    });
  };

  render() {
    return (
      <View>
        <HeaderIcon navigation={this.props.navigation} />
        <ScrollView>
          <View style={{borderWidth: 4, borderColor: 'rgba(255,255,255,0.3)'}}>
            <Image
              source={require('../image/shani.jpg')}
              style={styles.imgback1}
            />
          </View>
          <View
            style={{
              alignItems: 'center',
              justifyContent: 'center',
              flexDirection: 'column',
              padding: 10,
            }}>
            <Text
              style={{fontSize: 20, fontWeight: 'bold', fontFamily: 'Roboto'}}>
              Special For You
            </Text>
            <Text style={{fontSize: 16}}> On Prabhubhakti</Text>
          </View>
          <View
            style={{
              flexDirection: 'row',
              paddingTop: 10,
              // borderColor: 'rgba(100,100,100,0.3)',
              // borderWidth: 5,
              paddingBottom: 15,
            }}>
            <View style={{flex: 3.3}}>
              <TouchableWithoutFeedback
                onPress={() => {
                  this.openCustCate(6, 'Arti');
                }}>
                <View style={styles.imgthree}>
                  <Image
                    style={styles.imggod}
                    source={require('../image/Aarti.png')}
                  />
                  <Text style={{paddingHorizontal: 1}}>Arti</Text>
                </View>
              </TouchableWithoutFeedback>
            </View>

            <View style={{flex: 3.3}}>
              <TouchableWithoutFeedback
                onPress={() => {
                  this.openCustCate(6, 'Chalisha');
                }}>
                <View style={styles.imgthree}>
                  <Image
                    style={styles.imggod}
                    source={require('../image/chalisa.png')}
                  />
                  <Text>Chalisha</Text>
                </View>
              </TouchableWithoutFeedback>
            </View>
            <View style={{flex: 3.3}}>
              <TouchableWithoutFeedback
                onPress={() => {
                  this.openCustCate(6, 'Bhajan');
                }}>
                <View style={styles.imgthree}>
                  <Image
                    style={styles.imggod}
                    source={require('../image/Bhajan.png')}
                  />
                  <Text>Bhajan</Text>
                </View>
              </TouchableWithoutFeedback>
            </View>
          </View>
          <View style={{backgroundColor: 'rgba(255,255,255,0.3)'}}>
            <TouchableWithoutFeedback
              onPress={() =>
                this.props.navigation.navigate('Cat', {
                  catId: 6,
                })
              }>
              <Image
                resizeMode="contain"
                source={require('../image/shani1.jpg')}
                style={styles.imgback2}
              />
            </TouchableWithoutFeedback>
          </View>
          <View
            style={{
              alignItems: 'center',
              justifyContent: 'center',
              flexDirection: 'column',
              paddingTop: 15,
            }}>
            <Text
              style={{fontSize: 20, fontWeight: 'bold', fontFamily: 'Roboto'}}>
              Devotational
            </Text>
            <Text style={{fontSize: 16, paddingBottom: 30}}>
              Your Divine Connection
            </Text>
          </View>
          <View
            style={{
              flexDirection: 'row',
              // borderColor: 'rgba(100,100,100,0.3)',
              // borderWidth: 5,
              paddingBottom: 15,
            }}>
            <View style={{flex: 3.3}}>
              <TouchableWithoutFeedback
                onPress={() => {
                  this.openCustCate(6, 'Strotra');
                }}>
                <View style={styles.imgthree}>
                  <Image
                    style={styles.imggodrectangle}
                    source={require('../image/Strotra.png')}
                  />
                  <Text style={{paddingHorizontal: 1}}>Strotra</Text>
                </View>
              </TouchableWithoutFeedback>
            </View>

            <View style={{flex: 3.3}}>
              <TouchableWithoutFeedback
                onPress={() => {
                  this.openCustCate(6, 'Mantra');
                }}>
                <View style={styles.imgthree}>
                  <Image
                    style={styles.imggodrectangle}
                    source={require('../image/Mantra.png')}
                  />
                  <Text>Mantra</Text>
                </View>
              </TouchableWithoutFeedback>
            </View>
            <View style={{flex: 3.3}}>
              <TouchableWithoutFeedback
                onPress={() => {
                  this.openCustCate(6, 'Ashtakam');
                }}>
                <View style={styles.imgthree}>
                  <Image
                    style={styles.imggodrectangle}
                    source={require('../image/Ashtakam.png')}
                  />
                  <Text>Ashtakam</Text>
                </View>
              </TouchableWithoutFeedback>
            </View>
          </View>
          <View
            style={{
              borderWidth: 4,
              borderColor: 'rgba(255,255,255,0.3)',
              paddingBottom: 42,
            }}>
            <TouchableWithoutFeedback
              onPress={() => this.openCustCate(6, 'Mandir')}>
              <Image
                source={require('../image/shani2.jpg')}
                style={styles.img}
              />
            </TouchableWithoutFeedback>
          </View>
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  imggod: {
    width: 106,
    height: 106,
    borderRadius: 53,
    justifyContent: 'center',
    alignContent: 'center',
  },
  img: {
    width: global.width,
    height: 150,
    backgroundColor: 'rgba(100,100,100,0.3)',
    resizeMode: 'stretch',
    padding: 10,
  },
  imgback: {
    width: global.width,
    height: 150,
    backgroundColor: 'rgba(100,100,100,0.3)',
    resizeMode: 'stretch',
  },
  imgback1: {
    width: global.width,
    height: 150,
    backgroundColor: 'rgba(100,100,100,0.3)',
    resizeMode: 'stretch',
  },
  imgback2: {
    width: global.width,
    height: 150,
    backgroundColor: 'rgba(100,100,100,0.3)',
    resizeMode: 'stretch',
  },
  imggodrectangle: {
    width: 110,
    height: 110,
    borderRadius: 10,
    justifyContent: 'center',
    alignContent: 'center',
  },
  imgthree: {
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
