import React, {Component} from 'react';
import {
  View,
  Image,
  TextInput,
  StyleSheet,
  KeyboardAvoidingView,
  TouchableWithoutFeedback,
  TouchableOpacity,
  Keyboard,
  Text,
  Alert,
} from 'react-native';

class ForgetPassword extends Component {
  constructor(props) {
    super(props);
    this.state = {
      username: '',
      email: '',
      mobile: '',
      error: '',
    };
  }

  forget = () => {
    let user = this.state.username;
    let mobile = this.state.mobile;
    let email = this.state.email;
    let url = `${global.ip}forgetpassword?user=${user}&email=${email}&mobile=${mobile}`;
    if (user && mobile) {
      fetch(url)
        .then(response => response.json())
        .then(responseJson => {
          if (responseJson == '1') {
            Alert.alert(
              'Reset Password Successfully..!',
              'Your password is now your mobile number, Go to prabhubhakti website to change your new password..!',
              [
                {
                  text: 'OK',
                  onPress: () => this.props.navigation.navigate('Login'),
                },
              ],
              {cancelable: false},
            );
          } else {
            this.setState({
              error: 'Please fill your correct details..!',
            });
          }
        });
    } else {
      this.setState({
        error: 'Please Fill Your Details..',
      });
    }
  };
  render() {
    return (
      <View
        style={{
          flex: 1,
          margin: 10,
        }}>
        <View
          style={{
            alignItems: 'center',
            justifyContent: 'center',
          }}>
          <Image
            source={require('../image/logo.png')}
            style={{height: 40, width: 182}}
          />
        </View>
        <KeyboardAvoidingView
          behavior="position"
          enabled
          style={{flex: 1, marginTop: 200}}>
          <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
            <View>
              <TextInput
                style={styles.input}
                placeholder="Username"
                onChangeText={username => {
                  this.setState({username});
                  this.setState({error: ''});
                }}
                onSubmitEditing={() => this.refs.email.focus()}
              />
              <TextInput
                style={styles.input}
                placeholder="Email"
                onChangeText={email => {
                  this.setState({email});
                  this.setState({error: ''});
                }}
                onSubmitEditing={() => this.refs.mobile.focus()}
                ref={'email'}
              />
              <TextInput
                style={styles.input}
                placeholder="Mobile"
                onChangeText={mobile => {
                  this.setState({mobile});
                  this.setState({error: ''});
                }}
                returnKeyType="next"
                ref={'mobile'}
              />
              <Text style={styles.error}>{this.state.error}</Text>
              <TouchableOpacity onPress={() => this.forget()}>
                <View style={styles.loginBtn}>
                  <Text>ForgetPassword</Text>
                </View>
              </TouchableOpacity>
            </View>
          </TouchableWithoutFeedback>
        </KeyboardAvoidingView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  heading: {
    alignItems: 'center',
    justifyContent: 'center',
    fontSize: 20,
    color: '#ff7604',
    fontWeight: 'bold',
    textAlign: 'center',
  },
  main: {
    margin: 20,
    justifyContent: 'center',
    alignContent: 'center',
  },
  input: {
    borderBottomWidth: 1,
    borderBottomColor: '#ff7604',
  },
  loginBtn: {
    justifyContent: 'center',
    alignItems: 'center',
    padding: 10,
    borderWidth: 1,
    margin: 5,
    borderColor: '#ff7604',
    backgroundColor: '#ff7604',
    marginTop: 30,
    borderRadius: 15,
  },
  error: {
    color: 'red',
  },
});

export default ForgetPassword;
