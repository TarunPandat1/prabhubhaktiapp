import React, {Component} from 'react';
import {
  Text,
  StyleSheet,
  View,
  TextInput,
  TouchableOpacity,
  Button,
  Image,
  TouchableWithoutFeedback,
  KeyboardAvoidingView,
  Keyboard,
  ScrollView,
  Picker,
  ToastAndroid,
} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import {connect} from 'react-redux';

class SignUpScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      error: '',
      mobError: '',
      emailError: '',
      passError: '',
      username: '',
      mobile: '',
      email: '',
      add1: '',
      add2: '',
      state: '',
      city: '',
      pincode: '',
      password: '',
      conPassword: '',
    };
  }

  signUp = async () => {
    this.setState({
      error: '',
    });

    // var username = this.state.username;
    // var mobile = this.state.mobile;
    // var email = this.state.email;
    // var add1 = this.state.add1;
    // var add2 = this.state.add2;
    // var state = this.state.state;
    // var city = this.state.city;
    // var pincode = this.state.pincode;

    var {
      username,
      mobile,
      email,
      add1,
      add2,
      state,
      city,
      pincode,
      password,
      conPassword,
    } = this.state;

    if (
      username &&
      mobile &&
      add1 &&
      add2 &&
      state &&
      city &&
      password &&
      conPassword
    ) {
      if (conPassword == password) {
        if (!this.state.mobError && !this.state.emailError) {
          if (mobile.length == 10) {
            this.setState({
              mobError: '',
            });

            var address = `{"address1": "${add1}", "address2": "${add2}"}`;
            // var add = JSON.parse(address);
            let url =
              global.ip +
              'signUp?name=' +
              username +
              '&password=' +
              password +
              '&mobile=' +
              mobile +
              '&email=' +
              email +
              '&address=' +
              address +
              '&state=' +
              state +
              '&city=' +
              city +
              '&pincode=' +
              pincode;
            console.log(url);
            fetch(url)
              .then(response => response.json())
              .then(responseJson => {
                if (responseJson == '1') {
                  this.login(username, password);
                } else {
                  alert('Something went wrong..!');
                }
              });
          } else {
            this.setState({
              mobError: 'Please enter valid mobile number..!',
            });
          }
        } else {
          ToastAndroid.showWithGravity(
            'Something Went Wrong..!',
            ToastAndroid.LONG,
            ToastAndroid.BOTTOM,
          );
        }
      } else {
        this.setState({
          passError: 'Confirm Password Not Match..!',
        });
      }
    } else {
      this.setState({
        error: 'Please fill all the details...',
      });
    }
  };

  mobileVerification = async mobile => {
    const response = await fetch(global.ip + 'mobileVerify?mobile=' + mobile);
    const data = await response.json();
    if (data != '0') {
      this.setState({mobError: 'Mobile number is already registered..!'});
    } else {
      this.setState({mobError: ''});
    }
  };

  emailVerification = async email => {
    const response = await fetch(global.ip + 'emailVerify?email=' + email);
    const data = await response.json();

    if (data != '0' && this.state.email) {
      this.setState({emailError: 'Email is already registered..!'});
    } else {
      this.setState({emailError: ''});
    }
  };

  login = async (username, password) => {
    const response = await fetch(
      global.ip + 'login?username=' + username + '&password=' + password + '',
    );
    const data = await response.json();
    console.log(data);
    if (data.length > 0) {
      await AsyncStorage.setItem('id', String(data[0].id));
      await AsyncStorage.setItem('username', data[0].name);
      await AsyncStorage.setItem('mobile', String(data[0].mobile));
      await AsyncStorage.setItem('email', data[0].email);
      let address = JSON.parse(data[0].address);
      await AsyncStorage.setItem('address1', address.address1);
      await AsyncStorage.setItem('address2', address.address2);
      await AsyncStorage.setItem('state', data[0].state);
      await AsyncStorage.setItem('city', data[0].city);
      await AsyncStorage.setItem('pincode', String(data[0].pincode));
      await AsyncStorage.setItem('country', data[0].country);
      if (data[0].coupon_used !== null) {
        await AsyncStorage.setItem('coupon_used', data[0].coupon_used);
      }

      await AsyncStorage.setItem('image', data[0].image);
      this.setState({
        data: await AsyncStorage.getItem('username'),
      });
      this.props.navigation.replace('Profile');
      global.getData();
      this.props.CartCounting();
      this.props.OrderHistory();
    } else {
      // alert('Username and Password is wrong..!');
    }
  };

  render() {
    return (
      <ScrollView>
        <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
          <View style={styles.main}>
            <KeyboardAvoidingView style={styles.inputCon} behavior="padding">
              <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
                <View>
                  <View
                    style={{
                      justifyContent: 'center',
                      alignItems: 'center',
                      marginBottom: 20,
                    }}>
                    <Image
                      source={require('../image/logo.png')}
                      style={{height: 60, width: 300}}
                    />
                  </View>
                  <View>
                    <TextInput
                      style={styles.input}
                      placeholder="Username"
                      onChangeText={username => this.setState({username})}
                      onSubmitEditing={() => this.refs.mobile.focus()}
                    />
                    {/* <Text style={styles.error}>{this.state.uerror}</Text> */}
                    <TextInput
                      style={styles.input}
                      placeholder="Mobile"
                      onChangeText={mobile => {
                        this.setState({mobile});
                        this.mobileVerification(mobile);
                      }}
                      keyboardType={'numeric'}
                      onSubmitEditing={() => this.refs.email.focus()}
                      ref={'mobile'}
                      maxLength={10}
                    />
                    {this.state.mobError ? (
                      <Text style={styles.error}>{this.state.mobError}</Text>
                    ) : null}

                    <TextInput
                      style={styles.input}
                      placeholder="Email"
                      onChangeText={email => {
                        this.setState({email});
                        this.emailVerification(email);
                      }}
                      //secureTextEntry={true}
                      onSubmitEditing={() => this.refs.add1.focus()}
                      ref={'email'}
                    />

                    {this.state.emailError ? (
                      <Text style={styles.error}>{this.state.emailError}</Text>
                    ) : null}

                    <TextInput
                      style={styles.input}
                      placeholder="Address1"
                      onChangeText={add1 => this.setState({add1})}
                      //secureTextEntry={true}
                      onSubmitEditing={() => this.refs.add2.focus()}
                      ref={'add1'}
                    />
                    {/* <Text style={styles.error}>{this.state.perror}</Text> */}
                    <TextInput
                      style={styles.input}
                      placeholder="Address2"
                      onChangeText={add2 => this.setState({add2})}
                      //secureTextEntry={true}
                      onSubmitEditing={() => this.refs.city.focus()}
                      ref={'add2'}
                    />
                    {/* <Text style={styles.error}>{this.state.perror}</Text> */}

                    <Picker
                      style={styles.pick}
                      selectedValue={this.state.state}
                      style={{height: 50, width: '100%'}}
                      onValueChange={(itemValue, itemIndex) =>
                        this.setState({state: itemValue})
                      }>
                      <Picker.Item label="--- Select State---" value="" />
                      <Picker.Item
                        label="Andhra Pradesh"
                        value="Andhra Pradesh"
                      />
                      <Picker.Item
                        label="Arunachal Pradesh"
                        value="Arunachal Pradesh"
                      />
                      <Picker.Item label="Assam" value="Assam" />
                      <Picker.Item label="Bihar" value="Bihar" />
                      <Picker.Item label="Chhattisgarh" value="Chhattisgarh" />
                      <Picker.Item label="Goa" value="Goa" />
                      <Picker.Item label="Gujarat" value="Gujarat" />
                      <Picker.Item label="Haryana" value="Haryana" />
                      <Picker.Item
                        label="Himachal Pradesh"
                        value="Himachal Pradesh"
                      />
                      <Picker.Item
                        label="Jammu & Kashmir"
                        value="Jammu & Kashmir"
                      />
                      <Picker.Item label="Jharkhand" value="Jharkhand" />
                      <Picker.Item label="Karnataka" value="Karnataka" />
                      <Picker.Item label="Kerala" value="Kerala" />
                      <Picker.Item
                        label="Madhya Pradesh"
                        value="Madhya Pradesh"
                      />
                      <Picker.Item label="Maharashtra" value="Maharashtra" />
                      <Picker.Item label="Manipur" value="Manipur" />
                      <Picker.Item label="Meghalaya" value="Meghalaya" />
                      <Picker.Item label="Mizoram" value="Mizoram" />
                      <Picker.Item label="Nagaland" value="Nagaland" />
                      <Picker.Item label="Orissa" value="Orissa" />
                      <Picker.Item label="Punjab" value="Punjab" />
                      <Picker.Item label="Rajasthan" value="Rajasthan" />
                      <Picker.Item label="Sikkim" value="Sikkim" />
                      <Picker.Item label="Tamil Nadu" value="Tamil Nadu" />
                      <Picker.Item label="Telangana" value="Telangana" />
                      <Picker.Item label="Tripura" value="Tripura" />
                      <Picker.Item
                        label="Uttar Pradesh"
                        value="Uttar Pradesh"
                      />
                      <Picker.Item label="Uttarakhand" value="Uttarakhand" />
                      <Picker.Item label="West Bengal" value="West Bengal" />
                      <Picker.Item
                        label="Andaman & Nicobar Islands"
                        value="Andaman & Nicobar Islands"
                      />
                      <Picker.Item
                        label="Dadra and Nagar Haveli"
                        value="Dadra and Nagar Haveli"
                      />
                      <Picker.Item
                        label="Daman and Diu"
                        value="Daman and Diu"
                      />
                      <Picker.Item label="Lakshadweep" value="Lakshadweep" />
                      <Picker.Item label="Puducherry" value="Puducherry" />
                      <Picker.Item label="Delhi" value="Delhi" />
                      <Picker.Item label="Chandigarh" value="Chandigarh" />
                    </Picker>
                    {/* <Text style={styles.error}>{this.state.perror}</Text> */}
                    <View style={{flexDirection: 'row'}}>
                      <View
                        style={{
                          backgroundColor: '#ff7604',
                          height: 2,
                          flex: 1,
                          alignSelf: 'center',
                        }}
                      />
                      <View
                        style={{
                          backgroundColor: '#ff7604',
                          height: 2,
                          flex: 1,
                          alignSelf: 'center',
                        }}
                      />
                    </View>
                    <TextInput
                      style={styles.input}
                      placeholder="City"
                      onChangeText={city => this.setState({city})}
                      //secureTextEntry={true}
                      onSubmitEditing={() => this.refs.pincode.focus()}
                      ref={'city'}
                    />

                    <TextInput
                      style={styles.input}
                      placeholder="Pincode"
                      onChangeText={pincode => this.setState({pincode})}
                      keyboardType={'numeric'}
                      onSubmitEditing={() => this.refs.password.focus()}
                      ref={'pincode'}
                    />

                    <TextInput
                      style={styles.input}
                      placeholder="Password"
                      onChangeText={password => this.setState({password})}
                      onSubmitEditing={() => this.refs.conPassword.focus()}
                      secureTextEntry={true}
                      ref={'password'}
                    />

                    <TextInput
                      style={styles.input}
                      placeholder="Confirm Password"
                      onChangeText={conPassword => this.setState({conPassword})}
                      secureTextEntry={true}
                      returnKeyType="next"
                      ref={'conPassword'}
                    />
                    {this.state.passError ? (
                      <Text style={styles.error}>{this.state.passError}</Text>
                    ) : null}

                    {this.state.error ? (
                      <Text style={styles.error}>{this.state.error}</Text>
                    ) : null}

                    <TouchableOpacity
                      onPress={this.signUp}
                      style={styles.loginBtn}>
                      <Text style={{color: '#ffffff'}}>SignUp</Text>
                    </TouchableOpacity>
                    <View
                      style={{
                        margin: 20,
                        justifyContent: 'center',
                        alignItems: 'center',
                        flex: 1,
                      }}>
                      <TouchableOpacity
                        onPress={() => this.props.navigation.navigate('Login')}>
                        <Text style={{color: 'green'}}>
                          Already have an account..? Please Login.
                        </Text>
                      </TouchableOpacity>
                    </View>
                  </View>
                </View>
              </TouchableWithoutFeedback>
            </KeyboardAvoidingView>
          </View>
        </TouchableWithoutFeedback>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  heading: {
    alignItems: 'center',
    justifyContent: 'center',
    fontSize: 20,
    color: '#ff7604',
    fontWeight: 'bold',
    textAlign: 'center',
  },
  main: {
    margin: 20,
    justifyContent: 'center',
    alignContent: 'center',
  },
  input: {
    borderBottomWidth: 1,
    borderBottomColor: '#ff7604',
  },
  loginBtn: {
    justifyContent: 'center',
    alignItems: 'center',
    padding: 10,
    borderWidth: 1,
    margin: 5,
    borderColor: '#ff7604',
    backgroundColor: '#ff7604',
    marginTop: 30,
    borderRadius: 15,
  },
  error: {
    color: 'red',
    marginTop: 10,
  },
});

function mapStateToProps(state) {
  return {
    counter: state.cart,
    orders: state.orderHistory,
  };
}

const cartAPI = () => {
  return (dispatch, getState) => {
    fetch(
      `${global.ip}cart?id=${global.macAddress}&user=${global.username}&mobile=${global.mobile}`,
    )
      .then(response => response.json())
      .then(responseJson => {
        // console.log(responseJson);
        dispatch({type: 'CartCounting', data: responseJson});
      })
      .catch(err => console.log(err));
  };
};

const orderHistoryAPI = () => {
  return (dispatch, getState) => {
    fetch(
      `${global.ip}orderHistory?user=${global.username}&mobile=${global.mobile}`,
    )
      .then(response => response.json())
      .then(responseJson => {
        // console.log(responseJson);
        dispatch({type: 'OrderHistory', orders: responseJson});
      })
      .catch(err => console.log(err));
  };
};

function mapDispatchToProps(dispatch) {
  return {
    CartCounting: () => dispatch(cartAPI()),
    OrderHistory: () => dispatch(orderHistoryAPI()),
  };
}
export default connect(mapStateToProps, mapDispatchToProps)(SignUpScreen);
