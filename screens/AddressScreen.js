import React, {Component} from 'react';
import {
  View,
  Text,
  Image,
  TouchableWithoutFeedback,
  TouchableOpacity,
  StyleSheet,
} from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';
import HeaderIcon from './header';

export default class AddressScreen extends React.Component {
  render() {
    return (
      <View style={{flex: 100}}>
        <View style={{flex: 6, backgroundColor: 'white'}}>
          <HeaderIcon />
        </View>
        <View style={{flex: 88}}>
          <View
            style={{
              borderColor: 'rgba(255, 255, 255, 0.3)',
              borderWidth: 4,
            }}>
            <Text>Address: sector 19 Udyog Vihar Gurugram</Text>
            <Text>Udyog Vihar Gurugram</Text>
            <Text>Gurugram</Text>
            <Text>230306</Text>
          </View>
          <View
            style={{
              borderColor: 'rgba(255, 255, 255, 0.3)',
              borderWidth: 4,
            }}>
            <Text>Address: sector 19 Udyog Vihar Gurugram</Text>
            <Text>Udyog Vihar Gurugram</Text>
            <Text>Gurugram</Text>
            <Text>230306</Text>
          </View>
        </View>
        <View style={{flex: 6, backgroundColor: 'white'}}>
          <View Style={{flex: 0.0001}}>
            <View style={{}}>
              <TouchableOpacity
                style={styles.cart}
                onPress={() => this.props.navigation.navigate('PaymentScreen')}
                title="Continue">
                <Text>Continue</Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  img: {
    flex: 0.5,
    backgroundColor: '#fff',
    height: 300,
    flexDirection: 'column',
    marginBottom: 10,
    padding: 5,
  },
  cart: {
    height: 50,
    width: '100%',
    borderColor: 'rgba(100,100,100,0.3)',
    borderWidth: 3,
    backgroundColor: '#ff7604',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
