import React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {SliderBox} from 'react-native-image-slider-box';

export default class SlideshowhanumanTest extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      images: [
        'https://i.imgur.com/eyjBra4.jpg',
        'https://i.imgur.com/SEoJM93.jpg',
      ],
    };
  }

  render() {
    return (
      <SliderBox
        images={this.state.images}
        sliderBoxHeight={125}
        onCurrentImagePressed={index => console.warn(`image ${index} pressed`)}
        dotColor="#FFEE58"
        inactiveDotColor="#90A4AE"
        resizeMode={'contain'}
        // dotStyle={{
        //   width: 15,
        //   height: 15,
        //   borderRadius: 15,
        //   marginHorizontal: 10,
        //   padding: 0,
        //   margin: 0,
        // }}
      />
    );
  }
}
