import React, {Component} from 'react';
import {
  View,
  Text,
  FlatList,
  TouchableOpacity,
  StyleSheet,
  Image,
  ToastAndroid,
  Alert,
  Linking,
  RefreshControl,
} from 'react-native';
import {connect} from 'react-redux';
import HeaderIcon from './header';

import Ionicons from 'react-native-vector-icons/Ionicons';

class MyOrders extends Component {
  state = {
    refreshing: false,
  };

  componentDidMount() {
    this.props.OrderHistory();
  }

  cancelOrder = id => {
    Alert.alert(
      'Order Cancellation',
      'Are You Sure To Cancel Your Order',
      [
        {
          text: 'Yes, Cancel my order',
          onPress: () => {
            let url = `${global.ip}cancelOrder?id=${id}`;
            fetch(url)
              .then(response => response.json())
              .then(responseJson => {
                if (responseJson == '1') {
                  this.props.OrderHistory();
                  ToastAndroid.showWithGravity(
                    'Your Order is Cancelled Successfully.',
                    ToastAndroid.LONG,
                    ToastAndroid.BOTTOM,
                  );
                } else {
                  ToastAndroid.showWithGravity(
                    'Somthing Whent Wrong to Cancelling your Order.',
                    ToastAndroid.LONG,
                    ToastAndroid.BOTTOM,
                  );
                }
              })
              .catch(err => {
                ToastAndroid.showWithGravity(
                  'Somthing Whent Wrong to Cancelling your Order.',
                  ToastAndroid.LONG,
                  ToastAndroid.BOTTOM,
                );
              });
          },
          style: 'cancel',
        },
        {text: 'No', onPress: () => console.log('OK Pressed')},
      ],
      {cancelable: false},
    );
  };

  spearator = () => {
    return <View style={{height: 1, backgroundColor: 'rgba(0,0,0,0.1)'}} />;
  };
  orders2 = item => {
    return (
      <View style={{margin: 10}}>
        <View style={styles.row}>
          <View style={styles.col2}>
            {item.productImage ? (
              <Image
                source={{uri: item.productImage}}
                style={{height: 80, width: 80}}
              />
            ) : (
              <Image
                source={{
                  uri:
                    'https://www.indiaspora.org/wp-content/uploads/2018/10/image-not-available.jpg',
                }}
                style={{height: 80, width: 80}}
              />
            )}
          </View>

          <View style={styles.col3}>
            <Text>{item.hidden_product_name}</Text>
            <Text>Price: ₹{item.hidden_unit_price}</Text>
            <Text>Quantity: {item.quantity}</Text>
            <Text>Total: ₹{item.total_pr_price}</Text>
          </View>
        </View>
      </View>
    );
  };
  orders = item => {
    const pd = JSON.parse(item.product_details);
    return (
      <>
        <View
          style={{
            margin: 10,
            padding: 10,
            borderRadius: 10,
            borderWidth: 1,
            borderColor: 'rgba(0,0,0,0.4)',
          }}>
          <FlatList
            data={pd}
            keyExtractor={(item, index) => index.toString()}
            renderItem={({item}) => this.orders2(item)}
            ItemSeparatorComponent={this.spearator}
            // ListFooterComponent={this.footer}
          />
          <Text>Purchased on: {item.order_date.substring(0, 10)}</Text>
          <Text>Total Amount: ₹{item.hidden_total}</Text>

          {item.status == '1' ? (
            <Text style={{color: 'red', margin: 10}}>Order is cancelled</Text>
          ) : item.method == 'Complete' ? (
            <Text style={{color: 'green', margin: 10}}>Order is Delivered</Text>
          ) : item.ship_status == 'In Transit' ? (
            <View style={{flexDirection: 'row'}}>
              <Text style={{color: 'green', margin: 10}}>
                Order is On The Way
              </Text>
              <TouchableOpacity
                style={{backgroundColor: 'green', padding: 6}}
                onPress={() => this.call()}>
                <Text style={{fontSize: 12, color: 'white'}}>
                  {' '}
                  <Ionicons name="ios-call" color="white" size={25} /> Call for
                  more details...
                </Text>
              </TouchableOpacity>
            </View>
          ) : (
            <TouchableOpacity
              style={{margin: 10}}
              onPress={() => this.cancelOrder(item.order_id)}>
              <Text style={{color: '#ff7604'}}>Cancel My Order</Text>
            </TouchableOpacity>
          )}
          <Text style={{margin: 10}}> Your OrderID: {item.orderID}</Text>
        </View>
      </>
    );
  };

  footer = () => {
    return (
      <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
        <TouchableOpacity
          onPress={() => this.props.navigation.navigate('Product')}>
          <Text
            style={{
              padding: 15,
              backgroundColor: '#ff7604',
              color: 'white',
              marginTop: 100,
            }}>
            Continue Shopping
          </Text>
        </TouchableOpacity>
      </View>
    );
  };

  call = () => {
    let phoneNumber = 9999474433;
    let callLink = `tel:${phoneNumber}`;

    Linking.canOpenURL(callLink)
      .then(supported => {
        if (!supported) {
          Alert.alert('Phone number is not available');
        } else {
          return Linking.openURL(callLink);
        }
      })
      .catch(err => console.log(err));
  };

  onRefresh = () => {
    this.setState({
      refreshing: true,
    });
    this.props.OrderHistory();
    setTimeout(() => {
      this.setState({refreshing: false});
    }, 2000);
  };

  render() {
    return (
      <>
        <HeaderIcon navigation={this.props.navigation} />
        {this.props.orders.length > 0 ? (
          <FlatList
            refreshControl={
              <RefreshControl
                refreshing={this.state.refreshing}
                onRefresh={this.onRefresh.bind(this)}
              />
            }
            data={this.props.orders}
            keyExtractor={(item, index) => index.toString()}
            renderItem={({item}) => this.orders(item)}
            ItemSeparatorComponent={this.spearator}
          />
        ) : (
          <>
            <Text
              style={{
                flex: 1,
                justifyContent: 'center',
                textAlign: 'center',
                marginTop: 100,
                fontSize: 20,
                lineHeight: 40,
              }}>
              You Didn't Purchased Anything Yet, Please Purchase Some Valuable
              Products..!
            </Text>
            <View
              style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
              <TouchableOpacity
                onPress={() => this.props.navigation.navigate('Product')}>
                <Text
                  style={{
                    padding: 15,
                    backgroundColor: '#ff7604',
                    color: 'white',
                    marginTop: 100,
                  }}>
                  Continue Shopping
                </Text>
              </TouchableOpacity>
            </View>
          </>
        )}
      </>
    );
  }
}

const styles = StyleSheet.create({
  row: {
    flexDirection: 'row',
    flex: 1,
  },
  col1: {
    flex: 1,
  },
  col2: {
    flex: 2,
  },
  col3: {
    flex: 3,
  },
  col4: {
    flex: 4,
  },
  col5: {
    flex: 5,
  },
});

function mapStateToProps(state) {
  return {
    counter: state.cart,
    orders: state.orderHistory,
  };
}

const cartAPI = () => {
  return (dispatch, getState) => {
    fetch(
      `${global.ip}cart?id=${global.macAddress}&user=${global.username}&mobile=${global.mobile}`,
    )
      .then(response => response.json())
      .then(responseJson => {
        // console.log(responseJson);
        dispatch({type: 'CartCounting', data: responseJson});
      })
      .catch(err => console.log(err));
  };
};

const orderHistoryAPI = () => {
  return (dispatch, getState) => {
    fetch(
      `${global.ip}orderHistory?user=${global.username}&mobile=${global.mobile}`,
    )
      .then(response => response.json())
      .then(responseJson => {
        // console.log(responseJson);
        dispatch({type: 'OrderHistory', orders: responseJson});
      })
      .catch(err => console.log(err));
  };
};

function mapDispatchToProps(dispatch) {
  return {
    CartCounting: () => dispatch(cartAPI()),
    OrderHistory: () => dispatch(orderHistoryAPI()),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(MyOrders);
